module BDD where

import Prelude hiding (negate,sum)
import qualified Data.Set as S
import Data.List (intercalate,minimumBy,maximumBy,find,partition,delete)
import Data.Maybe
import Data.Either
import qualified Data.Map as M

import PropositionalLogic
import Types

-- This is a great algorithm! Seems to be very efficient.
ebddtaut :: Formula Prop -> Bool
ebddtaut fm = snd (mkbdds M.empty (mk_bdd (<),M.empty) defs fm') == 1
 where
  (l,r) = either (const (TT,fm)) id (dest_nimp fm)
  (eqs,noneqs) = partition (isRight . dest_iffdef) (conjuncts l)
  (defs,fm') = sort_defs [] (map (fromRight . dest_iffdef) eqs) (itlist Imp noneqs r)

mkbdds sfn bdd [] fm = mkbdde sfn bdd fm
mkbdds sfn bdd ((p,e):odefs) fm =
  let (bdd',b) = mkbdde sfn bdd e in
  mkbdds (M.insert p b sfn) bdd' odefs fm

mkbdde sfn bddcomp@(bdd,comp) FF = (bddcomp,-1)
mkbdde sfn bddcomp@(bdd,comp) TT = (bddcomp,1)
mkbdde sfn bddcomp@(bdd,comp) (Atom s) = maybe (let (bdd',n) = mk_node bdd (s,1,-1) in ((bdd',comp),n)) ((,) bddcomp) (M.lookup s sfn)
mkbdde sfn bddcomp@(bdd,comp) (Not p) = let (bddcomp',n) = mkbdde sfn bddcomp p in (bddcomp',-n)
mkbdde sfn bddcomp@(bdd,comp) (And p q) = thread bddcomp bdd_and (mkbdde sfn,p) (mkbdde sfn,q)
mkbdde sfn bddcomp@(bdd,comp) (Or p q) = thread bddcomp bdd_or (mkbdde sfn,p) (mkbdde sfn,q)
mkbdde sfn bddcomp@(bdd,comp) (Imp p q) = thread bddcomp bdd_imp (mkbdde sfn,p) (mkbdde sfn,q)
mkbdde sfn bddcomp@(bdd,comp) (Iff p q) = thread bddcomp bdd_iff (mkbdde sfn,p) (mkbdde sfn,q)

sort_defs acc defs fm
 | isJust mxe =
    let (x,e) = fromJust mxe in
    let (ps,nonps) = partition (\(x',_) -> x' == x) defs in
    let ps' = delete (x,e) ps in
    sort_defs ((x,e) : acc) nonps (itlist restore_iffdef ps' fm)
 | otherwise = (reverse acc,itlist restore_iffdef defs fm)
 where
  mxe = find (suitable_iffdef defs) defs

suitable_iffdef :: (Ord a, Foldable u) => u (a, v) -> (t, Formula a) -> Bool
suitable_iffdef defs (x,q) = not (any (\(x',_) -> S.member x' fvs) defs)
 where fvs = atoms q

restore_iffdef :: (a, Formula a) -> Formula a -> Formula a
restore_iffdef (x,e) fm = Imp (Iff (Atom x) e) fm

dest_iffdef (Iff (Atom x) r) = Right (x,r)
dest_iffdef (Iff r (Atom x)) = Right (x,r)
dest_iffdef _ = Left "not a defining equivalence"

dest_nimp (Not p) = Right (p,FF)
dest_nimp fm = dest_imp fm

bddtaut :: Formula Prop -> Bool
bddtaut fm = snd (mkbdd (mk_bdd (<),M.empty) fm) == 1

mkbdd bddcomp@(bdd,comp) FF = (bddcomp,-1)
mkbdd bddcomp@(bdd,comp) TT = (bddcomp,1)
mkbdd bddcomp@(bdd,comp) (Atom s) = let (bdd',n) = mk_node bdd (s,1,-1) in ((bdd',comp),n)
mkbdd bddcomp@(bdd,comp) (Not p) = let (bddcomp',n) = mkbdd bddcomp p in (bddcomp',-n)
mkbdd bddcomp@(bdd,comp) (And p q) = thread bddcomp bdd_and (mkbdd,p) (mkbdd,q)
mkbdd bddcomp@(bdd,comp) (Or p q) = thread bddcomp bdd_or (mkbdd,p) (mkbdd,q)
mkbdd bddcomp@(bdd,comp) (Imp p q) = thread bddcomp bdd_imp (mkbdd,p) (mkbdd,q)
mkbdd bddcomp@(bdd,comp) (Iff p q) = thread bddcomp bdd_iff (mkbdd,p) (mkbdd,q)

bdd_iff bdc (m1,m2) = thread bdc bdd_or (bdd_and,(m1,m2)) (bdd_and,(-m1,-m2))

bdd_imp bdc (m1,m2) = bdd_or bdc (-m1,m2)

bdd_or bdc (m1,m2) = (bdc1,-n)
 where
  (bdc1,n) = bdd_and bdc (-m1,-m2)

bdd_and :: (BDD, M.Map (Int, Int) Int) -> (Int, Int) -> ((BDD, M.Map (Int, Int) Int), Int)
bdd_and bddcomp@(bdd,comp) (m1,m2)
 | m1 == -1 || m2 == -1 = (bddcomp,-1)
 | m1 == 1 = (bddcomp,m2)
 | m2 == 1 = (bddcomp,m1)
 | isJust comp_m1_m2 = (bddcomp,fromJust comp_m1_m2)
 | isJust comp_m2_m1 = (bddcomp,fromJust comp_m2_m1)
 | otherwise = ((bdd'',M.insert (m1,m2) n comp'),n)
  where
    comp_m1_m2 = M.lookup (m1,m2) comp
    comp_m2_m1 = M.lookup (m2,m1) comp
    (BDDNode p1 l1 r1) = expand_node bdd m1
    (BDDNode p2 l2 r2) = expand_node bdd m2
    (p,lpair,rpair) =
       if p1 == p2 then (p1,(l1,l2),(r1,r2))
       else if order bdd p1 p2 then (p1,(l1,m2),(r1,m2))
       else (p2,(m1,l2),(m1,r2))
    ((bdd',comp'),(lnew,rnew)) = thread bddcomp (,) (bdd_and,lpair) (bdd_and,rpair)
    (bdd'',n) = mk_node bdd' (p,lnew,rnew)

data BDDNode = BDDNode Prop Int Int   deriving (Eq,Ord)

data BDD = BDD (M.Map BDDNode Int) (M.Map Int BDDNode) Int (Prop -> Prop -> Bool)

print_bdd :: BDD -> String
print_bdd (BDD unique uback n ord) = "<BDD with " ++ (show n) ++ " nodes>"

instance Show BDD where
  show = print_bdd

expand_node :: BDD -> Int -> BDDNode
expand_node (BDD _ expand _ _) n =
  if n >= 0 then M.findWithDefault (BDDNode (P "") 1 1) n expand
  else let (BDDNode p l r) = M.findWithDefault (BDDNode (P "") 1 1) (-n) expand in (BDDNode p (-l) (-r))

lookup_unique :: BDD -> BDDNode -> (BDD, Int)
lookup_unique bdd@(BDD unique expand n ord) node
  | M.member node unique = (bdd,unique M.! node)
  | otherwise = ((BDD (M.insert node n unique) (M.insert n node expand) (n+1) ord),n)

mk_node :: BDD -> (Prop, Int, Int) -> (BDD, Int)
mk_node bdd (s,l,r)
  | l == r = (bdd,l)
  | l >= 0 = lookup_unique bdd (BDDNode s l r)
  | otherwise = let (bdd',n) = lookup_unique bdd (BDDNode s (-l) (-r)) in (bdd',-n)

mk_bdd :: (Prop -> Prop -> Bool) -> BDD
mk_bdd ord = BDD M.empty M.empty 2 ord

order :: BDD -> Prop -> Prop -> Bool
order (BDD _ _ _ ord) p1 p2 = ((p2 == P "") && (p1 /= P "")) || ord p1 p2

thread s g (f1,x1) (f2,x2) = g s'' (y1,y2)
 where
  (s',y1) = f1 s x1
  (s'',y2) = f2 s' x2
