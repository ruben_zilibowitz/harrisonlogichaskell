module DP where

import Prelude hiding (negate,sum)
import qualified Data.Set as S
import Data.List (intercalate,minimumBy,maximumBy)
import Data.Maybe
import qualified Data.Map as M

import PropositionalLogic
import Types
import Failing

-- Section 2.9

dplbsat fm = dplb (defcnfs fm) []
dplbtaut fm = not (dplbsat (Not fm))

dplb cls trail =
  let (cls',trail') = unit_propagate (cls,trail) in
  if S.member S.empty cls' then
    case backtrack trail of
      (p,Guessed) : ts ->
        let trail' = backjump cls p ts in
        let declits = filter (isGuessed . snd) trail' in
        let conflict = (negate p) : (map (negate . fst) declits) in
        dplb (S.insert (S.fromList conflict) cls) ((negate p,Deduced) : trail')
      _ -> False
  else
    case unassigned cls trail' of
      ps | S.null ps -> True
         | otherwise -> let p = maximize (posneg_count cls') ps in
                        dplb cls ((p,Guessed) : trail')

backjump cls p trail = case backtrack trail of
  (q,Guessed) : ts -> let (cls',trail') = unit_propagate (cls,(p,Guessed):ts) in
                      if S.member S.empty cls' then backjump cls p ts else trail
  _ -> trail

dplisat :: Formula Prop -> Bool
dplisat fm = dpli (defcnfs fm) []

dplitaut :: Formula Prop -> Bool
dplitaut fm = not (dplisat (Not fm))

dpli :: Ord t => S.Set (S.Set (Formula t)) -> [(Formula t, TrailMix)] -> Bool
dpli cls trail
  | S.member S.empty cls' = case backtrack trail of
      ((p,Guessed) : ts) -> dpli cls ((negate p,Deduced) : ts)
      _                  -> False
  | otherwise = case unassigned cls trail' of
      ps | S.null ps -> True
         | otherwise -> let p = maximize (posneg_count cls') ps in
            dpli cls ((p,Guessed) : trail')
 where
  (cls',trail') = unit_propagate (cls,trail)

backtrack :: [(t, TrailMix)] -> [(t, TrailMix)]
backtrack ((p,Deduced) : ts) = backtrack ts
backtrack trail = trail

unit_propagate (cls,trail) = (cls',trail')
 where
  fn = itlist (\(x,_) -> M.insert x undefined) trail M.empty
  (cls',fn',trail') = unit_subpropagate (cls,fn,trail)

unit_subpropagate (cls,fn,trail)
  | null newunits = (cls',fn,trail)
  | otherwise = unit_subpropagate (cls',fn',trail')
 where
  cls' = S.map (S.filter ((flip M.notMember fn) . negate)) cls
  uu cs = case cs of
    [c] | M.notMember c fn -> Just c
    _                      -> Nothing
  newunits = mapfilter uu (S.toList (S.map S.toList cls'))
  trail' = itlist (\p t -> (p,Deduced) : t) newunits trail
  fn' = itlist (\u -> M.insert u undefined) newunits fn

unassigned
  :: Ord t =>
     S.Set (S.Set (Formula t)) -> [(Formula t, b)] -> S.Set (Formula t)
unassigned cls trail = S.difference (unions (S.map (S.map litabs) cls)) (S.map litabs (S.fromList (map fst trail)))
 where
  litabs p = case p of {Not q -> q; _ -> p}

isGuessed Guessed = True
isGuessed _ = False

data TrailMix = Guessed | Deduced

dpllsat :: Formula Prop -> Bool
dpllsat fm = dpll (defcnfs fm)

dplltaut :: Formula Prop -> Bool
dplltaut fm = not (dpllsat (Not fm))

dpll :: Ord t => S.Set (S.Set (Formula t)) -> Bool
dpll clauses
  | S.null clauses = True
  | S.member S.empty clauses = False
  | isSuccess try1 = dpll $ fromSuccess try1
  | isSuccess try2 = dpll $ fromSuccess try2
  | otherwise = dpll (S.insert (S.singleton p) clauses) || dpll (S.insert (S.singleton (negate p)) clauses)
 where
  try1 = one_literal_rule clauses 
  try2 = affirmative_negative_rule clauses 
  pvs = S.filter positive (unions clauses)
  p = maximize (posneg_count clauses) pvs

posneg_count :: Ord t => S.Set (S.Set (Formula t)) -> Formula t -> Int
posneg_count cls l = m + n
 where
  m = S.size (S.filter (S.member l) cls)
  n = S.size (S.filter (S.member (negate l)) cls)

dpsat :: Formula Prop -> Bool
dpsat fm = dp (defcnfs fm)

dptaut :: Formula Prop -> Bool
dptaut fm = not (dpsat (Not fm))

dp :: Ord t => S.Set (S.Set (Formula t)) -> Bool
dp clauses
  | S.null clauses = True
  | S.member S.empty clauses = False
  | isSuccess try1 = dp $ fromSuccess try1
  | isSuccess try2 = dp $ fromSuccess try2
  | otherwise = dp (resolution_rule clauses)
 where
  try1 = one_literal_rule clauses
  try2 = affirmative_negative_rule clauses

maximize :: (Ord b, Foldable t) => (a -> b) -> t a -> a
maximize f xs = maximumBy (\x y -> compare (f x) (f y)) xs

minimize :: (Ord b, Foldable t) => (a -> b) -> t a -> a
minimize f xs = minimumBy (\x y -> compare (f x) (f y)) xs

resolution_rule :: Ord t => S.Set (S.Set (Formula t)) -> S.Set (S.Set (Formula t))
resolution_rule clauses = resolve_on p clauses
 where
  pvs = S.filter positive (unions clauses)
  p = minimize (resolution_blowup clauses) pvs

resolution_blowup :: Ord t => S.Set (S.Set (Formula t)) -> Formula t -> Int
resolution_blowup cls l = m * n - m - n
 where
  m = S.size (S.filter (S.member l) cls)
  n = S.size (S.filter (S.member (negate l)) cls)

resolve_on :: Ord t => Formula t -> S.Set (S.Set (Formula t)) -> S.Set (S.Set (Formula t))
resolve_on p clauses = S.union other (S.filter (not . trivial) res0)
 where
  p' = negate p
  (pos,notpos) = S.partition (S.member p) clauses
  (neg,other) = S.partition (S.member p') notpos
  pos' = S.map (S.filter (/= p)) pos
  neg' = S.map (S.filter (/= p')) neg
  res0 = allpairs_union pos' neg'

affirmative_negative_rule clauses
  | S.null pure = failure "affirmative_negative_rule"
  | otherwise = return (S.filter (S.null . (S.intersection pure)) clauses)
 where
  (neg',pos) = S.partition negative (unions clauses)
  neg = S.map negate neg'
  pos_only = S.difference pos neg
  neg_only = S.difference neg pos
  pure = S.union pos_only (S.map negate neg_only)

one_literal_rule clauses
  | S.null f = failure "one_literal_rule"
  | otherwise = return (S.map (S.delete u') clauses1)
 where
  f = S.filter ((1 ==) . (S.size)) clauses
  u = S.findMin (S.findMin f)
  u' = negate u
  clauses1 = S.filter (S.notMember u) clauses
