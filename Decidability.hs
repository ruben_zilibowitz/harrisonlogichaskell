module Decidability where

import Prelude hiding (negate,sum,pred)
import qualified Data.Set as S
import Data.List (intercalate,minimumBy,maximumBy,find,partition,delete)
import Data.Maybe
import Data.Either (isRight)
import qualified Data.Map as M
import Debug.Trace

import PropositionalLogic hiding (nnf)
import FirstOrderLogic hiding (replace)
import Types
import Parser
import DP
import Equality

-- Section 5.6

dlo_test1 = quelim_dlo $ parseFOL "exists z. x < z & z < y"

-- Quantifier elimination for formulas in the theory of dense linear orders
quelim_dlo :: Formula FOL -> Formula FOL
quelim_dlo = lift_qelim afn_dlo (dnf . (cnnf lfn_dlo)) (\_ -> dlobasic)

afn_dlo :: t -> Formula FOL -> Formula FOL
afn_dlo vars (Atom (R "<=" [s,t])) = Not (Atom (R "<" [t,s]))
afn_dlo vars (Atom (R ">=" [s,t])) = Not (Atom (R "<" [s,t]))
afn_dlo vars (Atom (R ">" [s,t])) = Atom (R "<" [t,s])
afn_dlo vars fm = fm

dlobasic :: Formula FOL -> Formula FOL
dlobasic (Exists x p) = answer
 where
  answer =
      let cjs = delete (Atom (R "=" [Var x,Var x])) (setify (conjuncts p)) in
      try (do eqn <- maybe (Left "find") Right (find is_eq cjs)
              (s,t) <- dest_eq eqn
              let y = if s == Var x then t else s
              return (list_conj (map (subst (M.singleton x y)) (delete eqn (setify cjs)))))
          (if elem (Atom (R "<" [Var x,Var x])) cjs then FF else
           let (lefts,rights) = partition (\(Atom (R "<" [s,t])) -> t == Var x) cjs in
           let ls = map (\(Atom (R "<" [l,_])) -> l) lefts in
           let rs = map (\(Atom (R "<" [_,r])) -> r) rights in
           list_conj (allpairs (\l r -> Atom (R "<" [l,r])) ls rs))
dlobasic _ = error "dlobasic"

lfn_dlo :: Formula FOL -> Formula FOL
lfn_dlo (Not (Atom (R "<" [s,t]))) = Or (Atom (R "=" [s,t])) (Atom(R "<" [t,s]))
lfn_dlo (Not (Atom (R "=" [s,t]))) = Or (Atom (R "<" [s,t])) (Atom(R "<" [t,s]))
lfn_dlo fm = fm

cnnf :: (Formula FOL -> Formula FOL) -> Formula FOL -> Formula FOL
cnnf lfn = simplify . (cnnf' lfn) . simplify
 where
  cnnf' lfn (And p q) = And (cnnf' lfn p) (cnnf' lfn q)
  cnnf' lfn (Or p q) = Or (cnnf' lfn p) (cnnf' lfn q)
  cnnf' lfn (Imp p q) = Or (cnnf' lfn (Not p)) (cnnf' lfn q)
  cnnf' lfn (Iff p q) = Or (And (cnnf' lfn p) (cnnf' lfn q)) (And (cnnf' lfn (Not p)) (cnnf' lfn (Not q)))
  cnnf' lfn (Not (Not p)) = cnnf' lfn p
  cnnf' lfn (Not (And p q)) = Or (cnnf' lfn (Not p)) (cnnf' lfn (Not q))
  cnnf' lfn (Not (Or (And p q) (And p' r))) | p' == negate p = Or (cnnf' lfn (And p (Not q))) (cnnf' lfn (And p' (Not r)))
  cnnf' lfn (Not (Or p q)) = And (cnnf' lfn (Not p)) (cnnf' lfn (Not q))
  cnnf' lfn (Not (Imp p q)) = And (cnnf' lfn p) (cnnf' lfn (Not q))
  cnnf' lfn (Not (Iff p q)) = Or (And (cnnf' lfn p) (cnnf' lfn (Not q))) (And (cnnf' lfn (Not p)) (cnnf' lfn q))
  cnnf' lfn fm = lfn fm

lift_qelim afn nfn qfn fm0 = simplify (qelift (S.toList $ fv fm0) (miniscope fm0))
 where
  qelift vars fm@(Atom (R _ _)) = afn vars fm
  qelift vars (Not p) = Not (qelift vars p)
  qelift vars (And p q) = And (qelift vars p) (qelift vars q)
  qelift vars (Or p q) = Or (qelift vars p) (qelift vars q)
  qelift vars (Imp p q) = Imp (qelift vars p) (qelift vars q)
  qelift vars (Iff p q) = Iff (qelift vars p) (qelift vars q)
  qelift vars (Forall x p) = Not (qelift vars (Exists x (Not p)))
  qelift vars (Exists x p) = let djs = disjuncts (nfn (qelift (x:vars) p)) in list_disj (map (qelim (qfn vars) x) djs)
  qelift _ fm = fm

qelim
  :: (Formula FOL -> Formula FOL)
     -> String -> Formula FOL -> Formula FOL
qelim bfn x p = if null ycjs then p else itlist And ncjs q
 where
  cjs = conjuncts p
  (ycjs,ncjs) = partition ((S.member x) . fv) cjs
  q = bfn (Exists x (list_conj ycjs))

-- Section 5.5

decide_monadic :: Formula FOL -> Either String Bool
decide_monadic fm
  | not (null funcs) || any (\(_,ar) -> ar > 1) other = Left "Not in the monadic subset"
  | otherwise = Right (decide_finite n fm)
 where
  funcs = functions fm
  preds = predicates fm
  (monadic,other) = S.partition (\(_,ar) -> ar == 1) preds
  n = funpow (length monadic) (( * ) 2) 1

-- Decision procedure in principle for formulas with finite model property.
{-
From Harrison:

Definition 5.1 A formula is said to have the finite model property for validity
precisely when it is valid in all models iff it is valid in all finite models.
Similarly, it is said to have the finite model property for satisfiability
precisely when it is satisfiable iff it is satisfiable in a finite model.

Theorem 5.3 If a formula p involves k distinct monadic predicates (pred-
icates of arity 1) and none of higher arity (in particular, not equality) and
also involves no function symbols, then p has a model iff it has a model of size 2k.
-}
decide_fmp :: Formula FOL -> Bool
decide_fmp fm = test 1
 where test n = either (const (if decide_finite n fm then test (n + 1) else False)) (const True) (sequence $ S.toList $ limited_meson n fm)

limited_meson n fm =
  let fm1 = askolemize (Not (generalize fm)) in
  S.map ((limmeson n) . list_conj) (simpdnf fm1)

limmeson n fm =
  let cls = simpcnf (specialize (pnf fm)) in
  let rules = itlist ((++) . contrapositives) (flatten cls) [] in
  mexpand rules [] FF Right (M.empty,n,0)

decide_finite :: (Enum b, Eq b, Num b) => b -> Formula FOL -> Bool
decide_finite n fm = all (\md -> holds md M.empty fm') interps
 where
  funcs = functions fm
  preds = predicates fm
  dom = [1..n]
  fints = alldepmappings funcs (allfunctions dom)
  pints = alldepmappings preds (allpredicates dom)
  interps = allpairs (\f p -> (dom,f,p)) fints pints
  fm' = generalize fm

alltuples :: (Eq a, Num a) => a -> [t] -> [[t]]
alltuples 0 l = [[]]
alltuples n l =
  let tups = alltuples (n - 1) l in
  allpairs (:) l tups

allmappings dom ran = itlist (\p -> allpairs (valmod p) ran) dom [undefined]

alldepmappings dom ran =
  itlist (\(p,n) -> allpairs (valmod p) (ran n)) dom [undefined]

allfunctions dom n = allmappings (alltuples n dom) dom

allpredicates dom n = allmappings (alltuples n dom) [False,True]

-- Section 5.4

aristotle1 = putStr $ unlines $ map fromRight $ map (\s -> consequent s >>= return . anglicize_syllogism) all_valid_syllogisms'
aristotle0 = putStr $ unlines $ map anglicize_syllogism all_valid_syllogisms

all_valid_syllogisms' = filter (((Right True) ==) . aedecide) all_possible_syllogisms'

all_possible_syllogisms' = map (\t -> Imp p t) all_possible_syllogisms
 where p = parseFOL "(exists x. P(x)) & (exists x. M(x)) & (exists x. S(x))"

all_valid_syllogisms = filter (((Right True) ==) . aedecide) all_possible_syllogisms

all_possible_syllogisms :: [Formula FOL]
all_possible_syllogisms =
  let sylltypes = [premiss_A, premiss_E, premiss_I, premiss_O] in
  let prems1 = allpairs id sylltypes [("M","P"), ("P","M")] in
  let prems2 = allpairs id sylltypes [("S","M"), ("M","S")] in
  let prems3 = allpairs id sylltypes [("S","P")] in
  allpairs Imp (allpairs And prems1 prems2) prems3

anglicize_syllogism :: Formula FOL -> [Char]
anglicize_syllogism (Imp (And t1 t2) t3) =
  "If " ++ anglicize_premiss t1 ++ " and " ++ anglicize_premiss t2 ++
  ", then " ++ anglicize_premiss t3

anglicize_premiss :: Formula FOL -> [Char]
anglicize_premiss (Forall _ (Imp (Atom (R p _)) (Atom (R q _)))) = "all "++p++" are "++q
anglicize_premiss (Forall _ (Imp (Atom (R p _)) (Not (Atom (R q _))))) = "no "++p++" are "++q
anglicize_premiss (Exists _ (And (Atom (R p _)) (Atom (R  q _)))) = "some "++p++" are "++q
anglicize_premiss (Exists _ (And (Atom (R p _)) (Not (Atom (R  q _))))) = "some "++p++" are not "++q

atom p x = Atom (R p [Var x])

premiss_A (p,q) = Forall "x" (Imp (atom p "x") (atom q "x"))
premiss_E (p,q) = Forall "x" (Imp (atom p "x") (Not (atom q "x")))
premiss_I (p,q) = Exists "x" (And (atom p "x") (atom q "x"))
premiss_O (p,q) = Exists "x" (And (atom p "x") (Not (atom q "x")))

-- Section 5.3

wang :: Formula FOL -> Either String Bool
wang fm = aedecide (miniscope (nnf (simplify fm)))

miniscope :: Formula FOL -> Formula FOL
miniscope (Not p) = Not (miniscope p)
miniscope (And p q) = And (miniscope p) (miniscope q)
miniscope (Or p q) = Or (miniscope p) (miniscope q)
miniscope (Forall x p) = Not (pushquant x (Not (miniscope p)))
miniscope (Exists x p) = pushquant x (miniscope p)
miniscope fm = fm

pushquant :: String -> Formula FOL -> Formula FOL
pushquant x p
  | S.notMember x (fv p) = p
  | otherwise = let djs = purednf (nnf p) in list_disj (S.map (separate x) djs)

separate :: String -> S.Set (Formula FOL) -> Formula FOL
separate x cjs
  | null yes = list_conj no
  | null no = Exists x (list_conj yes)
  | otherwise = And (Exists x (list_conj yes)) (list_conj no)
 where
  (yes,no) = S.partition ((S.member x) . fv) cjs

-- Section 5.2

aedecide :: Formula FOL -> Either String Bool
aedecide fm
  | not (null funcs) = Left "Not decidable"
  | otherwise = Right (not (dpll (S.unions grounds)))
 where
  sfm = skolemize (Not fm)
  fvs = fv sfm
  (cnsts,funcs) = S.partition (\(_,ar) -> ar == 0) (functions sfm)
  consts = if null cnsts then [("c",0)] else (S.toList cnsts)
  cntms = map (\(c,_) -> Fn c []) consts
  alltuples = groundtuples cntms [] 0 (length fvs)
  cjs = simpcnf sfm
  grounds = map (\tup -> S.map (S.map (subst (fpf (S.toList fvs) tup))) cjs) alltuples
