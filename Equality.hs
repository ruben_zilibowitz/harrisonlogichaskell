module Equality where

import Prelude hiding (negate,sum,pred)
import qualified Data.Set as S
import Data.List (intercalate,minimumBy,maximumBy,find,partition,delete)
import Data.Maybe
import qualified Data.Map as M
import Debug.Trace

import PropositionalLogic hiding (nnf)
import FirstOrderLogic hiding (replace)
import Types
import Failing
import Parser
import DP

-- Section 4.9

-- test this with "paramodulation dij"
dij = parseFOL "(forall x. f(f(x)) = f(x)) & (forall x. exists y. f(y) = x) ==> forall x. f(x) = x"

paramodulation :: Formula FOL -> S.Set (Failing Bool)
paramodulation fm =
  let fm1 = askolemize (Not (generalize fm)) in
  S.map (pure_paramodulation . list_conj) (simpdnf fm1)

pure_paramodulation :: Formula FOL -> Failing Bool
pure_paramodulation fm = paraloop ([],flatten $ S.insert (S.singleton (mk_eq (Var "x") (Var "x"))) (simpcnf (specialize (pnf fm))))

paraloop :: ([[Formula FOL]], [[Formula FOL]]) -> Failing Bool
paraloop (used,[]) = failure "no proof found"
paraloop (used,cls:ros) = trace ((show (length used)) ++ " used; " ++ (show (length ros + 1)) ++ " unused.")
   (if any null news then return True else paraloop (used',itlist (incorporate cls) news ros))
 where
  used' = setify (cls : used)
  news = (concat (map ((map S.toList) . (resolve_clauses cls)) used')) ++ (concat (map (para_clauses cls) used'))

para_clauses :: [Formula FOL] -> [Formula FOL] -> [[Formula FOL]]
para_clauses cls1 cls2 =
  let cls1' = rename "x" cls1 in
  let cls2' = rename "y" cls2 in
  (paramodulate cls1' cls2') ++ (paramodulate cls2' cls1')

paramodulate :: [Formula FOL] -> [Formula FOL] -> [[Formula FOL]]
paramodulate pcl ocl = itlist (\eq ->
   let pcl' = delete eq pcl in
   let (l,r) = fromSuccess (dest_eq eq) in
   let rfn i ocl' = setify (map (subst i) (pcl' ++ ocl')) in
   (overlapc (l,r) ocl rfn) . (overlapc (r,l) ocl rfn)) (filter is_eq pcl) []

overlapc (l,r) cl rfn acc = listcases (\a b -> fromSuccess (overlapl (l,r) a b)) rfn cl acc

overlapl (l,r) (Atom (R f args)) rfn = return $ listcases (overlaps (l,r)) (\i a -> rfn i (Atom (R f a))) args []
overlapl (l,r) (Not p) rfn = overlapl (l,r) p (\i p -> rfn i (Not p))
overlapl _ _ _ = failure "overlapl: not a literal"

-- Section 4.8

test = parseFOL "(forall x y z. x * (y * z) = (x * y) * z) & (forall x. e * x = x) & (forall x. i(x) * x = e) ==> forall x. x * i(x) = e"

emeson :: Formula FOL -> S.Set Int
emeson fm = meson (equalitize fm)

bmeson :: Formula FOL -> [Int]
bmeson fm = let fm1 = askolemize (Not (generalize fm)) in map (bpuremeson . list_conj ) (flatten (simpdnf fm1))

bpuremeson :: Formula FOL -> Int
bpuremeson fm =
  let cls = brand (flatten (simpcnf (specialize (pnf fm)))) in
  let rules = itlist ((++) . contrapositives) cls [] in
  deepen (\n -> mexpand rules [] FF return (M.empty,n,0) >> return n) 0

brand :: [[Formula FOL]] -> [[Formula FOL]]
brand cls = [mk_eq (Var "x") (Var "x")] : (map modify_T cls2)
 where
  cls1 = map modify_E cls
  cls2 = setify (itlist ((++) . modify_S) cls1 [])

emodify :: S.Set String -> [Formula FOL] -> [Formula FOL]
emodify fvs cls = try (tryfind find_nvsubterm cls >>= return . good) cls
 where good t = let w = variant "w" fvs in
                let cls' = map (replace (M.singleton t (Var w))) cls in
                emodify (S.insert w fvs) (Not (mk_eq t (Var w)) : cls')

modify_E :: [Formula FOL] -> [Formula FOL]
modify_E cls = emodify (itlist (S.union . fv) cls S.empty) cls

replacet :: M.Map Term Term -> Term -> Term
replacet rfn tm
 | M.member tm rfn = rfn M.! tm
 | otherwise = case tm of
    (Fn f args) -> Fn f (map (replacet rfn) args)
    _ -> tm

replace :: M.Map Term Term -> Formula FOL -> Formula FOL
replace rfn = onformula (replacet rfn)

find_nvsubterm :: Formula FOL -> Failing Term
find_nvsubterm (Atom (R "=" [s,t])) = tryfind find_nestnonvar [s,t]
find_nvsubterm (Atom (R p args)) = maybe (failure "findnvsubt") return (find is_nonvar args)
find_nvsubterm (Not p) = find_nvsubterm p

find_nestnonvar :: Term -> Failing Term
find_nestnonvar (Var _) = failure "findnvsubt"
find_nestnonvar (Fn f args) = maybe (failure "findnvsubt") return (find is_nonvar args)

is_nonvar :: Term -> Bool
is_nonvar (Var _) = False
is_nonvar _ = True

modify_T :: [Formula FOL] -> [Formula FOL]
modify_T ((Atom (R "=" [s,t])):ps) =
  let eq = Atom (R "=" [s,t]) in
  let ps' = modify_T ps in
  let w = Var (variant "w" (itlist (S.union . fv) ps' (fv eq))) in
  (Not (mk_eq t w)) : (mk_eq s w) : ps'
modify_T (p:ps) = p : (modify_T ps)
modify_T [] = []

modify_S :: [Formula FOL] -> [[Formula FOL]]
modify_S cl = try (tryfind dest_eq cl >>= return . good) [cl]
 where
  good (s,t) = (map (eq1 :) sub) ++ (map (eq2 :) sub)
   where
    eq1 = mk_eq s t
    eq2 = mk_eq t s
    sub = modify_S (delete eq1 cl)

-- Section 4.7

anothertest = (meson2 . equalitize) (parseFOL "(forall x y z. x * y = x * z ==> y = z) <==> (forall x z. exists w. forall y. z = x * y ==> w = y)")

complete_and_simplify :: [String] -> [Formula FOL] -> [Formula FOL]
complete_and_simplify wts eqs = ((interreduce []) . (complete ord)) (eqs',[],setify $ concat (allpairs critical_pairs eqs' eqs'))
 where
  ord = lpo_ge (weight wts)
  eqs' = map (\e -> let (l,r) = fromSuccess (normalize_and_orient ord [] e) in mk_eq l r) eqs

interreduce :: [Formula FOL] -> [Formula FOL] -> [Formula FOL]
interreduce dun eqs@((Atom (R "=" [l,r])):oeqs) = let dun' = if rewrite (dun ++ oeqs) l /= l then dun else mk_eq l (rewrite (dun ++ eqs) r):dun in interreduce dun' oeqs
interreduce dun [] = reverse dun

grouptest1 = rewrite grouptest0 (parseFOLTerm  "i(x * i(x)) * (i(i((y * z) * u) * y) * i(u))")

grouptest0 = eqs'
 where
  eqs' = complete ord (eqs,[],setify $ concat (allpairs critical_pairs eqs eqs))
  ord = lpo_ge (weight ["1", "*", "i"])
  eqs = (map parseFOL ["1 * x = x", "i(x) * x = 1","(x * y) * z = x * (y * z)"])

maybeTrace (Just str) x = trace str x
maybeTrace Nothing x = x

complete ord (eqs,def,eq:ocrits) = maybeTrace (status trip eqs) complete ord trip
 where
  trip = try (normalize_and_orient ord eqs eq >>= return . norm) (eqs,eq:def,ocrits)
  norm (s',t')
     | s' == t' = (eqs,def,ocrits)
     | otherwise = let eq' = Atom (R "=" [s',t']) in
                   let eqs' = eq':eqs in
                   (eqs',def,ocrits ++ itlist ((++) . critical_pairs eq') eqs' [])

complete ord (eqs,def,[])
 | null def = eqs
 | otherwise = let e = fromJust $ find (isSuccess . (normalize_and_orient ord eqs)) def in complete ord (eqs,delete e def,[e])

status (eqs,def,crs) eqs0 =
  if (eqs == eqs0) && ((length crs) `mod` 1000 /= 0) then Nothing
  else Just $ (show (length eqs) ++ " equations and " ++ show (length crs) ++ " pending critical pairs + " ++ show (length def) ++ " deferred")

normalize_and_orient
  :: (Term -> Term -> Bool)
     -> [Formula FOL] -> Formula FOL -> Failing (Term, Term)
normalize_and_orient ord eqs (Atom (R "=" [s,t]))
  | ord s' t' = return (s',t')
  | ord t' s' = return (t',s')
  | otherwise = failure "Can't orient equation"
 where
  s' = rewrite eqs s
  t' = rewrite eqs t

crit1 :: Formula FOL -> Formula FOL -> [Formula FOL]
crit1 (Atom (R "=" [l1,r1])) (Atom (R "=" [l2,r2])) = overlaps (l1,r1) l2 (\i t -> subst i (mk_eq t r2))

critical_pairs :: Formula FOL -> Formula FOL -> [Formula FOL]
critical_pairs fma fmb =
  let (fm1,fm2) = renamepair (fma,fmb) in
  if fma == fmb then crit1 fm1 fm2
  else setify ((crit1 fm1 fm2) ++ (crit1 fm2 fm1))

listcases :: (b -> (u -> b -> t) -> [a]) -> (u -> [b] -> t) -> [b] -> [a] -> [a]
listcases fn rfn [] acc = acc
listcases fn rfn (h:t) acc = (fn h (\i h' -> rfn i (h':t))) ++ (listcases fn (\i t' -> rfn i (h:t')) t acc)

overlaps :: (Term, Term) -> Term -> (M.Map String Term -> Term -> a) -> [a]
overlaps (l,r) tm@(Fn f args) rfn = listcases (overlaps (l,r)) (\i a -> rfn i (Fn f a)) args (try (fullunify [(l,tm)] >>= return . (: []) . flip rfn r) [])
overlaps (l,r) (Var x) rfn = []

renamepair :: (Formula FOL, Formula FOL) -> (Formula FOL, Formula FOL)
renamepair (fm1,fm2) =
  let fvs1 = fv fm1 in
  let fvs2 = fv fm2 in
  let (nms1,nms2) = splitAt (length fvs1) (map (\n -> Var ("x"++(show n))) [0 .. (length fvs1 + length fvs2 - 1)]) in
  (subst (fpf (S.toList fvs1) nms1) fm1, subst (fpf (S.toList fvs2) nms2) fm2)

-- Section 4.6

weight :: (Eq b, Ord a) => [b] -> (b, a) -> (b, a) -> Bool
weight lis (f,n) (g,m) = if f == g then n > m else earlier lis g f

lpo_ge :: ((String, Int) -> (String, Int) -> Bool) -> Term -> Term -> Bool
lpo_ge w s t = (s == t) || lpo_gt w s t

lpo_gt :: ((String, Int) -> (String, Int) -> Bool) -> Term -> Term -> Bool
lpo_gt w s t@(Var x) = (s /= t) && S.member x (fvt s)
lpo_gt w s@(Fn f fargs) t@(Fn g gargs) =
        any (\si -> lpo_ge w si t) fargs ||
        all (lpo_gt w s) gargs &&
        (f == g && lexord (lpo_gt w) fargs gargs ||
         w (f,length fargs) (g,length gargs))
lpo_gt _ _ _ = False

lexord :: Eq a => (a -> a -> Bool) -> [a] -> [a] -> Bool
lexord ord (h1:t1) (h2:t2) = if ord h1 h2 then length t1 == length t2 else h1 == h2 && lexord ord t1 t2
lexord _ _ _ = False

termsize :: Num a => Term -> a
termsize (Var x) = 1
termsize (Fn f args) = itlist (\t n -> termsize t + n) args 1

-- Section 4.5

rewriteTest = rewrite (map parseFOL ["0 + x = x", "S(x) + y = S(x + y)", "0 * x = 0", "S(x) * y = y + x * y"]) (parseFOLTerm "S(S(S(0))) * S(S(0)) + S(S(S(S(0))))")

rewrite :: [Formula FOL] -> Term -> Term
rewrite eqs tm = try (rewrite1 eqs tm >>= return . rewrite eqs) blah
 where blah = case tm of
               (Var x) -> tm
               (Fn f args) -> let tm' = Fn f (map (rewrite eqs) args)
                              in if tm' == tm then tm else rewrite eqs tm'

rewrite1 :: [Formula FOL] -> Term -> Failing Term
rewrite1 ((Atom (R "=" [l,r])) : oeqs) t = tryM (term_match M.empty [(l,t)] >>= return . (flip tsubst r)) (rewrite1 oeqs t)
rewrite1 _ _ = failure "rewrite1"

-- Section 4.4

eq_eg2 = parseFOL "f(f(f(f(c)))) = c & f(f(c)) = c ==> f(c) = c"
eq_eg1 = parseFOL "f(f(f(f(f(c))))) = c & f(f(f(c))) = c ==> f(c) = c | f(g(c)) = g(f(c))"

ccvalid :: Formula FOL -> Failing Bool
ccvalid fm = do
   let fms = simpdnf (askolemize (Not (generalize fm)))
   sat <- mapM (ccsatisfiable . S.toList) (S.toList fms)
   return (not (or sat))

ccsatisfiable :: [Formula FOL] -> Failing Bool
ccsatisfiable fms = do
   let (pos,neg) = partition positive fms
   eqps <- mapM dest_eq pos
   eqns <- mapM (dest_eq . negate) neg
   let lrs = (map fst eqps) ++ (map snd eqps) ++ (map fst eqns) ++ (map snd eqns)
   let pfn = itlist predecessors (setify (concatMap subterms lrs)) M.empty
   let (eqv,_) = itlist emerge eqps (unequal,pfn)
   return (all (\(l,r) -> not (equivalent eqv l r)) eqns)

predecessors :: Term -> M.Map Term [Term] -> M.Map Term [Term]
predecessors t pfn =
  case t of
    Fn f a -> itlist (\s f -> (M.insert s (setify (t : (tryapplyl f s))) f)) (setify a) pfn
    _ -> pfn

emerge :: (Term, Term) -> (Partition Term, M.Map Term [Term]) -> (Partition Term, M.Map Term [Term])
emerge (s,t) (eqv,pfn)
 | s' == t' = (eqv,pfn)
 | otherwise = itlist (\(u,v) (eqv,pfn) -> if congruent eqv (u,v) then emerge (u,v) (eqv,pfn) else (eqv,pfn)) (allpairs (,) sp tp) (eqv',pfn')
 where
  s' = canonize eqv s
  t' = canonize eqv t
  sp = tryapplyl pfn s'
  tp = tryapplyl pfn t'
  eqv' = equate (s,t) eqv
  st' = canonize eqv' s'
  pfn' = M.insert st' (setify (sp ++ tp)) pfn

congruent :: Partition Term -> (Term, Term) -> Bool
congruent eqv (Fn f a1, Fn g a2) = f == g && and (zipWith (equivalent eqv) a1 a2)
congruent _ _ = False

-- Section 4.3

subterms :: Term -> [Term]
subterms (Fn f args) = (Fn f args) : (concatMap subterms args)
subterms tm = [tm]

-- Section 4.1

is_eq (Atom (R "=" _)) = True
is_eq _ = False

mk_eq s t = Atom (R "=" [s,t])

dest_eq (Atom (R "=" [s,t])) = return (s,t)
dest_eq _ = failure "dest_eq: not an equation"

lhs eq = (dest_eq eq) >>= return . fst
rhs eq = (dest_eq eq) >>= return . snd

predicates fm = atom_union (\(R p a) -> (p,length a)) fm

function_congruence :: (String, Int) -> Maybe (Formula FOL)
function_congruence (f,0) = Nothing
function_congruence (f,n) = Just (itlist Forall (argnames_x ++ argnames_y) (Imp ant con))
 where
  argnames_x = map (\k -> "x" ++ (show k)) [1..n]
  argnames_y = map (\k -> "y" ++ (show k)) [1..n]
  args_x = map Var argnames_x
  args_y = map Var argnames_y
  ant = end_itlist And (zipWith mk_eq args_x args_y)
  con = mk_eq (Fn f args_x) (Fn f args_y)

predicate_congruence :: (String, Int) -> Maybe (Formula FOL)
predicate_congruence (p,0) = Nothing
predicate_congruence (p,n) = Just (itlist Forall (argnames_x ++ argnames_y) (Imp ant con))
 where
  argnames_x = map (\k -> "x" ++ (show k)) [1..n]
  argnames_y = map (\k -> "y" ++ (show k)) [1..n]
  args_x = map Var argnames_x
  args_y = map Var argnames_y
  ant = end_itlist And (zipWith mk_eq args_x args_y)
  con = Imp (Atom (R p args_x)) (Atom (R p args_y))

equivalence_axioms :: [Formula FOL]
equivalence_axioms = map parseFOL ["forall x. x = x", "forall x y z. x = y & x = z ==> y = z"]

equalitize :: Formula FOL -> Formula FOL
equalitize fm
  | (S.notMember ("=",2) allpreds) = fm
  | otherwise = Imp (end_itlist And axioms) fm
 where
  allpreds = predicates fm
  preds = S.delete ("=",2) allpreds
  funcs = functions fm
  axioms = equivalence_axioms ++ (catMaybes$S.toList ((S.map function_congruence funcs) `S.union` (S.map predicate_congruence preds)))

ewd = equalitize $ parseFOL "(forall x. f(x) ==> g(x)) & (exists x. f(x)) & (forall x y. g(x) & g(y) ==> x = y) ==> forall y. g(y) ==> f(y)"

wishnu = equalitize $ parseFOL "(exists x. x = f(g(x)) & forall x2 . x2 = f(g(x2)) ==> x = x2) <==> (exists y. y = g(f(y)) & forall y2 . y2 = g(f(y2)) ==> y = y2)"
