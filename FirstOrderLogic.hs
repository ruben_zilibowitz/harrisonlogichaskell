module FirstOrderLogic where

import Prelude hiding (negate,sum,pred)
import qualified Data.Set as S
import Data.List (intercalate,minimumBy,maximumBy,find,partition,delete)
import Data.Maybe
import qualified Data.Map as M
import Debug.Trace

import PropositionalLogic hiding (nnf)
import Types
import Failing
import Parser
import DP

-- given a list of hypotheses and a conclusion
-- generate the formula that expresses the conclusion as
-- a consequence of the hypotheses.
hypothesesConclusion :: [String] -> String -> Formula FOL
hypothesesConclusion hyps conc = Imp (foldl1 And (map parseFOL hyps)) (parseFOL conc)

-- Special case of applying a subfunction to the top *terms*.
onformula :: (Term -> Term) -> Formula FOL -> Formula FOL
onformula f = onatoms (\(R p a) -> Atom (R p (map f a)))

-- Section 3.15

steamroller = parseFOL "((forall x. P1(x) ==> P0(x)) & (exists x. P1(x))) & ((forall x. P2(x) ==> P0(x)) & (exists x. P2(x))) & ((forall x. P3(x) ==> P0(x)) & (exists x. P3(x))) & ((forall x. P4(x) ==> P0(x)) & (exists x. P4(x))) & ((forall x. P5(x) ==> P0(x)) & (exists x. P5(x))) & ((exists x. Q1(x)) & (forall x. Q1(x) ==> Q0(x))) & (forall x. P0(x)  ==> (forall y. Q0(y) ==> R(x,y)) | ((forall y. P0(y) & S0(y,x) &   (exists z. Q0(z) & R(y,z)) ==> R(x,y)))) & (forall x y. P3(y) & (P5(x) | P4(x)) ==> S0(x,y)) & (forall x y. P3(x) & P2(y) ==> S0(x,y)) & (forall x y. P2(x) & P1(y) ==> S0(x,y)) & (forall x y. P1(x) & (P2(y) | Q1(y)) ==> ~(R(x,y))) & (forall x y. P3(x) & P4(y) ==> R(x,y)) & (forall x y. P3(x) & P5(y) ==> ~(R(x,y))) & (forall x. (P4(x) | P5(x)) ==> exists y. Q0(y) & R(x,y)) ==> exists x y. P0(x) & P0(y) &  exists z. Q1(z) & R(y,z) & R(x,y)"

meson2 :: Formula FOL -> S.Set Int
meson2 fm =
  let fm1 = askolemize (Not (generalize fm)) in
  S.map (puremeson2 . list_conj) (simpdnf fm1)

puremeson2 fm = deepen (\n -> mexpand2 rules [] FF return (M.empty,n,0) >> return n) 0
 where
  cls = simpcnf (specialize (pnf fm))
  rules = itlist ((++) . contrapositives) (flatten cls) []

mexpands rules ancestors gs cont (env,n,k)
  | n < 0 = failure "Too deep"
  | m <= 1 = itlist (mexpand rules ancestors) gs cont (env,n,k)
  | otherwise = tryM (expfn goals1 n1 goals2 n2 (-1) cont env k) second
  where
   m = length gs
   n1 = div n 2
   n2 = n - n1
   (goals1,goals2) = splitAt (div m 2) gs
   expfn = expand2 (mexpands rules ancestors)
   second = expfn goals2 n1 goals1 n2 n1 cont env k

mexpand2 rules ancestors g cont (env,n,k)
  | n < 0 = failure "Too deep"
  | any (equal env g) ancestors = failure "repetition"
  | otherwise = tryM (tryfind firstCheck ancestors) (tryfind secondCheck rules)
 where
  firstCheck a = do
    ul <- unify_literals env (g,negate a)
    c <- cont (ul,n,k)
    return c
  secondCheck rule = do
    let (Prolog asm c,k') = renamerule k rule
    ul <- unify_literals env (g,c)
    b <- mexpands rules (g:ancestors) asm cont (ul,n-(length asm),k')
    return b

expand2 expfn goals1 n1 goals2 n2 n3 cont env k =
   expfn goals1 (\(e1,r1,k1) ->
        expfn goals2 (\(e2,r2,k2) ->
                        if n2 + r1 <= n3 + r2 then failure "pair"
                        else cont (e2,r2,k2))
              (e1,n2+r1,k1))
        (env,n1,k)

equal :: M.Map String Term -> Formula FOL -> Formula FOL -> Bool
equal env fm1 fm2 = unify_literals env (fm1,fm2) == return env

--

meson :: Formula FOL -> S.Set Int
meson fm =
  let fm1 = askolemize (Not (generalize fm)) in
  S.map (puremeson . list_conj) (simpdnf fm1)

puremeson :: Formula FOL -> Int
puremeson fm = deepen (\n -> mexpand rules [] FF return (M.empty,n,0) >> return n) 0
 where
  cls = simpcnf (specialize (pnf fm))
  rules = itlist ((++) . contrapositives) (flatten cls) []

mexpand rules ancestors g cont (env,n,k)
  | n < 0 = failure "Too deep"
  | otherwise = tryM (tryfind firstCheck ancestors) (tryfind secondCheck rules)
   where
     firstCheck a = do
       ul <- unify_literals env (g,negate a)
       c <- cont (ul,n,k)
       return c
     secondCheck rule = do
       let (Prolog asm c,k') = renamerule k rule
       ul <- unify_literals env (g,c)
       b <- itlist (mexpand rules (g:ancestors)) asm cont (ul,n-(length asm),k')
       return b

contrapositives :: [Formula FOL] -> [PrologRule]
contrapositives cls =
  let base = map (\c -> (Prolog (map negate (delete c cls)) c)) cls in
  if all negative cls then (Prolog (map negate cls) FF) : base else base

-- Section 3.14

test7 = prolog reverserules "reverse(0::1::2::3::nil,R)"

reverserules = appendrules ++ ["reverse(nil,nil)","reverse(H::T,L) :- reverse(T,R),append(R,H::nil,L)"]

test6 = prolog sortrules "sort(S(S(S(S(0))))::S(0)::0::S(S(0))::S(0)::nil,X)"

sortrules =
 ["sort(X,Y) :- perm(X,Y),sorted(Y)",
  "sorted(nil)",
  "sorted(X::nil)",
  "sorted(X::Y::Z) :- X <= Y, sorted(Y::Z)",
  "perm(nil,nil)",
  "perm(X::Y,U::V) :- delete(U,X::Y,Z), perm(Z,V)",
  "delete(X,X::Y,Y)",
  "delete(X,Y::Z,Y::W) :- delete(X,Z,W)",
  "0 <= X",
  "S(X) <= S(Y) :- X <= Y"]

test5 = prolog appendrules "append(X,3::4::nil,X)"   -- loops indefinitely
test4 = prolog appendrules "append(X,Y,1::2::3::4::nil)"
test3 = prolog appendrules "append(X,3::4::nil,1::2::3::4::nil)"
test2 = prolog appendrules "append(1::2::nil,Y,1::2::3::4::nil)"
test1 = prolog appendrules "append(1::2::nil,3::4::nil,Z)"

appendrules = ["append(nil,L,L)", "append(H::T,L,H::A) :- append(T,L,A)"]

test0 = prolog lerules "LE(S(S(<zero>)),X)"

prolog :: [String] -> String -> Failing [Formula FOL]
prolog rules gl = do
  sp <- simpleprolog rules gl
  let i = solve sp
  return (mapfilter (\x -> do
    y <- M.lookup x i
    return (Atom (R "=" [Var x, y]))) (S.toList $ fv (parseFOL gl)))

test01 = simpleprolog lerules "LE(S(S(<zero>)) , S(<zero>))"
test00 = simpleprolog lerules "LE(S(S(<zero>)) , S(S(S(<zero>))))"

lerules = ["0 <= X", "S(X) <= S(Y) :- X <= Y"]

simpleprolog :: [String] -> String -> Failing (M.Map String Term)
simpleprolog rules gl = backchain (map parseProlog rules) (-1) 0 M.empty [parseFOL gl]

p32 = parseFOL "(forall x. P(x) & (G(x) | H(x)) ==> Q(x)) & (forall x. Q(x) & H(x) ==> J(x)) & (forall x. R(x) ==> H(x)) ==> (forall x. P(x) & R(x) ==> J(x))"

hornprove :: (Eq b, Num b, Show b) => Formula FOL -> Failing b
hornprove fm = do
  rules <- mapM hornify (flatten $ simpcnf (skolemize (Not (generalize fm))))
  return (deepen (\n -> backchain rules n 0 M.empty [FF] >> return n) 0)

hornify :: [Formula FOL] -> Failing PrologRule
hornify cls =
  let (pos,neg) = partition positive cls in
  if length pos > 1 then (failure "non-Horn clause")
  else return (Prolog (map negate neg) (if (null pos) then FF else (head pos)))

backchain
  :: (Eq a, Num a) =>
     [PrologRule]
     -> a
     -> Int
     -> M.Map String Term
     -> [Formula FOL]
     -> Failing (M.Map String Term)
backchain rules _ _ env [] = return env
backchain rules 0 _ _ (_:_) = failure "Too deep"
backchain rules n k env (g:gs) =
   tryfind (\rule -> let (Prolog a c,k') = renamerule k rule in do
      ul <- unify_literals env (c,g)
      b <- backchain rules (n - 1) k' ul (a ++ gs)
      return b) rules

renamerule
  :: Int
     -> PrologRule
     -> (PrologRule, Int)
renamerule k (Prolog asm c) = (Prolog (map inst asm) (inst c),k+n)
 where
  fvs = fv (list_conj (c:asm))
  n = length fvs
  vvs = map (\i -> "_" ++ (show i)) [k .. (k+n-1)]
  inst = subst (fpf (S.toList fvs) (map Var vvs))

-- Section 3.13

resolution3 :: Formula FOL -> S.Set (Failing Bool)
resolution3 fm = S.map (pure_resolution3 . list_conj) (simpdnf fm1)
 where fm1 = askolemize (Not (generalize fm))

pure_resolution3 fm = resloop2 (flatten x, flatten y)
 where
  (x,y) = S.partition (any positive) (simpcnf (specialize (pnf fm)))

los = parseFOL "(forall x y z. P(x,y) & P(y,z) ==> P(x,z)) & (forall x y z. Q(x,y) & Q(y,z) ==> Q(x,z)) & (forall x y. P(x,y) ==> P(y,x)) & (forall x y. P(x,y) | Q(x,y)) ==> (forall x y. P(x,y)) | (forall x y. Q(x,y))"

presolution :: Formula FOL -> S.Set (Failing Bool)
presolution fm = S.map (pure_presolution . list_conj) (simpdnf fm1)
 where fm1 = askolemize (Not (generalize fm))

pure_presolution fm = presloop ([],map S.toList $ S.toList $ simpcnf (specialize (pnf fm)))

presloop (used,[]) = failure "no proof found"
presloop (used,cls:ros) = trace ((show (length used)) ++ " used; " ++ (show (length ros + 1)) ++ " unused.")
   (if any null news then return True else presloop (used',itlist (incorporate cls) news ros))
 where
  used' = setify (cls : used)
  news = concat (map ((map S.toList) . (presolve_clauses cls)) used')

presolve_clauses :: [Formula FOL] -> [Formula FOL] -> [S.Set (Formula FOL)]
presolve_clauses cls1 cls2 =
  if (all positive cls1 || all positive cls2)
  then resolve_clauses cls1 cls2 else []

-- Section 3.12

resolution2 :: Formula FOL -> S.Set (Failing Bool)
resolution2 fm = S.map (pure_resolution2 . list_conj) (simpdnf fm1)
 where fm1 = askolemize (Not (generalize fm))

pure_resolution2 :: Formula FOL -> Failing Bool
pure_resolution2 fm = resloop2 ([],map S.toList $ S.toList $ simpcnf (specialize (pnf fm)))

resloop2 :: ([[Formula FOL]], [[Formula FOL]]) -> Failing Bool
resloop2 (used,[]) = failure "no proof found"
resloop2 (used,cls:ros) = trace ((show (length used)) ++ " used; " ++ (show (length ros + 1)) ++ " unused.")
   (if any null news then return True else resloop2 (used',itlist (incorporate cls) news ros))
 where
  used' = setify (cls : used)
  news = concat (map ((map S.toList) . (resolve_clauses cls)) used')

incorporate :: [Formula FOL] -> [Formula FOL] -> [[Formula FOL]] -> [[Formula FOL]]
incorporate gcl cl unused =
  if trivial (S.fromList cl) || any (\c -> subsumes_clause c cl) (gcl:unused)
  then unused else replace cl unused

replace :: [Formula FOL] -> [[Formula FOL]] -> [[Formula FOL]]
replace cl [] = [cl]
replace cl (c:cls) = if subsumes_clause cl c then (cl:cls) else c:(replace cl cls)

subsumes_clause :: [Formula FOL] -> [Formula FOL] -> Bool
subsumes_clause cls1 cls2 = isSuccess (subsume M.empty cls1)
 where
  subsume env [] = return env
  subsume env (l1:clt) = tryfind (\l2 -> do
    m <- match_literals env (l1,l2)
    s <- subsume m clt
    return s) cls2

match_literals
  :: M.Map String Term
     -> (Formula FOL, Formula FOL) -> Failing (M.Map String Term)
match_literals env (Atom (R p a1),Atom (R q a2)) = term_match env [(Fn p a1,Fn q a2)]
match_literals env (Not (Atom (R p a1)),Not (Atom (R q a2))) = term_match env [(Fn p a1,Fn q a2)]
match_literals _ _ = failure "match_literals"

term_match
  :: M.Map String Term
     -> [(Term, Term)] -> Failing (M.Map String Term)
term_match env [] = return env
term_match env ((Fn f fa,Fn g ga):oth) | f == g && length fa == length ga =
   term_match env ((zip fa ga) ++ oth)
term_match env ((Var x,t):oth) =
   if not (M.member x env) then term_match (M.insert x t env) oth
   else if env M.! x == t then term_match env oth
   else failure "term_match"
term_match _ _ = failure "term_match"

-- Section 3.11

-- use splittab ewd1062
ewd1062 = parseFOL "(forall x. LE(x,x)) & (forall x y z. LE(x,y) & LE(y,z) ==> LE(x,z)) & (forall x y. LE(f(x),y) <==> LE(x,g(y))) ==> (forall x y. LE(x,y) ==> LE(f(x),f(y))) & (forall x y. LE(x,y) ==> LE(g(x),g(y)))"

dpe = parseFOL "exists x. exists y. forall z. (F(x,y) ==> (F(y,z) & F(z,z))) & ((F(x,y) & G(x,y)) ==> (G(x,z) & G(z,z)))"

resolution :: Formula FOL -> S.Set (Failing Bool)
resolution fm = S.map (pure_resolution . list_conj) (simpdnf fm1)
 where fm1 = askolemize (Not (generalize fm))

pure_resolution fm = resloop ([],map S.toList $ S.toList $ simpcnf (specialize (pnf fm)))

resloop :: ([[Formula FOL]], [[Formula FOL]]) -> Failing Bool
resloop (used,[]) = failure "no proof found"
resloop (used,cl:ros) = trace ((show (length used)) ++ " used; " ++ (show (length ros + 1)) ++ " unused.")
   (if any null news then return True else resloop (used',ros++news))
 where
  used' = cl : used
  news = concat (map ((map S.toList) . (resolve_clauses cl)) used')

resolve_clauses
  :: [Formula FOL] -> [Formula FOL] -> [S.Set (Formula FOL)]
resolve_clauses cls1 cls2 = itlist (resolvents (S.fromList cls1') (S.fromList cls2')) cls1' []
 where
  cls1' = rename "x" cls1
  cls2' = rename "y" cls2

resolvents
  :: S.Set (Formula FOL)
     -> S.Set (Formula FOL)
     -> Formula FOL
     -> [S.Set (Formula FOL)]
     -> [S.Set (Formula FOL)]
resolvents cl1 cl2 p acc
  | null ps2 = acc
  | otherwise = itlist (\(s1,s2) sof -> flip try sof (do
      m <- mgu (S.toList (S.union s1 (S.map negate s2))) M.empty
      return ((S.map (subst m) (S.union (S.difference cl1 s1) (S.difference cl2 s2))) : sof))) pairs acc
 where
  ps2 = S.filter (unifiable (negate p)) cl2
  ps1 = S.filter (\q -> q /= p && unifiable p q) cl1
  pairs = allpairs (\ x y -> (S.fromList x,S.fromList y)) (map (p :) (allsubsets $ S.toList ps1)) (allnonemptysubsets $ S.toList ps2)

rename :: [Char] -> [Formula FOL] -> [Formula FOL]
rename pfx cls = map (subst (fpf (S.toList fvs) (S.toList vvs))) cls
 where
  fvs = fv (list_disj cls)
  vvs = S.map (\s -> Var (pfx ++ s)) fvs

unifiable :: Formula FOL -> Formula FOL -> Bool
unifiable p q = isSuccess (unify_literals M.empty (p,q))

mgu
  :: [Formula FOL]
     -> M.Map String Term -> Failing (M.Map String Term)
mgu (a:b:rest) env = unify_literals env (a,b) >>= mgu (b:rest)
mgu _ env = return (solve env)

-- Section 3.10

p34 = parseFOL "((exists x. forall y. P(x) <==> P(y)) <==> ((exists x. Q(x)) <==> (forall y. Q(y)))) <==> ((exists x. forall y. Q(x) <==> Q(y)) <==> ((exists x. P(x)) <==> (forall y. P(y))))"

splittab fm = map (tabrefute . S.toList) (S.toList $ simpdnf (askolemize (Not (generalize fm))))

p38 = parseFOL "(forall x. P(a) & (P(x) ==> (exists y. P(y) & R(x,y))) ==> (exists z w. P(z) & R(x,w) & R(w,z))) <==> (forall x. (~P(a) | P(x) | (exists z w. P(z) & R(x,w) & R(w,z))) & (~P(a) | ~(exists y. P(y) & R(x,y)) | (exists z w. P(z) & R(x,w) & R(w,z))))"

tab :: Formula FOL -> Int
tab fm =
  let sfm = askolemize (Not (generalize fm)) in
  if sfm == FF then 0 else tabrefute [sfm]

tabrefute :: (Num r, Ord r, Show r) => [Formula FOL] -> r
tabrefute fms = deepen (\n -> tableau (fms,[],n) return (M.empty,0) >> return n) 0

deepen :: (Num a, Show a) => (a -> Failing r) -> a -> r
deepen f n = trace ("Searching with depth limit " ++ (show n))
   (try (f n) (deepen f (n + 1)))

tableau (fms,lits,n) cont (env,k) =
  if n < 0 then failure "no proof at this level" else
  case fms of
    [] -> failure "tableau: no proof"
    (And p q):unexp ->
      tableau (p:q:unexp,lits,n) cont (env,k)
    (Or p q):unexp ->
      tableau (p:unexp,lits,n) (tableau (q:unexp,lits,n) cont) (env,k)
    (Forall x p):unexp ->
      let y = Var ("_" ++ (show k)) in
      let p' = subst (M.singleton x y) p in
      tableau (p':unexp++[Forall x p],lits,n-1) cont (env,k+1)
    fm:unexp ->
      try (tryfind (\l ->
              (do u <- unify_complements env (fm,l)
                  v <- cont (u,k)
                  return (return v))) lits) (tableau (unexp,fm:lits,n) cont (env,k))

prawitz :: Formula FOL -> Int
prawitz fm = snd (prawitz_loop (simpdnf fm0) (S.toList $ fv fm0) (S.singleton S.empty) 0)
 where fm0 = skolemize (Not (generalize fm))

prawitz_loop djs0 fvs djs n =
   (try (unify_refute (map S.toList (S.toList djs1)) M.empty >>= \x -> return (x,n+1)) (prawitz_loop djs0 fvs djs1 (n+1)))
 where
  l = length fvs
  newvars = map (\k -> "_" ++ (show (n * l + k))) [1..l]
  inst = fpf fvs (map Var newvars)
  djs1 = distrib (S.map (S.map (subst inst)) djs0) djs

unify_refute :: [[Formula FOL]] -> M.Map String Term -> Failing (M.Map String Term)
unify_refute [] env = return env
unify_refute (d : odjs) env = tryfind (\x -> (unify_complements env x) >>= (unify_refute odjs) >>= return) (allpairs (,) pos neg)
 where (pos,neg) = partition positive d

unify_complements
  :: M.Map String Term
     -> (Formula FOL, Formula FOL) -> Failing (M.Map String Term)
unify_complements env (p,q) = unify_literals env (p,negate q)

unify_literals
  :: M.Map String Term
     -> (Formula FOL, Formula FOL) -> Failing (M.Map String Term)
unify_literals env (Atom (R p1 a1),Atom (R p2 a2)) = unify env [(Fn p1 a1,Fn p2 a2)]
unify_literals env (Not p,Not q) = unify_literals env (p,q)
unify_literals env (FF,FF) = return env
unify_literals env _ = failure "Can't unify literals"

-- Section 3.9

unify_and_apply :: [(Term, Term)] -> Failing [(Term, Term)]
unify_and_apply eqs = do
  i <- fullunify eqs
  let apply (t1,t2) = (tsubst i t1,tsubst i t2)
  return (map apply eqs)

fullunify :: [(Term, Term)] -> Failing (M.Map String Term)
fullunify eqs = do
  u <- unify M.empty eqs
  return (solve u)

solve :: M.Map String Term -> M.Map String Term
solve env = if (env' == env) then env else solve env'
 where
  env' = M.map (tsubst env) env

unify :: M.Map String Term -> [(Term, Term)] -> Failing (M.Map String Term)
unify env [] = return env
unify env ((Fn f fargs,Fn g gargs) : oth) =
   if f == g && length fargs == length gargs
   then unify env ((zip fargs gargs) ++ oth)
   else failure "impossible unification"
unify env ((Var x,t) : oth) =
   if M.member x env then unify env ((env M.! x,t) : oth)
   else do
     z <- istriv env x t
     w <- unify (if z then env else M.insert x t env) oth
     return w
unify env ((t,Var x) : oth) = unify env ((Var x,t) : oth)

istriv :: M.Map String Term -> String -> Term -> Failing Bool
istriv env x (Var y)
 | y == x = return True
 | M.member y env = istriv env x (env M.! y)
 | otherwise = return False
istriv env x (Fn f args) = do
   a <- sequence (map (istriv env x) args)
   if (or a) then failure "cyclic" else return False

-- Section 3.8

p29 = parseFOL "(exists x. P(x)) & (exists x. G(x)) ==> ((forall x. P(x) ==> H(x)) & (forall x. G(x) ==> J(x)) <==> (forall x y. P(x) & G(y) ==> H(x) & J(y)))"
p36 = parseFOL "(forall x. exists y. P(x,y)) & (forall x. exists y. G(x,y)) & (forall x y. P(x,y) | G(x,y) ==> (forall z. P(y,z) | G(y,z) ==> H(x,z))) ==> (forall x. exists y. H(x,y))"

dp_refine_loop cjs0 cntms funcs fvs n cjs tried tuples =
  let tups = dp_loop cjs0 cntms funcs fvs n cjs tried tuples in
  dp_refine cjs0 fvs tups []

dp_refine cjs0 fvs [] need = need
dp_refine cjs0 fvs (cl : dknow) need = dp_refine cjs0 fvs dknow need'
 where
  mfn = (dp_mfn cjs0) . subst . (fpf fvs)
  need' = if dpll (itlist mfn (need ++ dknow) S.empty) then cl : need else need

davisputnam' = davisputnam_withloop dp_refine_loop
davisputnam = davisputnam_withloop dp_loop

davisputnam_withloop loop fm =
  let sfm = skolemize (Not (generalize fm)) in
  let fvs = fv sfm in
  let (consts,funcs) = herbfuns sfm in
  let cntms = S.map (\(c,_) -> Fn c []) consts in
  length (loop (simpcnf sfm) (S.toList cntms) funcs (S.toList fvs) 0 S.empty [] [])

dp_loop = herbloop dp_mfn dpll

dp_mfn
  :: Ord b =>
     S.Set (S.Set a) -> (a -> b) -> S.Set (S.Set b) -> S.Set (S.Set b)
dp_mfn cjs0 ifn cjs = S.union (S.map (S.map ifn) cjs0) cjs

p20 = parseFOL "(forall x y. exists z. forall w. P(x) & Q(y) ==> R(z) & U(w)) ==> (exists x y. P(x) & Q(y)) ==> (exists z. R(z))"
p45 = parseFOL "(forall x. P(x) & (forall y. G(y) & H(x,y) ==> J(x,y)) ==> (forall y. G(y) & H(x,y) ==> R(y))) & ~(exists y. L(y) & R(y)) & (exists x. P(x) & (forall y. H(x,y) ==> L(y)) & (forall y. G(y) & H(x,y) ==> J(x,y))) ==> (exists x. P(x) & ~(exists y. G(y) & H(x,y)))"
p24 = parseFOL "~(exists x. U(x) & Q(x)) & (forall x. P(x) ==> Q(x) | R(x)) & ~(exists x. P(x) ==> (exists x. Q(x))) & (forall x. Q(x) & R(x) ==> U(x)) ==> (exists x. P(x) & R(x))"

fpf :: Ord k => [k] -> [a] -> M.Map k a
fpf xs ys = M.fromList $ zip xs ys

gilmore :: Formula FOL -> Int
gilmore fm = length (gilmore_loop (simpdnf sfm) (S.toList cntms) funcs (S.toList fvs) 0 (S.singleton S.empty) [] [])
 where
  sfm = skolemize (Not (generalize fm))
  fvs = fv sfm
  cntms = S.map (\(c,_) -> Fn c []) consts
  (consts,funcs) = herbfuns sfm

gilmore_loop
  :: (Enum a1, Eq a1, Num a1, Foldable t2) =>
     S.Set (S.Set (Formula FOL))
     -> [Term]
     -> t2 (String, Int)
     -> [String]
     -> a1
     -> S.Set (S.Set (Formula FOL))
     -> [[Term]]
     -> [[Term]]
     -> [[Term]]
gilmore_loop = herbloop mfn (not . S.null)
 where mfn djs0 ifn djs = S.filter (not . trivial) (distrib (S.map (S.map ifn) djs0) djs)

herbloop mfn tfn f10 cntms funcs fvs n fl tried tuples = trace ((show (length tried)) ++ " ground instances tried; " ++ (show (length fl)) ++ " items in list")
   (case tuples of
      [] -> let newtups = groundtuples cntms funcs n (length fvs) in
            herbloop mfn tfn f10 cntms funcs fvs (n + 1) fl tried newtups
      (tup : tups) -> let fl' = mfn f10 (subst (fpf fvs tup)) fl in
                      if (not (tfn fl')) then tup : tried else
                      herbloop mfn tfn f10 cntms funcs fvs n fl' (tup : tried) tups)

groundterms cntms funcs 0 = cntms
groundterms cntms funcs n = itlist (\(f,m) l -> map (\args -> Fn f args) (groundtuples cntms funcs (n-1) m) ++ l) funcs []

groundtuples cntms funcs 0 0 = [[]]
groundtuples cntms funcs n 0 = []
groundtuples cntms funcs n m = itlist (\k l -> allpairs (\h t -> h : t) (groundterms cntms funcs k) (groundtuples cntms funcs (n - k) (m - 1)) ++ l) [0 .. n] []

-- Section 3.7

herbfuns :: Formula FOL -> (S.Set (String, Int), S.Set (String, Int))
herbfuns fm
  | null cns = (S.singleton ("c",0),fns)
  | otherwise = (cns,fns)
 where
  (cns,fns) = S.partition (\(_,ar) -> ar == 0) (functions fm)

pholds :: (Formula a -> Bool) -> Formula a -> Bool
pholds d fm = eval fm (\p -> d (Atom p))

-- Section 3.6

eg100 = Exists "y" (Imp (Atom$R "<" [Var "x",Var "y"]) (Forall "u" (Exists "v" (Atom$R "<" [Fn "*" [Var "x",Var "u"],Fn "*" [Var "y",Var "v"]]))))
eg110 = Imp (Forall "x" (Atom$R "P" [Var "x"])) (Exists "y" (Exists "z" (Or (Atom$R "Q" [Var "y"]) (Not (Exists "z" (And (Atom$R "P" [Var "z"]) (Atom$R "Q" [Var "z"])))))))

skolemize :: Formula FOL -> Formula FOL
skolemize fm = specialize (pnf (askolemize fm))

specialize :: Formula t -> Formula t
specialize (Forall _ p) = specialize p
specialize fm = fm

askolemize :: Formula FOL -> Formula FOL
askolemize fm = fst (skolem (nnf (simplify fm)) (S.map fst (functions fm)))

funcs :: Term -> S.Set (String, Int)
funcs (Var _) = S.empty
funcs (Fn f args) = S.insert (f,length args) (S.unions (map funcs args))

functions :: Formula FOL -> S.Set (String, Int)
functions fm = S.fold S.union S.empty (atom_union (\(R _ a) -> S.unions (map funcs a)) fm)

skolem :: Formula FOL -> S.Set String -> (Formula FOL, S.Set String)
skolem fm@(Exists y p) fns = skolem (subst (M.singleton y fx) p) (S.insert f fns)
 where
  xs = fv fm
  f = variant (if S.null xs then "c_"++y else "f_"++y) fns
  fx = Fn f (map Var (S.toList xs))
skolem fm@(Forall x p) fns = (Forall x p',fns')
 where
  (p',fns') = skolem p fns
skolem fm@(And p q) fns = skolem2 (uncurry And) (p,q) fns
skolem fm@(Or p q) fns = skolem2 (uncurry Or) (p,q) fns
skolem fm fns = (fm,fns)

skolem2 cons (p,q) fns = (cons (p',q'),fns'')
 where
  (p',fns') = skolem p fns
  (q',fns'') = skolem q fns'

-- Section 3.5

eg90 = Imp (Forall "x" (Or (Atom$R "P" [Var "x"]) (Atom$R "R" [Var "y"]))) (Exists "y" (Exists "z" (Or (Atom$R "Q" [Var "y"]) (Not (Exists "z" (And (Atom$R "P" [Var "z"]) (Atom$R "Q" [Var "z"])))))))

pnf fm = prenex (nnf (simplify fm))

prenex :: Formula FOL -> Formula FOL
prenex (Forall x p) = Forall x (prenex p)
prenex (Exists x p) = Exists x (prenex p)
prenex (And p q) = pullquants (And (prenex p) (prenex q))
prenex (Or p q) = pullquants (Or (prenex p) (prenex q))
prenex fm = fm

pullquants :: Formula FOL -> Formula FOL
pullquants fm@(And (Forall x p) (Forall y q)) = pullq (True,True) fm Forall And x y p q
pullquants fm@(Or (Exists x p) (Exists y q)) = pullq (True,True) fm Exists Or x y p q
pullquants fm@(And (Forall x p) q) = pullq (True,False) fm Forall And x x p q
pullquants fm@(And p (Forall y q)) = pullq (False,True) fm Forall And y y p q
pullquants fm@(Or (Forall x p) q) = pullq (True,False) fm Forall Or x x p q
pullquants fm@(Or p (Forall y q)) = pullq (False,True) fm Forall Or y y p q
pullquants fm@(And (Exists x p) q) = pullq (True,False) fm Exists And x x p q
pullquants fm@(And p (Exists y q)) = pullq (False,True) fm Exists And y y p q
pullquants fm@(Or (Exists x p) q) = pullq (True,False) fm Exists Or x x p q
pullquants fm@(Or p (Exists y q)) = pullq (False,True) fm Exists Or y y p q
pullquants fm = fm

pullq (l,r) fm quant op x y p q = quant z (pullquants (op p' q'))
 where
  z = variant x (fv fm)
  p' = if l then subst (M.singleton x (Var z)) p else p
  q' = if r then subst (M.singleton y (Var z)) q else q

nnf (And p q) = And (nnf p) (nnf q)
nnf (Or p q) = Or (nnf p) (nnf q)
nnf (Imp p q) = Or (nnf (Not p)) (nnf q)
nnf (Iff p q) = Or (And (nnf p) (nnf q)) (And (nnf (Not p)) (nnf (Not q)))
nnf (Not (Not p)) = nnf p
nnf (Not (And p q)) = Or (nnf (Not p)) (nnf (Not q))
nnf (Not (Or p q)) = And (nnf (Not p)) (nnf (Not q))
nnf (Not (Imp p q)) = And (nnf p) (nnf (Not q))
nnf (Not (Iff p q)) = Or (And (nnf p) (nnf (Not q))) (And (nnf (Not p)) (nnf q))
nnf (Forall x p) = Forall x (nnf p)
nnf (Exists x p) = Exists x (nnf p)
nnf (Not (Forall x p)) = Exists x (nnf (Not p))
nnf (Not (Exists x p)) = Forall x (nnf (Not p))
nnf fm = fm

simplify :: Formula FOL -> Formula FOL
simplify (Not p) = simplify1 (Not (simplify p))
simplify (And p q) = simplify1 (And (simplify p) (simplify q))
simplify (Or p q) = simplify1 (Or (simplify p) (simplify q))
simplify (Imp p q) = simplify1 (Imp (simplify p) (simplify q))
simplify (Iff p q) = simplify1 (Iff (simplify p) (simplify q))
simplify (Forall x p) = simplify1 (Forall x (simplify p))
simplify (Exists x p) = simplify1 (Exists x (simplify p))
simplify fm = fm

simplify1 :: Formula FOL -> Formula FOL
simplify1 fm@(Forall x p)
 | S.member x (fv p) = fm
 | otherwise = p
simplify1 fm@(Exists x p)
 | S.member x (fv p) = fm
 | otherwise = p
simplify1 fm = psimplify1 fm

-- Section 3.4

subst :: M.Map String Term -> Formula FOL -> Formula FOL
subst subfn FF = FF
subst subfn TT = TT
subst subfn (Atom (R p args)) = Atom (R p (map (tsubst subfn) args))
subst subfn (Not p) = Not (subst subfn p)
subst subfn (And p q) = And (subst subfn p) (subst subfn q)
subst subfn (Or p q) = Or (subst subfn p) (subst subfn q)
subst subfn (Imp p q) = Imp (subst subfn p) (subst subfn q)
subst subfn (Iff p q) = Iff (subst subfn p) (subst subfn q)
subst subfn (Forall x p) = substq subfn Forall x p
subst subfn (Exists x p) = substq subfn Exists x p

substq subfn quant x p = quant x' (subst (M.insert x (Var x') subfn) p)
 where x' = if (any (\y -> S.member x (fvt (maybe (Var y) id (M.lookup y subfn))))
                    (S.delete x (fv p)))
            then variant x (fv (subst (M.delete x subfn) p)) else x

variant :: String -> S.Set String -> String
variant x vars
 | S.member x vars = variant (x ++ "'") vars
 | otherwise = x

tsubst :: M.Map String Term -> Term -> Term
tsubst sfn tm@(Var x) = maybe tm id (M.lookup x sfn)
tsubst sfn (Fn f args) = Fn f (map (tsubst sfn) args)

generalize :: Formula FOL -> Formula FOL
generalize fm = itlist Forall (fv fm) fm

-- Section 3.3

fv :: Formula FOL -> S.Set String
fv FF = S.empty
fv TT = S.empty
fv (Atom (R p args)) = S.unions (map fvt args)
fv (Not p) = fv p
fv (And p q) = S.union (fv p) (fv q)
fv (Or p q) = S.union (fv p) (fv q)
fv (Imp p q) = S.union (fv p) (fv q)
fv (Iff p q) = S.union (fv p) (fv q)
fv (Forall x p) = S.delete x (fv p)
fv (Exists x p) = S.delete x (fv p)

var :: Formula FOL -> S.Set String
var FF = S.empty
var TT = S.empty
var (Atom (R p args)) = S.unions (map fvt args)
var (Not p) = var p
var (And p q) = S.union (var p) (var q)
var (Or p q) = S.union (var p) (var q)
var (Imp p q) = S.union (var p) (var q)
var (Iff p q) = S.union (var p) (var q)
var (Forall x p) = S.insert x (var p)
var (Exists x p) = S.insert x (var p)

fvt :: Term -> S.Set String
fvt (Var x) = S.singleton x
fvt (Fn f args) = S.unions (map fvt args)

termval :: (t, String -> [b] -> b, u) -> M.Map String b -> Term -> Maybe b
termval _ v (Var x) = M.lookup x v
termval m@(_,func,_) v (Fn f args) = do
   t <- mapM (termval m v) args
   return (func f t)

holds :: ([b], String -> [b] -> b, String -> [b] -> Bool) -> M.Map String b -> Formula FOL -> Bool
holds _ v FF = False
holds _ v TT = True
holds m@(_,_,pred) v (Atom (R r args)) = maybe False (pred r) (mapM (termval m v) args)
holds m v (Not p) = not (holds m v p)
holds m v (And p q) = (holds m v p) && (holds m v q)
holds m v (Or p q) = (holds m v p) || (holds m v q)
holds m v (Imp p q) = (not (holds m v p)) || (holds m v q)
holds m v (Iff p q) = (holds m v p) == (holds m v q)
holds m@(domain,_,_) v (Forall x p) = all (\a -> holds m (M.insert x a v) p) domain
holds m@(domain,_,_) v (Exists x p) = any (\a -> holds m (M.insert x a v) p) domain

bool_interp :: ([Bool], String -> [Bool] -> Bool, String -> [Bool] -> Bool)
bool_interp = ([False, True],func,pred)
 where
  func "0" [] = False
  func "1" [] = True
  func "+" [x,y] = x /= y
  func "*" [x,y] = x && y
  func _ _ = error "uninterpreted function"
  pred "=" [x,y] = x == y
  pred _ _ = error "uninterpreted predicate"

mod_interp n = ([0..n-1],func,pred)
 where
  func "0" [] = 0
  func "1" [] = 1 `mod` n
  func "+" [x,y] = (x + y) `mod` n
  func "*" [x,y] = (x * y) `mod` n
  func _ _ = error "uninterpreted function"
  pred "=" [x,y] = x == y
  pred _ _ = error "uninterpred predicate"

pconstant x = R x []
constant x = Fn x []
equalsAtom x y = Atom (R "=" [x,y])

eg10 = holds bool_interp M.empty (Forall "x" (Or (equalsAtom (Var "x") (constant "0")) (equalsAtom (Var "x") (constant "1"))))
eg20 = holds (mod_interp 2) M.empty (Forall "x" (Or (equalsAtom (Var "x") (constant "0")) (equalsAtom (Var "x") (constant "1"))))
eg30 = holds (mod_interp 3) M.empty (Forall "x" (Or (equalsAtom (Var "x") (constant "0")) (equalsAtom (Var "x") (constant "1"))))
eg40 = Forall "x" (Imp (Not (equalsAtom (Var "x") (constant "0"))) (Exists "y" (equalsAtom (Fn "*" [Var "x",Var "y"]) (constant "1"))))

eg50 = Imp TT (Iff (Atom$pconstant "p") (Iff (Atom$pconstant "p") FF))
eg60 = Exists "x" (Exists "y" (Exists "z" ((Imp (Atom$R "P" [Var "x"]) (Imp (Atom$R "Q" [Var "z"]) FF)))))
eg70 = Forall "x" (Imp (Atom$R "P" [Var "x"]) (Atom$pconstant "Q"))
eg80 = Imp (Forall "x" (Atom$R "P" [Var "x"])) (Iff (Exists "y" (Atom$R "Q" [Var "y"])) (Exists "z" (And (Atom$R "P" [Var "z"]) (Atom$R "Q" [Var "z"]))))
