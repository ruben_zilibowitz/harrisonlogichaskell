module LCFFOL where

import Prelude hiding (negate,sum,pred)
import qualified Data.Set as S
import Data.List (intercalate,minimumBy,maximumBy,find,partition,delete,findIndex,sortBy)
import Data.Maybe
import Data.Either (isRight)
import qualified Data.Map as M
import Debug.Trace

import PropositionalLogic hiding (nnf)
import FirstOrderLogic hiding (replace)
import Types
import Failing
import Parser
import Equality (mk_eq, lhs, rhs, dest_eq, termsize, replacet)
import Proofsystem
import LCFProp

{- ------------------------------------------------------------------------- -}
{-                         ******                                            -}
{-         ------------------------------------------ eq_sym                 -}
{-                      |- s = t ==> t = s                                   -}
{- ------------------------------------------------------------------------- -}

eq_sym :: Term -> Term -> Failing Thm
eq_sym s t =
  let rth = axiom_eqrefl s in
  funpowM 2 (\th -> imp_swap th >>= flip modusponens rth)
            (axiom_predcong "=" [s, s] [t, s])

{- ------------------------------------------------------------------------- -}
{- |- s = t ==> t = u ==> s = u.                                             -}
{- ------------------------------------------------------------------------- -}

eq_trans :: Term -> Term -> Term -> Failing Thm
eq_trans s t u =
  let th1 = axiom_predcong "=" [t, u] [s, u] in do
  x <- imp_swap th1
  th2 <- modusponens x (axiom_eqrefl u)
  y <- eq_sym s t
  imp_trans y th2

{- ------------------------------------------------------------------------- -}
{-         ---------------------------- icongruence                          -}
{-          |- s = t ==> tm[s] = tm[t]                                       -}
{- ------------------------------------------------------------------------- -}

icongruence :: Term -> Term -> Term -> Term -> Failing Thm
icongruence s t stm ttm =
  if stm == ttm then add_assum (mk_eq s t) (axiom_eqrefl stm)
  else if stm == s && ttm == t then imp_refl (mk_eq s t) else
  case (stm,ttm) of
    (Fn fs sa,Fn ft ta) | fs == ft && length sa == length ta -> do
       ths <- sequence (zipWith (icongruence s t) sa ta)
       ts <- mapM (consequent . concl) ths
       x <- mapM lhs ts
       y <- mapM rhs ts
       imp_trans_chain ths (axiom_funcong fs x y)
    _ -> failure "icongruence: not congruent"

icong_eg :: Failing Thm
icong_eg = icongruence (parseFOLTerm "s") (parseFOLTerm "t") (parseFOLTerm "f(s,g(s,t,s),u,h(h(s)))") (parseFOLTerm "f(s,g(t,t,s),u,h(h(t)))")

{- ------------------------------------------------------------------------- -}
{- |- (forall x. p ==> q(x)) ==> p ==> (forall x. q(x))                      -}
{- ------------------------------------------------------------------------- -}

gen_right_th :: String -> Formula FOL -> Formula FOL -> Failing Thm
gen_right_th x p q = do
  y <- axiom_impall x p
  imp_swap (axiom_allimp x p q) >>= imp_trans y >>= imp_swap

{- ------------------------------------------------------------------------- -}
{-                       |- p ==> q                                          -}
{-         ------------------------------------- genimp "x"                  -}
{-           |- (forall x. p) ==> (forall x. q)                              -}
{- ------------------------------------------------------------------------- -}

genimp :: String -> Thm -> Failing Thm
genimp x th = do
  (p,q) <- dest_imp (concl th)
  modusponens (axiom_allimp x p q) (gen x th)

{- ------------------------------------------------------------------------- -}
{- If |- p ==> q[x] then |- p ==> forall x. q[x]                             -}
{- ------------------------------------------------------------------------- -}

gen_right :: String -> Thm -> Failing Thm
gen_right x th = do
  (p,q) <- dest_imp (concl th)
  y <- gen_right_th x p q
  modusponens y (gen x th)

{- ------------------------------------------------------------------------- -}
{- |- (forall x. p(x) ==> q) ==> (exists x. p(x)) ==> q                      -}
{- ------------------------------------------------------------------------- -}

exists_left_th :: String -> Formula FOL -> Formula FOL -> Failing Thm
exists_left_th x p q =
  let p' = Imp p FF
      q' = Imp q FF in do
  th1 <- imp_trans_th p q FF >>= imp_swap >>= genimp x
  th2 <- gen_right_th x q' p' >>= imp_trans th1
  th3 <- imp_trans_th q' (Forall x p') FF >>= imp_swap
  th4 <- imp_trans th2 th3 >>= flip imp_trans2 (axiom_doubleneg q)
  th5 <- iff_imp2 (axiom_not p) >>= genimp x >>= imp_add_concl FF
  th6 <- iff_imp1 (axiom_not (Forall x (Not p))) >>= flip imp_trans th5
  th7 <- iff_imp1 (axiom_exists x p) >>= flip imp_trans th6
  imp_swap th4 >>= imp_trans th7 >>= imp_swap

{- ------------------------------------------------------------------------- -}
{- If |- p(x) ==> q then |- (exists x. p(x)) ==> q                           -}
{- ------------------------------------------------------------------------- -}

exists_left :: String -> Thm -> Failing Thm
exists_left x th = do
  (p,q) <- dest_imp (concl th)
  y <- exists_left_th x p q
  modusponens y (gen x th)

{- ------------------------------------------------------------------------- -}
{-    |- x = t ==> p ==> q    [x not in t and not free in q]                 -}
{-  --------------------------------------------------------------- subspec  -}
{-                 |- (forall x. p) ==> q                                    -}
{- ------------------------------------------------------------------------- -}

subspec :: Thm -> Failing Thm
subspec th =
  case concl th of
    (Imp (e@(Atom (R "=" [Var x,t]))) (Imp p q)) -> do
        y <- imp_swap th >>= genimp x
        z <- exists_left_th x e q
        th1 <- imp_trans y z
        w <- imp_swap th1
        u <- axiom_existseq x t
        modusponens w u
    _ -> failure "subspec: wrong sort of theorem"

{- ------------------------------------------------------------------------- -}
{-    |- x = y ==> p[x] ==> q[y]  [x not in FV(q); y not in FV(p) or x == y] -}
{-  --------------------------------------------------------- subalpha       -}
{-                 |- (forall x. p) ==> (forall y. q)                        -}
{- ------------------------------------------------------------------------- -}

subalpha :: Thm -> Failing Thm
subalpha th =
   case concl th of
    (Imp (Atom (R "=" [Var x,Var y])) (Imp p q)) ->
        if x == y then modusponens th (axiom_eqrefl (Var x)) >>= genimp x
        else subspec th >>= gen_right y
    _ -> failure "subalpha: wrong sort of theorem"

{- ------------------------------------------------------------------------- -}
{-         ---------------------------------- isubst                         -}
{-            |- s = t ==> p[s] ==> p[t]                                     -}
{- ------------------------------------------------------------------------- -}

isubst :: Term -> Term -> Formula FOL -> Formula FOL -> Failing Thm
isubst s t sfm tfm =
  if sfm == tfm then imp_refl tfm >>= add_assum (mk_eq s t) else
  case (sfm,tfm) of
    (Atom (R p sa),Atom (R p' ta)) | p == p' && length sa == length ta -> do
        ths <- sequence (zipWith (icongruence s t) sa ta)
        (ls,rs) <- mapM (\x -> consequent (concl x) >>= dest_eq) ths >>= return . unzip
        imp_trans_chain ths (axiom_predcong p ls rs)
    (Imp sp sq,Imp tp tq) -> do
        x <- eq_sym s t
        y <- isubst t s tp sp
        th1 <- imp_trans x y
        th2 <- isubst s t sq tq
        imp_mono_th sp tp sq tq >>= imp_trans_chain [th1, th2]
    (Forall x p,Forall y q) ->
        if x == y then do
          u <- (isubst s t p q) >>= (gen_right x)
          imp_trans u (axiom_allimp x p q)
        else do
          let z = Var (variant x (S.unions [fv p, fv q, fvt s, fvt t]))
          th1 <- isubst (Var x) z p (subst (M.singleton x z) p)
          th2 <- isubst z (Var y) (subst (M.singleton y z) q) q
          th3 <- subalpha th1
          th4 <- subalpha th2
          u <- consequent (concl th3)
          v <- antecedent (concl th4)
          th5 <- isubst s t u v
          imp_swap th5 >>= imp_trans th3 >>= flip imp_trans2 th4 >>= imp_swap
    _ -> do
        sth <- expand_connective sfm >>= iff_imp1
        tth <- expand_connective tfm >>= iff_imp2
        u <- consequent (concl sth)
        v <- antecedent (concl tth)
        th1 <- isubst s t u v
        imp_trans2 th1 tth >>= imp_swap >>= imp_trans sth >>= imp_swap

{- ------------------------------------------------------------------------- -}
{-                                                                           -}
{- -------------------------------------------- alpha "z" <<forall x. p[x]>> -}
{-   |- (forall x. p[x]) ==> (forall z. p'[z])                               -}
{-                                                                           -}
{- [Restriction that z is not free in the initial p[x].]                     -}
{- ------------------------------------------------------------------------- -}

alpha :: String -> Formula FOL -> Failing Thm
alpha z (Forall x p) =
   let p' = subst (M.singleton x (Var z)) p in
   isubst (Var x) (Var z) p p' >>= subalpha
alpha _ _ = failure "alpha: not a universal formula"

{- ------------------------------------------------------------------------- -}
{-                                                                           -}
{- -------------------------------- ispec t <<forall x. p[x]>>               -}
{-   |- (forall x. p[x]) ==> p'[t]                                           -}
{- ------------------------------------------------------------------------- -}

ispec :: Term -> Formula FOL -> Failing Thm
ispec t fm@(Forall x p) =
      if S.member x (fvt t) then do
        th <- alpha (variant x (S.union (fvt t) (var p))) fm
        consequent (concl th) >>= ispec t >>= imp_trans th
      else isubst (Var x) t p (subst (M.singleton x t) p) >>= subspec
ispec _ _ = failure "ispec: non-universal formula"

ispec_eg1 :: Failing Thm
ispec_eg1 = ispec (parseFOLTerm "y") (parseFOL "forall x y z. x + y + z = z + y + x")

{- ------------------------------------------------------------------------- -}
{- Specialization rule.                                                      -}
{- ------------------------------------------------------------------------- -}

spec :: Term -> Thm -> Failing Thm
spec t th = ispec t (concl th) >>= flip modusponens th

{- ------------------------------------------------------------------------- -}
{- Unification of complementary literals.                                    -}
{- ------------------------------------------------------------------------- -}

unify_complementsf
  :: M.Map String Term
     -> (Formula FOL, Formula FOL) -> Failing (M.Map String Term)
unify_complementsf env ((Atom (R p1 a1)), (Imp (Atom (R p2 a2)) FF)) = unify env [(Fn p1 a1,Fn p2 a2)]
unify_complementsf env (Imp (Atom (R p1 a1)) FF, Atom (R p2 a2)) = unify env [(Fn p1 a1,Fn p2 a2)]
unify_complementsf _ _ = failure "unify_complementsf"

{- ------------------------------------------------------------------------- -}
{-    |- (q ==> f) ==> ... ==> (q ==> p) ==> r                               -}
{- --------------------------------------------- use_laterimp <<q ==> p>>    -}
{-    |- (p ==> f) ==> ... ==> (q ==> p) ==> r                               -}
{- ------------------------------------------------------------------------- -}

use_laterimp :: Formula FOL -> Formula FOL -> Failing Thm
use_laterimp i fm =
  case fm of
    (Imp (Imp q' (s)) (Imp (Imp q p) r)) | Imp q p == i -> do
        let th1 = axiom_distribimp i (Imp (Imp q s) r) (Imp (Imp p s) r)
        th2 <- imp_trans_th q p s >>= imp_swap
        th3 <- imp_trans_th (Imp p s) (Imp q s) r >>= imp_swap
        imp_trans th2 th3 >>= modusponens th1 >>= imp_swap2
    (Imp qs (Imp a b)) ->
        use_laterimp i (Imp qs b) >>= imp_add_assum a >>= imp_swap2

{- ------------------------------------------------------------------------- -}
{- The "closure" inference rules.                                            -}
{- ------------------------------------------------------------------------- -}

imp_false_rule' :: (t -> Failing Thm) -> t -> Failing Thm
imp_false_rule' th es = (th es) >>= imp_false_rule

imp_true_rule' :: (t -> Failing Thm) -> (t -> Failing Thm) -> t -> Failing Thm
imp_true_rule' th1 th2 es = do
   x <- th1 es
   y <- th2 es
   imp_true_rule x y

imp_front' :: (Eq a, Num a) => a -> (t -> Failing Thm) -> t -> Failing Thm
imp_front' n thp es = (thp es) >>= imp_front n

add_assum'
  :: Formula FOL
     -> ((Term -> Term, t) -> Failing Thm) -> (Term -> Term, t) -> Failing Thm
add_assum' fm thp es@(e,s) =
  (thp es) >>= add_assum (onformula e fm)

eliminate_connective'
  :: Formula FOL
     -> ((Term -> Term, t) -> Failing Thm)
     -> (Term -> Term, t)
     -> Failing Thm
eliminate_connective' fm thp es@(e,s) = do
   x <- thp es
   y <- eliminate_connective (onformula e fm)
   imp_trans y x

spec'
  :: (Eq a, Num a) =>
     Term
     -> Formula FOL
     -> a
     -> ((Term -> Term, t) -> Failing Thm)
     -> (Term -> Term, t)
     -> Failing Thm
spec' y fm n thp (e,s) = do
  th <- thp (e,s) >>= imp_front n >>= imp_swap
  ispec (e y) (onformula e fm) >>= flip imp_trans th >>= imp_unduplicate

ex_falso'
  :: Foldable t =>
     t (Formula FOL) -> (Term -> Term, Formula FOL) -> Failing Thm
ex_falso' fms (e,s) =
  ex_falso (itlist (Imp . onformula e) fms s)

complits'
  :: ([Formula FOL], [Formula FOL])
     -> Int -> (Term -> Term, Formula FOL) -> Failing Thm
complits' (p:fl,lits) i (e,s) = 
  let (l1,p':l2) = splitAt i lits in do
  u <- (imp_contr (onformula e p)
                    (itlist (Imp . onformula e) l2 s))
  itlistM (imp_insert . onformula e) (fl ++ l1) u

deskol'
  :: Formula FOL
     -> ((Term -> Term, t) -> Failing Thm)
     -> (Term -> Term, t)
     -> Failing Thm
deskol' skh thp (e,s) = do
  th <- thp (e,s)
  use_laterimp (onformula e skh) (concl th) >>= flip modusponens th

{- ------------------------------------------------------------------------- -}
{- Main refutation function.                                                 -}
{- ------------------------------------------------------------------------- -}

lcftab _ (_,_,n) _ _ | n < 0 = failure "lcftab: no proof"
lcftab skofun (fms,lits,n) cont esk@(env,sks,k) =
  case fms of
    FF:fl -> cont (ex_falso' (fl ++ lits)) esk
    (fm@(Imp p q)):fl | p == q -> 
      lcftab skofun (fl,lits,n) (cont . add_assum' fm) esk
    (Imp (Imp p q) FF):fl -> 
      lcftab skofun (p:(Imp q FF):fl,lits,n)
                    (cont . imp_false_rule') esk
    (Imp p q):fl | q /= FF -> 
      lcftab skofun ((Imp p FF):fl,lits,n)
        (\th -> lcftab skofun (q:fl,lits,n)
                                 (cont . imp_true_rule' th)) esk
    l@((Atom _):fl) -> lcftabHelper l
    l@((Imp (Atom _) FF):fl) -> lcftabHelper l
    (fm@(Forall x p)):fl -> 
      let y = Var ("X_" ++ (show k)) in
      lcftab skofun ((subst (M.singleton x y) p):fl ++ [fm],lits,n-1)
                    (cont . spec' y fm (length fms)) (env,sks,k+1)
    (Imp (yp@(Forall y p)) FF):fl -> do
      fx <- skofun yp
      let p' = subst (M.singleton y fx) p
      let skh = Imp p' (Forall y p)
      let sks' = (Forall y p,fx):sks
      lcftab skofun ((Imp p' FF) : fl,lits,n)
                    (cont . deskol' skh) (env,sks',k)
    fm:fl -> do
      fm' <- eliminate_connective fm >>= return . concl >>= consequent
      lcftab skofun (fm':fl,lits,n)
                    (cont . eliminate_connective' fm) esk
    [] -> failure "lcftab: No contradiction"
 where lcftabHelper (p:fl) = (tryM (tryfind (\p' -> do
               env' <- unify_complementsf env (p,p')
               p'i <- maybe (failure "findIndex") return (findIndex (p' ==) lits)
               cont (complits' (fms,lits) p'i) (env',sks,k)) lits)
               (lcftab skofun (fl,p:lits,n)
                              (cont . imp_front' (length fl)) esk))

{- ------------------------------------------------------------------------- -}
{- Identify quantified subformulas; true = exists, false = forall. This is   -}
{- taking into account the effective parity.                                 -}
{- NB: maybe I can use this in sigma/delta/pi determination.                 -}
{- ------------------------------------------------------------------------- -}

quantforms :: Ord t => Bool -> Formula t -> S.Set (Formula t)
quantforms e fm =
  case fm of
    Not p -> quantforms (not e) p
    And p q -> S.union (quantforms e p) (quantforms e q)
    Or p q -> S.union (quantforms e p) (quantforms e q)
    Imp p q -> quantforms e (Or (Not p) q)
    Iff p q -> quantforms e (Or (And(p) q) (And (Not p) (Not q)))
    Exists x p -> if e then S.insert fm (quantforms e p) else quantforms e p
    Forall x p -> if e then quantforms e p else S.insert fm (quantforms e p)
    _ -> S.empty

{- ------------------------------------------------------------------------- -}
{- Now create some Skolem functions.                                         -}
{- ------------------------------------------------------------------------- -}

skolemfuns :: Formula FOL -> [(Formula FOL, Term)]
skolemfuns fm =
  let fns = S.map fst (functions fm) in
  let skts = S.map (\y -> case y of {(Exists x p) -> Forall x (Not p); p -> p})
                 (quantforms True fm) in
  let skofun i ap@(Forall y p) = let vars = S.map Var (fv ap) in (ap,Fn (variant ("f" ++ "_" ++ show i) fns) (S.toList vars)) in
  zipWith skofun [1 .. length skts] (S.toList skts)

{- ------------------------------------------------------------------------- -}
{- Matching.                                                                 -}
{- ------------------------------------------------------------------------- -}

form_match
  :: (Formula FOL, Formula FOL)
     -> M.Map String Term -> Failing (M.Map String Term)
form_match fp env = case fp of
   (FF,FF) -> return env
   (TT,TT) -> return env
   (Atom (R p pa),Atom (R q qa)) -> term_match env [(Fn p pa,Fn q qa)]
   (Not p1,Not p2) -> form_match (p1,p2) env
   (And p1 q1,And p2 q2) -> form_match_helper1 p1 q1 p2 q2
   (Or p1 q1,Or p2 q2) -> form_match_helper1 p1 q1 p2 q2
   (Imp p1 q1,Imp p2 q2) -> form_match_helper1 p1 q1 p2 q2
   (Iff p1 q1,Iff p2 q2) -> form_match_helper1 p1 q1 p2 q2
   (Exists x1 p1,Exists x2 p2) | x1 == x2 -> form_match_helper2 x1 p1 x2 p2
   (Forall x1 p1,Forall x2 p2) | x1 == x2 -> form_match_helper2 x1 p1 x2 p2
   _ -> failure "form_match"
 where
  form_match_helper1 p1 q1 p2 q2 = form_match (q1,q2) env >>= form_match (p1,p2)
  form_match_helper2 x1 p1 x2 p2 =
    let z = variant x1 (S.union (fv p1) (fv p2))
        inst_fn = subst (M.singleton x1 (Var z)) in
    form_match (inst_fn p1,inst_fn p2) env >>= return . (M.delete z)

{- ------------------------------------------------------------------------- -}
{- With the current approach to picking Skolem functions.                    -}
{- ------------------------------------------------------------------------- -}

lcfrefute fm n cont =
  let sl = skolemfuns fm
      find_skolem fm0 = tryfind (findf fm0) sl in
  lcftab find_skolem ([fm],[],n) cont (M.empty,[],0)
 where
  findf fm0 (f,t) = do
    x <- form_match (f,fm0) M.empty
    return (tsubst x t)

{- ------------------------------------------------------------------------- -}
{- A quick demo before doing deskolemization.                                -}
{- ------------------------------------------------------------------------- -}

mk_skol :: (Formula FOL, Term) -> Formula FOL -> Formula FOL
mk_skol (Forall y p,fx) q =
  Imp (Imp (subst (M.singleton y fx) p) (Forall y p)) q

simpcont
  :: (Foldable u) =>
     ((Term -> Term, Formula FOL) -> r)
     -> (M.Map String Term, u (Formula FOL, Term), t) -> Failing r

simpcont thp (env,sks,k) =
  let ifn = tsubst (solve env) in
  return (thp (ifn,onformula ifn (itlist mk_skol sks FF)))

lcfrefute_eg1 = lcfrefute (parseFOL "p(1) & ~q(1) & (forall x. p(x) ==> q(x))") 1 simpcont

lcfrefute_eg2 = lcfrefute (parseFOL "(exists x. ~p(x)) & (forall x. p(x))") 1 simpcont

{- ------------------------------------------------------------------------- -}
{-         |- (p(v) ==> forall x. p(x)) ==> q                                -}
{-       -------------------------------------- elim_skolemvar               -}
{-                   |- q                                                    -}
{- ------------------------------------------------------------------------- -}

elim_skolemvar :: Thm -> Failing Thm
elim_skolemvar th =
  case concl th of
    Imp (Imp pv (apx@(Forall x px))) q -> do
        [th1,th2] <- sequence (imp_false_conseqs pv apx) >>= mapM (\a -> do {b <- imp_add_concl FF th; imp_trans b a})
        let v = head ((S.toList ((fv pv) S.\\ (fv apx))) ++ [x])
        th3 <- gen_right v th1
        th4 <- consequent (concl th3) >>= alpha x >>= imp_trans th3
        right_mp th2 th4 >>= modusponens (axiom_doubleneg q)
    _ -> failure "elim_skolemvar"

{- ------------------------------------------------------------------------- -}
{- Top continuation with careful sorting and variable replacement.           -}
{- Also need to delete post-instantiation duplicates! This shows up more     -}
{- often now that we have adequate sharing.                                  -}
{- ------------------------------------------------------------------------- -}

deskolcont
  :: ((Term -> Term, Formula FOL) -> Failing Thm)
     -> (M.Map String Term, [(Formula FOL, Term)], t) -> Failing Thm

deskolcont thp (env,sks,k) =
  let ifn = tsubst (solve env)
      isk = setify (map (\(p,t) -> (onformula ifn p,ifn t)) sks)
      ssk = sortBy (decreasing (termsize . snd)) isk
      vs = map (\i -> Var ("Y_" ++ (show i))) [1 .. length ssk]
      vfn = replacet (itlist2 (\(p,t) v -> M.insert t v) ssk vs M.empty) in do
  th <- thp (vfn . ifn, onformula vfn (itlist mk_skol ssk FF))
  return (repeatTo (\x -> imp_swap x >>= elim_skolemvar) th)

{- ------------------------------------------------------------------------- -}
{- Overall first-order prover.                                               -}
{- ------------------------------------------------------------------------- -}

lcffol :: Formula FOL -> Failing Thm
lcffol fm = --trace ("proving: " ++ show fm) $
  let fvs = S.toList $ fv fm
      fm' = Imp (itlist Forall fvs fm) FF
      th1 = deepen (\n -> lcfrefute fm' n deskolcont) 0 in do
  th2 <- modusponens (axiom_doubleneg (negatef fm')) th1
  itlistM (\v -> spec (Var v)) (reverse fvs) th2

ewd1062_1 = lcffol $ parseFOL "(forall x. x <= x) & (forall x y z. x <= y & y <= z ==> x <= z) & (forall x y. f(x) <= y <==> x <= g(y)) ==> (forall x y. x <= y ==> f(x) <= f(y))"

p58 = lcffol $ parseFOL "forall x. exists v. exists w. forall y. forall z. ((P(x) & Q(y)) ==> ((P(v) | R(w)) & (R(z) ==> Q(v))))"
