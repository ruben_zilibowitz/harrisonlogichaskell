module LCFProp where

import Prelude hiding (negate,sum,pred)
import qualified Data.Set as S
import Data.List (intercalate,minimumBy,maximumBy,find,partition,delete,findIndex,sortBy)
import Data.Maybe
import Data.Either (isRight)
import qualified Data.Map as M
import Debug.Trace

import PropositionalLogic hiding (nnf)
import FirstOrderLogic hiding (replace)
import Types
import Failing
import Parser
import DP
import Equality
import Proofsystem

lcftaut_eg1 :: Failing Thm
lcftaut_eg1 = lcftaut $ parseFOL "(p ==> q) | (q ==> p)"

lcftaut_eg2 :: Failing Thm
lcftaut_eg2 = lcftaut $ parseFOL "p & q <==> ((p <==> q) <==> p | q)"

lcftaut_eg3 :: Failing Thm
lcftaut_eg3 = lcftaut $ parseFOL "((p <==> q) <==> r) <==> (p <==> (q <==> r))"

{- ------------------------------------------------------------------------- -}
{- In particular, this gives a tautology prover.                             -}
{- ------------------------------------------------------------------------- -}

lcftaut :: Formula FOL -> Failing Thm
lcftaut p =
  lcfptab [negatef p] [] >>= modusponens (axiom_doubleneg p)

{- ------------------------------------------------------------------------- -}
{- Propositional tableaux procedure.                                         -}
{- ------------------------------------------------------------------------- -}

lcfptab :: [Formula FOL] -> [Formula FOL] -> Failing Thm
lcfptab (FF:fl) lits = ex_falso (itlist Imp (fl ++ lits) FF)
lcfptab (fm@(Imp p q):fl) lits | p == q = lcfptab fl lits >>= add_assum fm
lcfptab (Imp (Imp p q) FF:fl) lits = lcfptab (p:(Imp q FF):fl) lits >>= imp_false_rule
lcfptab ((Imp p q):fl) lits | q /= FF = do
   x <- lcfptab (q:fl) lits
   y <- lcfptab ((Imp p FF):fl) lits
   imp_true_rule y x
lcfptab l@((Atom _) : fl) lits = lcfptab_helper l lits
lcfptab l@((Forall _ _) : fl) lits = lcfptab_helper l lits
lcfptab l@((Imp (Forall _ _) FF) : fl) lits = lcfptab_helper l lits
lcfptab l@((Imp (Atom _) FF) : fl) lits = lcfptab_helper l lits
lcfptab (fm:fl) lits = do
   th <- eliminate_connective fm
   q <- consequent (concl th)
   lcfptab (q:fl) lits >>= imp_trans th
lcfptab _ _ = failure "lcfptab: no contradiction"

lcfptab_helper (p:fl) lits =
   if elem (negatef p) lits then
     let (l1,l2) = break ((negatef p) ==) lits in do
     th <- imp_contr p (itlist Imp (tail l2) FF)
     itlistM imp_insert (fl ++ l1) th
   else lcfptab fl (p:lits) >>= imp_front (length fl)

{- ------------------------------------------------------------------------- -}
{-           |- p0 ==> p1 ==> ... ==> pn ==> q                               -}
{-         ------------------------------------------ imp_front n            -}
{-           |- pn ==> p0 ==> p1 ==> .. p(n-1) ==> q                         -}
{- ------------------------------------------------------------------------- -}

imp_front :: (Eq a, Num a) => a -> Thm -> Failing Thm
imp_front n th =
   let p = concl th in do
   x <- imp_front_th n p
   modusponens x th

{- ------------------------------------------------------------------------- -}
{-                                                                           -}
{- --------------------------------------------- imp_front (this antecedent) -}
{-  |- (p0 ==> p1 ==> ... ==> pn ==> q)                                      -}
{-     ==> pn ==> p0 ==> p1 ==> .. p(n-1) ==> q                              -}
{- ------------------------------------------------------------------------- -}

imp_front_th :: (Eq a, Num a) => a -> Formula FOL -> Failing Thm
imp_front_th 0 fm = imp_refl fm
imp_front_th n (Imp p qr) = do
  th1 <- imp_front_th (n - 1) qr >>= imp_add_assum p
  (q',r') <- consequent (concl th1) >>= consequent >>= dest_imp
  imp_swap_th p q' r' >>= imp_trans th1
imp_front_th _ _ = failure "imp_front_th"

{- ------------------------------------------------------------------------- -}
{-                                 *                                         -}
{-                 -------------------------------------- imp_contr          -}
{-                        |- p ==> -p ==> q                                  -}
{- ------------------------------------------------------------------------- -}

imp_contr :: Formula FOL -> Formula FOL -> Failing Thm
imp_contr p q =
  if negativef p then ex_falso q >>= imp_add_assum (negatef p)
  else ex_falso q >>= imp_add_assum p >>= imp_swap

{- ------------------------------------------------------------------------- -}
{-         |- (p ==> false) ==> r          |- q ==> r                        -}
{-       ---------------------------------------------- imp_true_rule        -}
{-                      |- (p ==> q) ==> r                                   -}
{- ------------------------------------------------------------------------- -}

imp_true_rule :: Thm -> Thm -> Failing Thm
imp_true_rule th1 th2 = do
  p <- antecedent (concl th1) >>= antecedent
  q <- antecedent (concl th2)
  th3 <- imp_add_concl FF th1 >>= right_doubleneg
  th4 <- imp_add_concl FF th2
  th5 <- imp_truefalse p q >>= imp_swap
  th6 <- imp_trans_chain [th3, th4] th5 >>= imp_add_concl FF
  th7 <- imp_refl (Imp (Imp p q) FF) >>= imp_swap
  imp_trans th7 th6 >>= right_doubleneg

{- ------------------------------------------------------------------------- -}
{-         |- p ==> (q ==> false) ==> r                                      -}
{-        ------------------------------------ imp_false_rule                -}
{-             |- ((p ==> q) ==> false) ==> r                                -}
{- ------------------------------------------------------------------------- -}

imp_false_rule :: Thm -> Failing Thm
imp_false_rule th = do
  p <- antecedent (concl th)
  aar <- consequent (concl th) >>= antecedent >>= antecedent
  x <- sequence (imp_false_conseqs p aar)
  imp_trans_chain x th

{- ------------------------------------------------------------------------- -}
{-                                                                           -}
{-   ------------------------------------------------- imp_false_conseqs     -}
{-      [|- ((p ==> q) ==> false) ==> (q ==> false);                         -}
{-       |- ((p ==> q) ==> false) ==> p]                                     -}
{- ------------------------------------------------------------------------- -}

imp_false_conseqs :: Formula FOL -> Formula FOL -> [Failing Thm]
imp_false_conseqs p q =
 [do x <- ex_falso q
     y <- imp_add_assum p x
     z <- imp_add_concl FF y
     right_doubleneg z,
  do x <- imp_refl q
     y <- imp_insert p x
     imp_add_concl FF y]

{- ------------------------------------------------------------------------- -}
{- Produce "expansion" theorem for defined connectives.                      -}
{- ------------------------------------------------------------------------- -}

eliminate_connective :: Formula FOL -> Failing Thm
eliminate_connective fm =
  if not (negativef fm) then expand_connective fm >>= iff_imp1
  else expand_connective (negatef fm) >>= iff_imp2 >>= imp_add_concl FF

expand_connective :: Formula FOL -> Failing Thm
expand_connective TT = return axiom_true
expand_connective (Not p) = return $ axiom_not p
expand_connective (And p q) = return $ axiom_and p q
expand_connective (Or p q) = return $ axiom_or p q
expand_connective (Iff p q) = iff_def p q
expand_connective (Exists x p) = return $ axiom_exists x p
expand_connective _ = failure "expand_connective"

{- ------------------------------------------------------------------------- -}
{- Produce |- (p <=> q) <=> (p ==> q) /\ (q ==> p)                           -}
{- ------------------------------------------------------------------------- -}

iff_def :: Formula FOL -> Formula FOL -> Failing Thm
iff_def p q = do
  th <- and_pair (Imp p q) (Imp q p)
  let thl = [axiom_iffimp1 p q, axiom_iffimp2 p q]
  x <- unshunt (axiom_impiff p q)
  y <- imp_trans_chain thl th
  imp_antisym y x

{-
	iff_def p q =
	  let th1 = and_pair (Imp p q) (Imp q p) in
	  let th2 = imp_trans_chain [axiom_iffimp1 p q, axiom_iffimp2 p q] th1 in
	  imp_antisym th2 (unshunt (axiom_impiff p q))
-}

{- ------------------------------------------------------------------------- -}
{- If |- p ==> q ==> r then |- p /\ q ==> r                                  -}
{- ------------------------------------------------------------------------- -}

unshunt :: Thm -> Failing Thm
unshunt th = do
  p <- antecedent (concl th)
  (q,r) <- consequent (concl th) >>= dest_imp
  x <- and_left p q
  y <- and_right p q
  imp_trans_chain [x, y] th

{- ------------------------------------------------------------------------- -}
{- If |- p /\ q ==> r then |- p ==> q ==> r                                  -}
{- ------------------------------------------------------------------------- -}

shunt :: Thm -> Failing Thm
shunt th = do
  (p,q) <- antecedent (concl th) >>= dest_and
  x <- (and_pair p q)
  y <- (itlistM imp_add_assum [p,q] th)
  modusponens y x

{- ------------------------------------------------------------------------- -}
{- |- p ==> q ==> p /\ q                                                     -}
{- ------------------------------------------------------------------------- -}

and_pair :: Formula FOL -> Formula FOL -> Failing Thm
and_pair p q = do
  th1 <- iff_imp2 (axiom_and p q)
  th2 <- imp_swap_th (Imp p (Imp q (FF))) q FF
  th3 <- imp_trans2 th2 th1 >>= imp_add_assum p
  imp_refl (Imp p (Imp q FF)) >>= imp_swap >>= modusponens th3

{- ------------------------------------------------------------------------- -}
{- |- p1 /\ ... /\ pn ==> pi for each 1 <= i <= n (input term right assoc)   -}
{- ------------------------------------------------------------------------- -}

conjths :: Formula FOL -> [Failing Thm]
conjths (And p q) = (and_left p q) : (map (\x -> do {y <- x; z <- and_right p q; imp_trans z y}) (conjths q))
conjths fm = [imp_refl fm]

{- ------------------------------------------------------------------------- -}
{- |- p /\ q ==> q                                                           -}
{- ------------------------------------------------------------------------- -}

and_right :: Formula FOL -> Formula FOL -> Failing Thm
and_right p q = do
  let th1 = axiom_addimp (Imp q FF) p
  th2 <- (imp_add_concl FF th1) >>= right_doubleneg
  x <- iff_imp1 (axiom_and p q)
  imp_trans x th2

{- ------------------------------------------------------------------------- -}
{- |- p /\ q ==> p                                                           -}
{- ------------------------------------------------------------------------- -}

and_left :: Formula FOL -> Formula FOL -> Failing Thm
and_left p q = do
  th1 <- imp_add_assum p (axiom_addimp FF q)
  th2 <- (imp_add_concl FF th1) >>= right_doubleneg
  x <- iff_imp1 (axiom_and p q)
  imp_trans x th2

{- ------------------------------------------------------------------------- -}
{-         |- p ==> q                                                        -}
{-      ----------------- contrapos                                          -}
{-         |- ~q ==> ~p                                                      -}
{- ------------------------------------------------------------------------- -}

contrapos :: Thm -> Failing Thm
contrapos th = do
  (p,q) <- dest_imp (concl th)
  x <- imp_add_concl FF th
  y <- iff_imp1 (axiom_not q)
  z <- (imp_trans y x)
  iff_imp2 (axiom_not p) >>= imp_trans z

{- ------------------------------------------------------------------------- -}
{- |- true                                                                   -}
{- ------------------------------------------------------------------------- -}

truth :: Failing Thm
truth = do
  x <- imp_refl FF
  y <- iff_imp2 axiom_true
  modusponens y x

{- ------------------------------------------------------------------------- -}
{-  |- (p' ==> p) ==> (q ==> q') ==> (p ==> q) ==> (p' ==> q')               -}
{- ------------------------------------------------------------------------- -}

imp_mono_th :: Formula FOL -> Formula FOL -> Formula FOL -> Formula FOL -> Failing Thm
imp_mono_th p p' q q' = do
  th1 <- imp_trans_th (Imp p q) (Imp p' q) (Imp p' q')
  th2 <- imp_trans_th p' q q'
  th3 <- imp_trans_th p' p q >>= imp_swap
  x <- imp_trans th2 th1
  y <- imp_swap x
  imp_trans th3 y

{- ------------------------------------------------------------------------- -}
{- |- (q ==> false) ==> p ==> (p ==> q) ==> false                            -}
{- ------------------------------------------------------------------------- -}

imp_truefalse :: Formula FOL -> Formula FOL -> Failing Thm
imp_truefalse p q = do
  x <- (imp_swap_th (Imp p q) p FF)
  y <- (imp_trans_th p q FF)
  imp_trans y x

{- ------------------------------------------------------------------------- -}
{-         |- p ==> q1   ...   |- p ==> qn   |- q1 ==> ... ==> qn ==> r      -}
{-        --------------------------------------------------------------     -}
{-                             |- p ==> r                                    -}
{- ------------------------------------------------------------------------- -}

imp_trans_chain :: [Thm] -> Thm -> Failing Thm
imp_trans_chain ths th =
  (imp_trans (head ths) th) >>= (itlistM (\a b -> do {x <- imp_swap b; y <- imp_trans a x; imp_unduplicate y}) (reverse (tail ths)))

{- ------------------------------------------------------------------------- -}
{-  |- p ==> q ==> r        |- r ==> s                                       -}
{- ------------------------------------ imp_trans2                           -}
{-      |- p ==> q ==> s                                                     -}
{- ------------------------------------------------------------------------- -}

imp_trans2 :: Thm -> Thm -> Failing Thm
imp_trans2 th1 th2 = do
  p <- antecedent (concl th1)
  (q,r) <- consequent (concl th1) >>= dest_imp
  (r',s) <- dest_imp (concl th2)
  x <- imp_trans_th q r s
  y <- modusponens x th2
  th <- imp_add_assum p y
  modusponens th th1

{- ------------------------------------------------------------------------- -}
{-                                                                           -}
{-         ------------------------------------------- ex_falso (p)          -}
{-                 |- false ==> p                                            -}
{- ------------------------------------------------------------------------- -}

ex_falso :: Formula FOL -> Failing Thm
ex_falso p = right_doubleneg (axiom_addimp FF (Imp p FF))

{- ------------------------------------------------------------------------- -}
{-         |- p ==> (q ==> false) ==> false                                  -}
{-       ----------------------------------- right_doubleneg                 -}
{-               |- p ==> q                                                  -}
{- ------------------------------------------------------------------------- -}

right_doubleneg :: Thm -> Failing Thm
right_doubleneg th = do
  (p,q) <- consequent (concl th) >>= antecedent >>= dest_imp
  r <- consequent (concl th) >>= consequent
  if (q == FF && r == FF) then
    imp_trans th (axiom_doubleneg p)
  else
    failure "right_doubleneg"

{- ------------------------------------------------------------------------- -}
{-         |- p ==> q      |- q ==> p                                        -}
{-        ---------------------------- imp_antisym                           -}
{-              |- p <=> q                                                   -}
{- ------------------------------------------------------------------------- -}

imp_antisym :: Thm -> Thm -> Failing Thm
imp_antisym th1 th2 = do
  (p,q) <- dest_imp (concl th1)
  x <- modusponens (axiom_impiff p q) th1
  modusponens x th2

{- ------------------------------------------------------------------------- -}
{-                 |- p <=> q                                                -}
{-                ------------ iff_imp2                                      -}
{-                 |- q ==> p                                                -}
{- ------------------------------------------------------------------------- -}

iff_imp2 :: Thm -> Failing Thm
iff_imp2 th = do
  (p,q) <- dest_iff (concl th)
  modusponens (axiom_iffimp2 p q) th

{- ------------------------------------------------------------------------- -}
{-                 |- p <=> q                                                -}
{-                ------------ iff_imp1                                      -}
{-                 |- p ==> q                                                -}
{- ------------------------------------------------------------------------- -}

iff_imp1 :: Thm -> Failing Thm
iff_imp1 th = do
  (p,q) <- dest_iff (concl th)
  modusponens (axiom_iffimp1 p q) th

{- ------------------------------------------------------------------------- -}
{- If |- p ==> q ==> r and |- p ==> q then |- p ==> r.                       -}
{- ------------------------------------------------------------------------- -}

right_mp :: Thm -> Thm -> Failing Thm
right_mp ith th = do
  x <- imp_swap ith
  y <- imp_trans th x
  imp_unduplicate y

{- ------------------------------------------------------------------------- -}
{-  |- (p ==> q ==> r) ==> (s ==> t ==> u)                                   -}
{- -----------------------------------------                                 -}
{-  |- (q ==> p ==> r) ==> (t ==> s ==> u)                                   -}
{- ------------------------------------------------------------------------- -}

imp_swap2 :: Thm -> Failing Thm
imp_swap2 th = do
  (pqr,stu) <- dest_imp (concl th)
  (p,qr) <- dest_imp pqr
  (q,r) <- dest_imp qr
  (s,tu) <- dest_imp stu
  (t,u) <- dest_imp tu
  x <- imp_swap_th s t u
  y <- imp_trans th x
  z <- imp_swap_th q p r
  imp_trans z y

{- ------------------------------------------------------------------------- -}
{- |- (p ==> q ==> r) ==> (q ==> p ==> r)                                    -}
{- ------------------------------------------------------------------------- -}

imp_swap_th :: Formula FOL -> Formula FOL -> Formula FOL -> Failing Thm
imp_swap_th p q r =
  (imp_add_concl (Imp p r) (axiom_addimp q p)) >>= imp_trans (axiom_distribimp p q r)

{- ------------------------------------------------------------------------- -}
{-                 |- p ==> q                                                -}
{-         ------------------------------- imp_add_concl r                   -}
{-          |- (q ==> r) ==> (p ==> r)                                       -}
{- ------------------------------------------------------------------------- -}

imp_add_concl :: Formula FOL -> Thm -> Failing Thm
imp_add_concl r th = do
  (p,q) <- dest_imp (concl th)
  x <- imp_trans_th p q r
  y <- imp_swap x
  modusponens y th

{- ------------------------------------------------------------------------- -}
{- |- (q ==> r) ==> (p ==> q) ==> (p ==> r)                                  -}
{- ------------------------------------------------------------------------- -}

imp_trans_th :: Formula FOL -> Formula FOL -> Formula FOL -> Failing Thm
imp_trans_th p q r =
   imp_trans (axiom_addimp (Imp q r) p)
             (axiom_distribimp p q r)

{- ------------------------------------------------------------------------- -}
{-                 |- p ==> q ==> r                                          -}
{-              ---------------------- imp_swap                              -}
{-                 |- q ==> p ==> r                                          -}
{- ------------------------------------------------------------------------- -}

imp_swap :: Thm -> Failing Thm
imp_swap th = do
  (p,qr) <- dest_imp (concl th)
  (q,r) <- dest_imp qr
  (modusponens (axiom_distribimp p q r) th) >>= imp_trans (axiom_addimp q p)

{- ------------------------------------------------------------------------- -}
{-                 |- p ==> r                                                -}
{-         -------------------------- imp_insert q                           -}
{-              |- p ==> q ==> r                                             -}
{- ------------------------------------------------------------------------- -}

imp_insert :: Formula FOL -> Thm -> Failing Thm
imp_insert q th = do
  (p,r) <- dest_imp (concl th)
  imp_trans th (axiom_addimp r q)

{- ------------------------------------------------------------------------- -}
{-            |- p ==> q              |- q ==> r                             -}
{-         ----------------------------------------- imp_trans               -}
{-                 |- p ==> r                                                -}
{- ------------------------------------------------------------------------- -}

imp_trans :: Thm -> Thm -> Failing Thm
imp_trans th1 th2 = do
  p <- antecedent (concl th1)
  x <- imp_add_assum p th2
  modusponens x th1

{- ------------------------------------------------------------------------- -}
{-                   |- q ==> r                                              -}
{-         --------------------------------------- imp_add_assum p           -}
{-           |- (p ==> q) ==> (p ==> r)                                      -}
{- ------------------------------------------------------------------------- -}

imp_add_assum :: Formula FOL -> Thm -> Failing Thm
imp_add_assum p th = do
  (q,r) <- dest_imp (concl th)
  add_assum p th >>= modusponens (axiom_distribimp p q r)

{- ------------------------------------------------------------------------- -}
{-                           |- q                                            -}
{-         ------------------------------------------------ add_assum (p)    -}
{-                         |- p ==> q                                        -}
{- ------------------------------------------------------------------------- -}

add_assum :: Formula FOL -> Thm -> Failing Thm
add_assum p th =
   let q = concl th in
   modusponens (axiom_addimp q p) th

{- ------------------------------------------------------------------------- -}
{-                 |- p ==> p ==> q                                          -}
{-               -------------------- imp_unduplicate                        -}
{-                 |- p ==> q                                                -}
{- ------------------------------------------------------------------------- -}

imp_unduplicate :: Thm -> Failing Thm
imp_unduplicate th = do
  (p,q_) <- dest_imp (concl th)
  q <- consequent q_
  x <- (modusponens (axiom_distribimp p p q) th)
  y <- imp_refl p
  modusponens x y

{- ------------------------------------------------------------------------- -}
{- |- p ==> p                                                                -}
{- ------------------------------------------------------------------------- -}

imp_refl :: Formula FOL -> Failing Thm
imp_refl p = do
   x <- (modusponens (axiom_distribimp p (Imp p p) p) (axiom_addimp p (Imp p p)))
   modusponens x (axiom_addimp p p)

negativef :: Formula t -> Bool
negativef (Imp p FF) = True
negativef _ = False

negatef :: Formula t -> Formula t
negatef (Imp p FF) = p
negatef p = Imp p FF
