module Main where

import SampleProofs

testsuite = mapM_ (\(name,thm) -> putStrLn name >> print thm)
  [("left_cancellation_thm",left_cancellation_thm),
   ("right_identity_thm",right_identity_thm),
   ("right_inverse_thm",right_inverse_thm),
   ("right_cancellation_thm",right_cancellation_thm),
   ("identity_unique_thm",identity_unique_thm),
   ("inverses_unique_thm",inverses_unique_thm)]

main = testsuite
