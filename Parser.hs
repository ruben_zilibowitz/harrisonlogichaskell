{-# LANGUAGE NoMonomorphismRestriction, FlexibleContexts, FlexibleInstances #-}
module Parser where

-- Parsing expressions and statements
-- https://wiki.haskell.org/Parsing_expressions_and_statements

import Text.Parsec
import Text.Parsec.String
import Text.Parsec.Expr
import Text.Parsec.Token
import Text.Parsec.Language

import Types hiding (try)

instance Read PrologRule where
   readsPrec n str = [(parseProlog str,"")]

instance Read (Formula FOL) where
   readsPrec n str = [(parseFOL str,"")]

instance Read (Formula Prop) where
   readsPrec n str = [(parsePL str,"")]

parseProlog str = either (error . show) id $ parse prologparser "" str
parseFOL str = either (error . show) id $ parse folparser "" str
parsePL str = either (error . show) id $ parse propparser "" str
parseFOLTerm str = either (error . show) id $ parse folsubterm "" str

def = emptyDef{ identStart = letter
              , identLetter = alphaNum
              , opStart = oneOf "~&|=<:*/+->"
              , opLetter = oneOf "~&|=><:-"
              , reservedOpNames = ["~", "&", "|", "<==>", "==>", ":-", "::", "*", "/", "+", "-"] ++ predicate_infix_symbols
              , reservedNames = ["true", "false", "exists", "forall"] ++ constants
              }

TokenParser{ parens = m_parens
           , angles = m_angles
--           , brackets = m_brackets
           , symbol = m_symbol
           , integer = m_integer
           , identifier = m_identifier
           , reservedOp = m_reservedOp
           , reserved = m_reserved
--           , semiSep1 = m_semiSep1
           , whiteSpace = m_whiteSpace } = makeTokenParser def

prologparser = try (do
   left <- folparser
   m_symbol ":-"
   right <- sepBy folparser (m_symbol ",")
   return (Prolog right left))
   <|> (do
   left <- folparser
   return (Prolog [] left))
   <?> "prolog expression"

propparser = exprparser propterm
folparser = exprparser folterm

exprparser term = buildExpressionParser table term <?> "expression"
 where
  table = [ [Prefix (m_reservedOp "~" >> return Not)]
          , [Infix (m_reservedOp "&" >> return And) AssocRight]
          , [Infix (m_reservedOp "|" >> return Or) AssocRight]
          , [Infix (m_reservedOp "<==>" >> return Iff) AssocRight]
          , [Infix (m_reservedOp "==>" >> return Imp) AssocRight]
          ]

propterm = m_parens propparser
       <|> fmap (Atom . P) m_identifier
       <|> (m_reserved "true" >> return TT)
       <|> (m_reserved "false" >> return FF)

folterm = try (m_parens folparser)
       <|> try folpredicate_infix
       <|> folpredicate
       <|> existentialQuantifier
       <|> forallQuantifier
       <|> (m_reserved "true" >> return TT)
       <|> (m_reserved "false" >> return FF)

existentialQuantifier = quantifier "exists" Exists
forallQuantifier = quantifier "forall" Forall

quantifier name op = do
   m_reserved name
   is <- many1 m_identifier
   m_symbol "."
   fm <- folparser
   return (foldr op fm is)

predicate_infix_symbols = ["=","<",">","<=",">="]

folpredicate_infix = choice (map (try.app) predicate_infix_symbols)
 where
  app op = do
   x <- folsubterm
   m_reservedOp op
   y <- folsubterm
   return (Atom (R op [x,y]))

folpredicate = do
   p <- m_identifier <|> m_symbol "|--"
   xs <- option [] (m_parens (sepBy1 folsubterm (m_symbol ",")))
   return (Atom (R p xs))

folfunction = do
   fname <- m_identifier
   xs <- m_parens (sepBy1 folsubterm (m_symbol ","))
   return (Fn fname xs)

folconstant_numeric = do
   i <- m_integer
   return (Fn (show i) [])

folconstant_reserved str = do
   m_reserved str
   return (Fn str [])

folconstant = do
   name <- m_angles m_identifier
   return (Fn name [])

folsubterm = folfunction_infix <|> folsubterm_prefix

constants = ["nil"]

folsubterm_prefix =
   m_parens folfunction_infix
   <|> try folfunction
   <|> choice (map folconstant_reserved constants)
   <|> folconstant_numeric
   <|> folconstant
   <|> (fmap Var m_identifier)

folfunction_infix = buildExpressionParser table folsubterm_prefix <?> "expression"
 where
  table = [ [Infix (m_reservedOp "::" >> return (\x y -> Fn "::" [x,y])) AssocRight]
            , [Infix (m_reservedOp "*" >> return (\x y -> Fn "*" [x,y])) AssocLeft, Infix (m_reservedOp "/" >> return (\x y -> Fn "/" [x,y])) AssocLeft]
            , [Infix (m_reservedOp "+" >> return (\x y -> Fn "+" [x,y])) AssocLeft, Infix (m_reservedOp "-" >> return (\x y -> Fn "-" [x,y])) AssocLeft]
          ]
