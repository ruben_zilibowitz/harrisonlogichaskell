module Presburger where

import Prelude hiding (negate,sum,pred)
import qualified Data.Set as S
import Data.List (intercalate,minimumBy,maximumBy,find,partition,delete)
import Data.Maybe
import Data.Either (isRight)
import qualified Data.Map as M
import Debug.Trace

import PropositionalLogic hiding (nnf)
import FirstOrderLogic hiding (replace)
import Types
import Parser
import DP
import Equality

-- Section 5.7

linear_cmul :: Integer -> Term -> Failing Term
linear_cmul 0 tm = Right zero
linear_cmul n (Fn "+" [Fn "*" [c, x], r]) = do
  a <- linear_cmul n r
  b <- numeral1 (* n) c
  return (Fn "+" [Fn "*" [b, x], a])
linear_cmul n k = numeral1 (* n) k

mk_numeral :: Show a => a -> Term
mk_numeral n = Fn (show n) []

dest_numeral :: Term -> Failing Integer
dest_numeral (Fn ns []) = Right (read ns)
dest_numeral _ = Left "dest_numeral"

is_numeral :: Term -> Bool
is_numeral = isRight . dest_numeral

numeral1 :: Show a => (Integer -> a) -> Term -> Failing Term
numeral1 fn n = do
  x <- dest_numeral n
  return (mk_numeral (fn x))

numeral2
  :: Show a =>
     (Integer -> Integer -> a) -> Term -> Term -> Failing Term
numeral2 fn m n = do
  x <- dest_numeral m
  y <- dest_numeral n
  return (mk_numeral (fn x y))

zero :: Term
zero = Fn "0" []
