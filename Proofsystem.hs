module Proofsystem (
   Thm,
   Proven,
   modusponens,
   gen,
   axiom_addimp,
   axiom_distribimp,
   axiom_doubleneg,
   axiom_allimp,
   axiom_impall,
   axiom_existseq,
   axiom_eqrefl,
   axiom_funcong,
   axiom_predcong,
   axiom_iffimp1,
   axiom_iffimp2,
   axiom_impiff,
   axiom_true,
   axiom_not,
   axiom_and,
   axiom_or,
   axiom_exists,
   concl
) where

import Prelude hiding (negate,sum,pred)
import qualified Data.Set as S
import Data.List (intercalate,minimumBy,maximumBy,find,partition,delete,findIndex,sortBy)
import Data.Maybe
import Data.Either (isRight)
import qualified Data.Map as M
import Debug.Trace

import PropositionalLogic hiding (nnf)
import FirstOrderLogic hiding (replace)
import Types
import Failing
import Parser
import DP
import Equality

newtype Proven a = Pr a

instance (Show a) => Show (Proven a) where
   show (Pr thm) = "|- " ++ (show thm)

type Thm = Proven (Formula FOL)

-------------------
------ Proofsystem
-------------------

modusponens :: Thm -> Thm -> Failing Thm
gen :: String -> Thm -> Thm
axiom_addimp :: Formula FOL -> Formula FOL -> Thm
axiom_distribimp :: Formula FOL -> Formula FOL -> Formula FOL -> Thm
axiom_doubleneg :: Formula FOL -> Thm
axiom_allimp :: String -> Formula FOL -> Formula FOL -> Thm
axiom_impall :: String -> Formula FOL -> Failing Thm
axiom_existseq :: String -> Term -> Failing Thm
axiom_eqrefl :: Term -> Thm
axiom_funcong :: String -> [Term] -> [Term] -> Thm
axiom_predcong :: String -> [Term] -> [Term] -> Thm
axiom_iffimp1 :: Formula FOL -> Formula FOL -> Thm
axiom_iffimp2 :: Formula FOL -> Formula FOL -> Thm
axiom_impiff :: Formula FOL -> Formula FOL -> Thm
axiom_true :: Thm
axiom_not :: Formula FOL -> Thm
axiom_and :: Formula FOL -> Formula FOL -> Thm
axiom_or :: Formula FOL -> Formula FOL -> Thm
axiom_exists :: String -> Formula FOL -> Thm
concl :: Thm -> Formula FOL

modusponens thmA thmB =
  let (Imp p' q) = concl thmA
      p = concl thmB in
  case p == p' of
    True -> return (Pr q)
    False -> failure "modusponens"

gen x thm =
  let p = concl thm in
  Pr (Forall x p)

axiom_addimp p q = Pr (Imp p (Imp q p))

axiom_distribimp p q r = Pr (Imp (Imp p (Imp q r)) (Imp (Imp p q) (Imp p r)))

axiom_doubleneg p = Pr (Imp (Imp (Imp p FF) FF) p)

axiom_allimp x p q = Pr (Imp (Forall x (Imp p q)) (Imp (Forall x p) (Forall x q)))

axiom_impall x p
  | not (free_in (Var x) p) = return (Pr (Imp p (Forall x p)))
  | otherwise = failure "axiom_impall: variable free in formula"

axiom_existseq x t
  | not (occurs_in (Var x) t) = return (Pr (Exists x (mk_eq (Var x) t)))
  | otherwise = failure "axiom_existseq: variable free in term"

axiom_eqrefl t = Pr (mk_eq t t)

axiom_funcong f lefts rights = Pr (itlist2 (\s t p -> Imp (mk_eq s t) p) lefts rights (mk_eq (Fn f lefts) (Fn f rights)))

axiom_predcong p lefts rights = Pr (itlist2 (\s t p -> Imp (mk_eq s t) p) lefts rights (Imp (Atom (R p lefts)) (Atom (R p rights))))

axiom_iffimp1 p q = Pr (Imp (Iff p q) (Imp p q))

axiom_iffimp2 p q = Pr (Imp (Iff p q) (Imp q p))

axiom_impiff p q = Pr (Imp (Imp p q) (Imp (Imp q p) (Iff p q)))

axiom_true = Pr (Iff TT (Imp FF FF))

axiom_not p = Pr (Iff (Not p) (Imp p FF))

axiom_and p q = Pr (Iff (And p q) (Imp (Imp p (Imp q FF)) FF))

axiom_or p q = Pr (Iff (Or p q) (Not (And (Not p) (Not q))))

axiom_exists x p = Pr (Iff (Exists x p) (Not (Forall x (Not p))))

concl (Pr p) = p

free_in :: Term -> Formula FOL -> Bool
free_in t FF = False
free_in t TT = False
free_in t (Atom (R p args)) = any (occurs_in t) args
free_in t (Not p) = free_in t p
free_in t (And p q) = free_in t p || free_in t q
free_in t (Or p q) = free_in t p || free_in t q
free_in t (Imp p q) = free_in t p || free_in t q
free_in t (Iff p q) = free_in t p || free_in t q
free_in t (Forall y p) = not (occurs_in (Var y) t) && free_in t p
free_in t (Exists y p) = not (occurs_in (Var y) t) && free_in t p

occurs_in :: Term -> Term -> Bool
occurs_in s t | s == t = True
occurs_in s (Var _) = False
occurs_in s (Fn f args) = any (occurs_in s) args

-------------------
------ End Proofsystem
-------------------



{-
	ewd1062_1 = lcffol $ parseFOL "(forall x. x <= x) & (forall x y z. x <= y & y <= z => x <= z) & (forall x y. f(x) <= y <=> x <= g(y)) => (forall x y. x <= y => f(x) <= f(y))"

	p58 = lcffol (parseFOL "forall x. exists v. exists w. forall y. forall z. ((P(x) & Q(y)) => ((P(v) | R(w)) & (R(z) => Q(v))))")

	lcffol fm =
	  let fvs = S.toList $ fv fm in
	  let fm' = Imp (itlist Forall fvs fm) FF in
	  let th1 = deepen (\n -> lcfrefute fm' n deskolcont) 0 in
	  let th2 = modusponens (axiom_doubleneg (negatef fm')) th1 in
	  itlist (\v -> fromRight . (spec (Var v))) (reverse fvs) th2

	deskolcont thp (env,sks,k) =
	  let ifn = tsubst (solve env) in
	  let isk = setify (map (\(p,t) -> (onformula ifn p,ifn t)) sks) in
	  let ssk = sortBy (decreasing (termsize . snd)) isk in
	  let vs = map (\i -> Var ("Y_" ++ show i)) [1 .. length ssk] in
	  let vfn = replacet (itlist2 (\(p,t) v -> M.insert t v) ssk vs M.empty) in
	  let th = thp (vfn . ifn, onformula vfn (itlist mk_skol ssk FF)) in
	  Right $ repeatTo (elim_skolemvar . imp_swap) th

	elim_skolemvar :: Formula FOL -> Either String (Formula FOL)
	elim_skolemvar th =
	  case concl th of
	    Imp (Imp pv (apx@(Forall x px))) q ->
	        let [th1,th2] = map (imp_trans (imp_add_concl FF th))
	                            (imp_false_conseqs pv apx) in
	        let v = S.findMin (S.insert x ((fv pv) S.\\ (fv apx))) in
	        let th3 = gen_right v th1 in
	        let th4 = imp_trans th3 (alpha x (fromRight $ consequent (concl th3))) in
	        Right (modusponens (axiom_doubleneg q) (right_mp th2 th4))
	    _ -> Left "elim_skolemvar"

	lcfrefute_eg2 = lcfrefute (parseFOL "(exists x. ~p(x)) & (forall x. p(x))") 1 simpcont
	lcfrefute_eg1 = lcfrefute (parseFOL "p(1) & ~q(1) & (forall x. p(x) => q(x))") 1 simpcont

	simpcont thp (env,sks,k) =
	  let ifn = tsubst (solve env) in
	  Right $ thp (ifn,onformula ifn (itlist mk_skol sks FF))

	mk_skol :: (Formula FOL, Term) -> Formula FOL -> Formula FOL
	mk_skol (Forall y p,fx) q =
	  Imp (Imp (subst (M.singleton y fx) p) (Forall y p)) q

	lcfrefute fm n cont =
	  let sl = skolemfuns fm in
	  let find_skolem fm0 = tryfind (findf fm0) sl in
	  lcftab find_skolem ([fm],[],n) cont (M.empty,[],0)
	 where
	  findf fm0 (f,t) = do
	    x <- form_match (f,fm0) M.empty
	    return (tsubst x t)

	form_match
	  :: (Formula FOL, Formula FOL)
	     -> M.Map String Term -> Either String (M.Map String Term)
	form_match (FF,FF) env = Right env
	form_match (TT,TT) env = Right env
	form_match (Atom (R p pa),Atom (R q qa)) env = term_match env [(Fn p pa,Fn q qa)]
	form_match (Not p1,Not p2) env = form_match (p1,p2) env
	form_match (And p1 q1,And p2 q2) env = form_match_helper1 env p1 q1 p2 q2
	form_match (Or p1 q1,Or p2 q2) env = form_match_helper1 env p1 q1 p2 q2
	form_match (Imp p1 q1,Imp p2 q2) env = form_match_helper1 env p1 q1 p2 q2
	form_match (Iff p1 q1,Iff p2 q2) env = form_match_helper1 env p1 q1 p2 q2
	form_match (Exists x1 p1,Exists x2 p2) env | x1 == x2 = form_match_helper2 env x1 p1 x2 p2
	form_match (Forall x1 p1,Forall x2 p2) env | x1 == x2 = form_match_helper2 env x1 p1 x2 p2
	form_match _ _ = Left "form_match"

	form_match_helper1 env p1 q1 p2 q2 = form_match (q1,q2) env >>= form_match (p1,p2)
	form_match_helper2 env x1 p1 x2 p2 = let z = variant x1 (S.union (fv p1) (fv p2)) in
	  let inst_fn = subst (M.singleton x1 (Var z)) in
	  form_match (inst_fn p1,inst_fn p2) env >>= return . (M.delete z)

	skolemfuns :: Formula FOL -> [(Formula FOL, Term)]
	skolemfuns fm =
	  let fns = S.map fst (functions fm) in
	  let skts = S.map (\y -> case y of {(Exists x p) -> Forall x (Not p); p -> p})
	                 (quantforms True fm) in
	  let skofun i ap@(Forall y p) = let vars = S.map Var (fv ap) in (ap,Fn (variant ("f" ++ "_" ++ show i) fns) (S.toList vars)) in
	  zipWith skofun [1 .. length skts] (S.toList skts)

	quantforms :: Ord t => Bool -> Formula t -> S.Set (Formula t)
	quantforms e fm =
	  case fm of
	    Not p -> quantforms (not e) p
	    And p q -> S.union (quantforms e p) (quantforms e q)
	    Or p q -> S.union (quantforms e p) (quantforms e q)
	    Imp p q -> quantforms e (Or (Not p) q)
	    Iff p q -> quantforms e (Or (And p q) (And (Not p) (Not q)))
	    Exists x p -> if e then S.insert fm (quantforms e p) else quantforms e p
	    Forall x p -> if e then quantforms e p else S.insert fm (quantforms e p)
	    _ -> S.empty

	lcftab
	  :: (Num a, Num t, Ord a, Show t) =>
	     (Formula FOL -> Either String Term)
	     -> ([Formula FOL], [Formula FOL], a)
	     -> (((Term -> Term, Formula FOL) -> Formula FOL)
	         -> (M.Map String Term, [(Formula FOL, Term)], t)
	         -> Either String b)
	     -> (M.Map String Term, [(Formula FOL, Term)], t)
	     -> Either String b

	lcftab _ (_,_,n) _ _ | n < 0 = Left "lcftab: no proof"
	lcftab _ ([],_,_) _ _ = Left "lcftab: No contradiction"
	lcftab skofun (fms,lits,n) cont esk@(env,sks,k) = blotto
	 where
	  blotto = case fms of
	    FF:fl -> cont (ex_falso' (fl ++ lits)) esk
	    (fm@(Imp p q)):fl | p == q ->
	      lcftab skofun (fl,lits,n) (cont . add_assum' fm) esk
	    (Imp (Imp p q) FF):fl ->
	      lcftab skofun (p:(Imp q FF):fl,lits,n)
	                    (cont . imp_false_rule') esk
	    (Imp p q):fl | q /= FF ->
	      lcftab skofun ((Imp p FF):fl,lits,n)
	        (\th -> lcftab skofun (q:fl,lits,n)
	                                 (cont . imp_true_rule' th)) esk
	    l@((Atom _):fl) -> monkey l
	    l@((Imp (Atom _) FF):fl) -> monkey l
	    (fm@(Forall x p)):fl ->
	      let y = Var ("X_" ++ show k) in
	      lcftab skofun ((subst (M.singleton x y) p):fl ++ [fm],lits,n-1)
	                    (cont . spec' y fm (length fms)) (env,sks,k+1)
	    (Imp (yp@(Forall y p)) FF):fl -> do
	      fx <- skofun yp
	      let p' = subst (M.singleton y fx) p
	      let skh = Imp p' (Forall y p)
	      let sks' = (Forall y p,fx):sks
	      result <- lcftab skofun ((Imp p' FF):fl,lits,n)
	                              (cont . deskol' skh) (env,sks',k)
	      return result
	    fm:fl ->
	      let fm' = fromRight $ consequent (concl (eliminate_connective fm)) in
	      lcftab skofun (fm':fl,lits,n)
	                    (cont . eliminate_connective' fm) esk
	  monkey (p:fl) = (try (tryfind (\p' -> do
	           env' <- unify_complementsf env (p,p')
	           p'i <- maybe (Left "findIndex") Right (findIndex (p' ==) lits)
	           return (cont (complits' (fms,lits) p'i) (env',sks,k))) lits)
	           (lcftab skofun (fl,p:lits,n)
	                          (cont . imp_front' (length fl)) esk))


	deskol' skh thp (e,s) =
	  let th = thp (e,s) in
	  modusponens (use_laterimp (onformula e skh) (concl th)) th

	complits' (p:fl,lits) i (e,s) =
	  let (l1,p':l2) = splitAt i lits in
	  itlist (imp_insert . onformula e) (fl ++ l1)
	         (imp_contr (onformula e p)
	                    (itlist (Imp . onformula e) l2 s))

	ex_falso' fms (e,s) =
	  ex_falso (itlist (Imp . onformula e) fms s)

	spec' y fm n thp (e,s) = noerr $ do
	  let th = imp_swap (imp_front n (thp (e,s)))
	  z <- ispec (e y) (onformula e fm)
	  return (imp_unduplicate (imp_trans z th))

	eliminate_connective' fm thp es@(e,s) =
	  imp_trans (eliminate_connective (onformula e fm)) (thp es)

	add_assum' fm thp es@(e,s) =
	  add_assum (onformula e fm) (thp es)

	imp_front' n thp es = imp_front n (thp es)

	imp_true_rule' th1 th2 es = imp_true_rule (th1 es) (th2 es)

	imp_false_rule' th es = imp_false_rule (th es)

	use_laterimp :: Eq t => Formula t -> Formula t -> Formula t
	use_laterimp i fm =
	  case fm of
	    (Imp (Imp q' (s)) (Imp (Imp q p) r)) | Imp q p == i ->
	        let th1 = axiom_distribimp i (Imp (Imp q s) r) (Imp (Imp p s) r) in
	        let th2 = imp_swap (imp_trans_th q p s) in
	        let th3 = imp_swap (imp_trans_th (Imp p s) (Imp q s) r) in
	        imp_swap2 (modusponens th1 (imp_trans th2 th3))
	    (Imp qs (Imp a b)) ->
	        imp_swap2 (imp_add_assum a (use_laterimp i (Imp qs b)))

	unify_complementsf env ((Atom (R p1 a1)), (Imp (Atom (R p2 a2)) FF)) = unify env [(Fn p1 a1,Fn p2 a2)]
	unify_complementsf env (Imp (Atom (R p1 a1)) FF, Atom (R p2 a2)) = unify env [(Fn p1 a1,Fn p2 a2)]
	unify_complementsf _ _ = Left "unify_complementsf"

	spec t th = do
	  x <- ispec t (concl th)
	  return (modusponens x th)

	tp_eg1 = ispec (parseFOLTerm "y") (parseFOL "forall x y z. x + y + z = z + y + x")

	ispec t fm =
	  case fm of
	    (Forall x p) -> Right
	      (if S.member x (fvt t) then
	        let th = alpha (variant x (S.union (fvt t) (var p))) fm in
	        imp_trans th (fromRight $ ispec t (fromRight $ consequent (concl th)))
	      else subspec (isubst (Var x) t p (subst (M.singleton x t) p)))
	    _ -> Left "ispec: non-universal formula"

	alpha :: String -> Formula FOL -> Formula FOL
	alpha z fm =
	  case fm of
	    (Forall x p) -> let p' = subst (M.singleton x (Var z)) p in
	                    subalpha (isubst (Var x) (Var z) p p')
	    _ -> error "alpha: not a universal formula"

	isubst :: Term -> Term -> Formula FOL -> Formula FOL -> Formula FOL
	isubst s t sfm tfm =
	  if sfm == tfm then add_assum (mk_eq s t) (imp_refl tfm) else
	  case (sfm,tfm) of
	    (Atom (R p sa),Atom (R p' ta)) | (p == p' && length sa == length ta) ->
	        let ths = zipWith (icongruence s t) sa ta in
	        let (ls,rs) = unzip (map (fromRight . dest_eq . fromRight . consequent . concl) ths) in
	        imp_trans_chain ths (axiom_predcong p ls rs)
	    (Imp sp sq,Imp tp tq) ->
	        let th1 = imp_trans (eq_sym s t) (isubst t s tp sp) in
	        let th2 = isubst s t sq tq in
	        imp_trans_chain [th1, th2] (imp_mono_th sp tp sq tq)
	    (Forall x p,Forall y q) ->
	        if x == y then
	          imp_trans (gen_right x (isubst s t p q)) (axiom_allimp x p q)
	        else
	          let z = Var (variant x (S.unions [fv p, fv q, fvt s, fvt t])) in
	          let th1 = isubst (Var x) z p (subst (M.singleton x z) p) in
	          let th2 = isubst z (Var y) (subst (M.singleton y z) q) q in
	          let th3 = subalpha th1 in
	          let th4 = subalpha th2 in
	          let th5 = isubst s t (fromRight $ consequent (concl th3))
	                               (fromRight $ antecedent (concl th4)) in
	          imp_swap (imp_trans2 (imp_trans th3 (imp_swap th5)) th4)
	    _ ->
	        let sth = iff_imp1 (expand_connective sfm) in
	        let tth = iff_imp2 (expand_connective tfm) in
	        let th1 = isubst s t (fromRight $ consequent (concl sth))
	                             (fromRight $ antecedent (concl tth)) in
	        imp_swap (imp_trans sth (imp_swap (imp_trans2 th1 tth)))

	subalpha :: Formula FOL -> Formula FOL
	subalpha th =
	   case concl th of
	    (Imp (Atom (R "=" [Var x,Var y])) (Imp p q)) ->
	        if x == y then genimp x (modusponens th (axiom_eqrefl (Var x)))
	        else gen_right y (subspec th)
	    _ -> error "subalpha: wrong sort of theorem"

	subspec :: Formula FOL -> Formula FOL
	subspec th =
	   case concl th of
	      (Imp (Atom (R "=" [Var x,t])) (Imp p q)) ->
	         let e = (Atom (R "=" [Var x,t])) in
	         let th1 = imp_trans (genimp x (imp_swap th))
	                             (exists_left_th x e q) in
	         modusponens (imp_swap th1) (axiom_existseq x t)
	      _ -> error "subspec: wrong sort of theorem"

	exists_left x th = noerr $ do
	  (p,q) <- dest_imp(concl th)
	  return (modusponens (exists_left_th x p q) (gen x th))

	exists_left_th :: String -> Formula FOL -> Formula FOL -> Formula FOL
	exists_left_th x p q =
	  let p' = Imp p FF in
	  let q' = Imp q FF in
	  let th1 = genimp x (imp_swap (imp_trans_th p q FF)) in
	  let th2 = imp_trans th1 (gen_right_th x q' p') in
	  let th3 = imp_swap (imp_trans_th q' (Forall x p') FF) in
	  let th4 = imp_trans2 (imp_trans th2 th3) (axiom_doubleneg q) in
	  let th5 = imp_add_concl FF (genimp x (iff_imp2 (axiom_not p))) in
	  let th6 = imp_trans (iff_imp1 (axiom_not (Forall x (Not p)))) th5 in
	  let th7 = imp_trans (iff_imp1 (axiom_exists x p)) th6 in
	  imp_swap (imp_trans th7 (imp_swap th4))

	gen_right x th = noerr $ do
	  (p,q) <- dest_imp (concl th)
	  return (modusponens (gen_right_th x p q) (gen x th))

	genimp :: String -> Formula FOL -> Formula FOL
	genimp x th = noerr $ do
	  (p,q) <- dest_imp (concl th)
	  return (modusponens (axiom_allimp x p q) (gen x th))

	gen_right_th :: String -> Formula FOL -> Formula FOL -> Formula FOL
	gen_right_th x p q = imp_swap (imp_trans (axiom_impall x p) (imp_swap (axiom_allimp x p q)))

	-- icongruence (parseFOLTerm "s") (parseFOLTerm "t") (parseFOLTerm "f(s,g(s,t,s),u,h(h(s)))") (parseFOLTerm "f(s,g(t,t,s),u,h(h(t)))")

	icongruence :: Term -> Term -> Term -> Term -> Formula FOL
	icongruence s t stm ttm =
	  if stm == ttm then add_assum (mk_eq s t) (axiom_eqrefl stm)
	  else if stm == s && ttm == t then imp_refl (mk_eq s t) else
	  case (stm,ttm) of
	    (Fn fs sa,Fn ft ta) | (fs == ft && length sa == length ta) ->
	        let ths = zipWith (icongruence s t) sa ta in
	        let ts = fromRight (mapM (consequent . concl) ths) in
	        imp_trans_chain ths (axiom_funcong fs (map (fromRight . lhs) ts) (map (fromRight . rhs) ts))
	    _ -> error "icongruence: not congruent"

	eq_trans s t u =
	  let th1 = axiom_predcong "=" [t, u] [s, u] in
	  let th2 = modusponens (imp_swap th1) (axiom_eqrefl u) in
	  imp_trans (eq_sym s t) th2

	eq_sym :: Term -> Term -> Formula FOL
	eq_sym s t =
	  let rth = axiom_eqrefl s in
	  funpow 2 (\th -> modusponens (imp_swap th) rth)
	           (axiom_predcong "=" [s, s] [t, s])

	lcf_eg3 = lcftaut $ parseFOL "((p <=> q) <=> r) <=> (p <=> (q <=> r))"
	lcf_eg2 = lcftaut $ parseFOL "p & q <=> ((p <=> q) <=> p | q)"
	lcf_eg1 = lcftaut $ parseFOL "(p => q) | (q => p)"

	lcftaut :: Formula FOL -> Formula FOL
	lcftaut p =
	  modusponens (axiom_doubleneg p) (lcfptab [negatef p] [])

	lcfptab :: [Formula FOL] -> [Formula FOL] -> Formula FOL
	lcfptab fms lits =
	  case fms of
	    (FF:fl) ->
	        ex_falso (itlist Imp (fl ++ lits) FF)
	    ((Imp p q):fl) | p == q ->
	        add_assum (Imp p q) (lcfptab fl lits)
	    ((Imp (Imp p q) FF):fl) ->
	        imp_false_rule(lcfptab (p:(Imp q FF):fl) lits)
	    ((Imp p q):fl) | q /= FF ->
	        imp_true_rule (lcfptab ((Imp p FF):fl) lits)
	                      (lcfptab (q:fl) lits)
	    l@((Atom _) : fl) -> monkey l
	    l@((Forall _ _) : fl) -> monkey l
	    l@((Imp (Forall _ _) FF) : fl) -> monkey l
	    l@((Imp (Atom _) FF) : fl) -> monkey l
	    (fm:fl) -> noerr $ do
	        let th = eliminate_connective fm
	        a <- consequent (concl th)
	        return (imp_trans th (lcfptab (a:fl) lits))
	    _ -> error "lcfptab: no contradiction"
	  where monkey (p:fl) = if elem (negatef p) lits then let (l1,l2) = splitAt (fromJust $ findIndex ((negatef p) ==) lits) lits in let th = imp_contr p (itlist Imp (tail l2) FF) in itlist imp_insert (fl ++ l1) th else imp_front (length fl) (lcfptab fl (p:lits))

	imp_front n th = modusponens (imp_front_th n (concl th)) th

	imp_front_th 0 fm = imp_refl fm
	imp_front_th n fm = noerr $ do
	  (p,qr) <- dest_imp fm
	  let th1 = imp_add_assum p (imp_front_th (n - 1) qr)
	  (q',r') <- consequent (concl th1) >>= consequent >>= dest_imp
	  return (imp_trans th1 (imp_swap_th p q' r'))

	imp_contr p q =
	  if negativef p then imp_add_assum (negatef p) (ex_falso q)
	  else imp_swap (imp_add_assum p (ex_falso q))

	imp_true_rule th1 th2 = noerr $ do
	  p <- antecedent (concl th1) >>= antecedent
	  q <- antecedent (concl th2)
	  let th3 = right_doubleneg (imp_add_concl FF th1)
	  let th4 = imp_add_concl FF th2
	  let th5 = imp_swap (imp_truefalse p q)
	  let th6 = imp_add_concl FF (imp_trans_chain [th3, th4] th5)
	  let th7 = imp_swap (imp_refl (Imp (Imp p q) FF))
	  return (right_doubleneg (imp_trans th7 th6))

	imp_false_rule th = noerr $ do
	  (p,r) <- dest_imp (concl th)
	  ar <- antecedent r >>= antecedent
	  return (imp_trans_chain (imp_false_conseqs p ar) th)

	imp_false_conseqs p q =
	 [right_doubleneg (imp_add_concl FF (imp_add_assum p (ex_falso q))),
	  imp_add_concl FF (imp_insert p (imp_refl q))]

	eliminate_connective :: Formula FOL -> Formula FOL
	eliminate_connective fm =
	  if not (negativef fm) then iff_imp1 (expand_connective fm)
	  else imp_add_concl FF (iff_imp2 (expand_connective (negatef fm)))

	expand_connective :: Formula FOL -> Formula FOL
	expand_connective fm =
	  case fm of
	    TT -> axiom_true
	    Not p -> axiom_not p
	    And p q -> axiom_and p q
	    Or p q -> axiom_or p q
	    Iff p q -> iff_def p q
	    Exists x p -> axiom_exists x p
	    _ -> error "expand_connective"

	-- Produce |- (p <=> q) <=> (p ==> q) /\ (q ==> p)
	iff_def p q =
	  let th = and_pair (Imp p q) (Imp q p) in
	  let thl = [axiom_iffimp1 p q, axiom_iffimp2 p q] in
	  imp_antisym (imp_trans_chain thl th) (unshunt (axiom_impiff p q))

	{-
		iff_def p q =
		  let th1 = and_pair (Imp p q) (Imp q p) in
		  let th2 = imp_trans_chain [axiom_iffimp1 p q, axiom_iffimp2 p q] th1 in
		  imp_antisym th2 (unshunt (axiom_impiff p q))
	-}

	unshunt th = noerr $ do
	  (p,qr) <- dest_imp (concl th)
	  (q,r) <- dest_imp qr
	  return (imp_trans_chain [and_left p q, and_right p q] th)

	shunt th = noerr $ do
	  (p,q) <- antecedent (concl th) >>= dest_and
	  return (modusponens (itlist imp_add_assum [p,q] th) (and_pair p q))

	and_pair p q =
	  let th1 = iff_imp2 (axiom_and p q) in
	  let th2 = imp_swap_th (Imp p (Imp q FF)) q FF in
	  let th3 = imp_add_assum p (imp_trans2 th2 th1) in
	  modusponens th3 (imp_swap (imp_refl (Imp p (Imp q FF))))

	-- |- p1 /\ ... /\ pn ==> pi for each 1 <= i <= n (input term right assoc)
	conjths fm =
	  try (do (p,q) <- dest_and fm
	          return ((and_left p q):(map (imp_trans (and_right p q)) (conjths q))))
	  [imp_refl fm]

	and_right p q =
	  let th1 = axiom_addimp (Imp q FF) p in
	  let th2 = right_doubleneg(imp_add_concl FF th1) in
	  imp_trans (iff_imp1 (axiom_and p q)) th2

	and_left p q =
	  let th1 = imp_add_assum p (axiom_addimp FF q) in
	  let th2 = right_doubleneg (imp_add_concl FF th1) in
	  imp_trans (iff_imp1 (axiom_and p q)) th2

	contrapos th = noerr $ do
	  (p,q) <- dest_imp (concl th)
	  return (imp_trans (imp_trans (iff_imp1 (axiom_not q)) (imp_add_concl FF th))
	            (iff_imp2 (axiom_not p)))

	truth = modusponens (iff_imp2 axiom_true) (imp_refl FF)

	imp_mono_th p p' q q' =
	  let th1 = imp_trans_th (Imp p q) (Imp p' q) (Imp p' q') in
	  let th2 = imp_trans_th p' q q' in
	  let th3 = imp_swap(imp_trans_th p' p q) in
	  imp_trans th3 (imp_swap(imp_trans th2 th1))

	imp_truefalse p q =
	  imp_trans (imp_trans_th p q FF) (imp_swap_th (Imp p q) p FF)


	--         |- p ==> q1   ...   |- p ==> qn   |- q1 ==> ... ==> qn ==> r 
	--        --------------------------------------------------------------
	--                             |- p ==> r                               
	imp_trans_chain
	 ths th =
	  itlist (\a b -> imp_unduplicate (imp_trans a (imp_swap b)))
	         (reverse (tail ths)) (imp_trans (head ths) th)

	imp_trans2 th1 th2 =
	  let Imp p (Imp q r) = concl th1 in
	  let (Imp r' s) = concl th2 in
	  let th = imp_add_assum p (modusponens (imp_trans_th q r s) th2) in
	  modusponens th th1

	ex_falso p = right_doubleneg (axiom_addimp FF (Imp p FF))

	right_doubleneg th =
	  case concl th of
	    Imp _ (Imp (Imp p FF) FF) -> imp_trans th (axiom_doubleneg p)
	    _ -> error "right_doubleneg"

	imp_antisym th1 th2 = noerr $ do
	  (p,q) <- dest_imp (concl th1)
	  return (modusponens (modusponens (axiom_impiff p q) th1) th2)

	iff_imp2 th = noerr $ do
	  (p,q) <- dest_iff(concl th)
	  return (modusponens (axiom_iffimp2 p q) th)

	iff_imp1 th = noerr $ do
	  (p,q) <- dest_iff (concl th)
	  return (modusponens (axiom_iffimp1 p q) th)

	right_mp ith th =
	  imp_unduplicate (imp_trans th (imp_swap ith))

	imp_swap2 th =
	  case concl th of
	    Imp (Imp p (Imp q r)) (Imp s (Imp t u)) ->
	        imp_trans (imp_swap_th q p r) (imp_trans th (imp_swap_th s t u))
	    _ -> error "imp_swap2"

	imp_swap_th p q r =
	  imp_trans (axiom_distribimp p q r)
	            (imp_add_concl (Imp p r) (axiom_addimp q p))

	imp_add_concl r th = noerr $ do
	  (p,q) <- dest_imp (concl th)
	  return (modusponens (imp_swap (imp_trans_th p q r)) th)

	imp_trans_th p q r =
	   imp_trans (axiom_addimp (Imp q r) p)
	             (axiom_distribimp p q r)

	imp_swap :: Eq t => Formula t -> Formula t
	imp_swap th = noerr $ do
	  (p,qr) <- dest_imp (concl th)
	  (q,r) <- dest_imp qr
	  return (imp_trans (axiom_addimp q p)
	            (modusponens (axiom_distribimp p q r) th))

	imp_insert :: Eq t => Formula t -> Formula t -> Formula t
	imp_insert q th = noerr $ do
	  (p,r) <- dest_imp (concl th)
	  return (imp_trans th (axiom_addimp r q))

	imp_trans :: Eq t => Formula t -> Formula t -> Formula t
	imp_trans th1 th2 = noerr $ do
	  p <- antecedent (concl th1)
	  return (modusponens (imp_add_assum p th2) th1)

	imp_add_assum :: Eq t => Formula t -> Formula t -> Formula t
	imp_add_assum p th = noerr $ do
	  (q,r) <- dest_imp (concl th)
	  return (modusponens (axiom_distribimp p q r) (add_assum p th))

	add_assum :: ProofsystemPL thm => thm -> thm -> thm
	add_assum p th = modusponens (axiom_addimp (concl th) p) th

	negativef :: Formula t -> Bool
	negativef (Imp p FF) = True
	negativef _ = False

	negatef :: Formula t -> Formula t
	negatef (Imp p FF) = p
	negatef p = Imp p FF

	-- |- P => P => Q
	-- ---------------
	--   |- P => Q
	imp_unduplicate :: Formula FOL -> Formula FOL
	imp_unduplicate th = noerr $ do
	  (p,pq) <- dest_imp (concl th)
	  q <- consequent pq
	  return (modusponens (modusponens (axiom_distribimp p p q) th) (imp_refl p))

	-- |- P => P
	imp_refl :: Formula FOL -> Formula FOL
	imp_refl p = modusponens (modusponens (axiom_distribimp p (Imp p p) p) (axiom_addimp p (Imp p p))) (axiom_addimp p p)

	data Proven a = Pr a | Fl String

	instance (Show a) => Show (Proven a) where
	   show (Pr thm) = "|- " ++ (show thm)
	   show (Fl err) = "<<" ++ err ++ ">>"

	instance Functor Proven where
	    fmap f (Pr a) = Pr (f a)
	    fmap _ (Fl e) = Fl e

	instance Applicative Proven where
	    pure = Pr
	    Pr f <*> m = fmap f m
	    Fl e <*> _ = Fl e

	instance Monad Proven where
	   return = Pr
	   fail e = Fl e
	   Fl e >>= _ = Fl e
	   (Pr thm) >>= k = k thm

	instance ProofsystemFOL (Formula FOL) Term where
	  axiom_eqrefl t = mk_eq t t
	  axiom_existseq x t
	     | not (occurs_in (Var x) t) = Exists x (mk_eq (Var x) t)
	     | otherwise = error "axiom_existseq: variable free in term"
	  axiom_funcong f lefts rights = itlist2 (\s t p -> Imp (mk_eq s t) p) lefts rights (mk_eq (Fn f lefts) (Fn f rights))
	  axiom_predcong p lefts rights = itlist2 (\s t p -> Imp (mk_eq s t) p) lefts rights (Imp (Atom (R p lefts)) (Atom (R p rights)))

	instance ProofsystemQuant (Formula FOL) where
	  gen x p = Forall x p
	  axiom_allimp x p q = Imp (Forall x (Imp p q)) (Imp (Forall x p) (Forall x q))
	  axiom_impall x p
	     | not (free_in (Var x) p) = Imp p (Forall x p)
	     | otherwise = error "axiom_impall: variable free in formula"
	  axiom_exists x p = Iff (Exists x p) (Not (Forall x (Not p)))

	instance (Eq a) => ProofsystemPL (Formula a) where
	  modusponens (Imp p' q) p
	    | p == p' = q
	    | otherwise = error "modusponens"
	  axiom_addimp p q = Imp p (Imp q p)
	  axiom_distribimp p q r = Imp (Imp p (Imp q r)) (Imp (Imp p q) (Imp p r))
	  axiom_doubleneg p = Imp (Imp (Imp p FF) FF) p
	  axiom_iffimp1 p q = Imp (Iff p q) (Imp p q)
	  axiom_iffimp2 p q = Imp (Iff p q) (Imp q p)
	  axiom_impiff p q = Imp (Imp p q) (Imp (Imp q p) (Iff p q))
	  axiom_true = Iff TT (Imp FF FF)
	  axiom_not p = Iff (Not p) (Imp p FF)
	  axiom_and p q = Iff (And p q) (Imp (Imp p (Imp q FF)) FF)
	  axiom_or p q = Iff (Or p q) (Not (And (Not p) (Not q)))
	  concl c = c


	class ProofsystemFOL thm term where
	  axiom_eqrefl :: term -> thm
	  axiom_existseq :: String -> term -> thm
	  axiom_funcong :: String -> [term] -> [term] -> thm
	  axiom_predcong :: String -> [term] -> [term] -> thm

	class ProofsystemQuant thm where
	  gen :: String -> thm -> thm
	  axiom_allimp :: String -> thm -> thm -> thm
	  axiom_impall :: String -> thm -> thm
	  axiom_exists :: String -> thm -> thm

	class ProofsystemPL thm where
	  modusponens :: thm -> thm -> thm
	  axiom_addimp :: thm -> thm -> thm
	  axiom_distribimp :: thm -> thm -> thm -> thm
	  axiom_doubleneg :: thm -> thm
	  axiom_iffimp1 :: thm -> thm -> thm
	  axiom_iffimp2 :: thm -> thm -> thm
	  axiom_impiff :: thm -> thm -> thm
	  axiom_true :: thm
	  axiom_not :: thm -> thm
	  axiom_and :: thm -> thm -> thm
	  axiom_or :: thm -> thm -> thm
	  concl :: thm -> thm
-}
