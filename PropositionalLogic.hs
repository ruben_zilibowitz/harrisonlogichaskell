module PropositionalLogic where

import Prelude hiding (negate,sum)
import qualified Data.Set as S
import Data.List (intercalate,minimumBy,maximumBy)
import Data.Maybe
import qualified Data.Map as M
import GHC.Unicode(isDigit)
import Types
import Failing

eg1 = Imp (And (Atom$P "p") (Atom$P "q")) (And (Atom$P "q") (Atom$P "r"))
eg2 = Imp (Imp TT (Iff (Atom$P "x") FF)) (Not (Or (Atom$P "y") (And FF (Atom$P "z"))))
eg3 = Or (Imp (Imp (Atom$P "x") (Atom$P "y")) TT) (Not FF)
eg4 = Iff (Iff (Atom$P "p") (Atom$P "q")) (Not (Imp (Atom$P "r") (Atom$P "s")))
eg5 = And (Or (Atom$P "p") (And (Atom$P "q") (Atom$P "r"))) (Or (Not (Atom$P "p")) (Not (Atom$P "r")))
eg6 = And (Or (Atom$P "p") (And (Atom$P "q") (Not (Atom$P "r")))) (Atom$P "s")

-- Section 2.8

defcnf3 fm = list_conj (S.map list_disj (mk_defcnf andcnf3 fm))

andcnf3 trip@(And p q,defs,n) = subcnf andcnf3 And (p,q) trip
andcnf3 trip = maincnf trip

-- NB: This code translated from Harrison does not type check.
-- The pos argument seems redundant. Not quite what is going on here.
-- A modified version without the pos argument type checks, and is given above.
--andcnf3 pos trip@(And p q,defs,n) = subcnf (andcnf3 pos) And (p,q) trip
--andcnf3 pos trip = maincnf pos trip

defcnf :: Formula Prop -> Formula Prop
defcnf fm = list_conj (S.map list_disj (defcnfs fm))

defcnfs fm = mk_defcnf andcnf fm

andcnf trip@(And p q,defs,n) = subcnf orcnf And (p,q) trip
andcnf trip = orcnf trip

orcnf trip@(Or p q,defs,n) = subcnf orcnf Or (p,q) trip
orcnf trip = maincnf trip

subcnf sfn op (p,q) (fm,defs,n) = (op fm1 fm2,defs2,n2)
 where
  (fm1,defs1,n1) = sfn (p,defs,n)
  (fm2,defs2,n2) = sfn (q,defs1,n1)

defcnf1 :: Formula Prop -> Formula Prop
defcnf1 fm = list_conj (S.map list_disj (mk_defcnf maincnf fm))

mk_defcnf fn fm = S.unions (simpcnf fm'' : map simpcnf deflist)
 where
  fm' = nenf fm
  n = 1 + (overatoms ((max_varindex "p_") . pname) fm' 0)
  (fm'',defs,_) = fn (fm',M.empty,n)
  deflist = map (snd . snd) (M.assocs defs)

max_varindex :: (Ord a, Read a) => String -> String -> a -> a
max_varindex pfx s n
  | l <= m || take m s /= pfx = n
  | all isDigit s' = max n (read s')
  | otherwise = n
 where
  m = length pfx
  l = length s
  s' = drop m s

maincnf trip@((And p q),_,_) = defstep And (p,q) trip
maincnf trip@((Or p q),_,_) = defstep Or (p,q) trip
maincnf trip@((Iff p q),_,_) = defstep Iff (p,q) trip
maincnf trip = trip

defstep op (p,q) (fm,defs,n)
   | M.member fm' defs2 = (fst (defs2 M.! fm'),defs2,n2)
   | otherwise          = let (v,n3) = mkprop n2 in (v,M.insert fm' (v,Iff v fm') defs2,n3)
 where
  (fm1,defs1,n1) = maincnf (p,defs,n)
  (fm2,defs2,n2) = maincnf (q,defs1,n1)
  fm' = op fm1 fm2

mkprop n = (Atom (P ("p_" ++ (show n))) , n + 1)

-- Section 2.7

-- a formula that is valid iff p is a prime
prime :: Integral a => a -> Formula Prop
prime p | p < 2 = FF
prime p = Not (And (multiplier m u v out (n - 1)) (congruent_to out p (max n (2 * n - 2))))
 where
  [x,y,out] = (map mk_index ["x", "y", "out"]) :: [Int -> Formula Prop]
  m i j = And (x i) (y j)
  [u,v] = (map mk_index2 ["u","v"]) :: [Int -> Int -> Formula Prop]
  n = bitlength p

congruent_to x m n =
  conjoin (\i -> if bit i m then x i else Not (x i)) [0..n-1]

bit 0 x = mod x 2 == 1
bit n x = bit (n - 1) (div x 2)

bitlength 0 = 0
bitlength x = 1 + (bitlength (div x 2))

multiplier x u v out 1 = And (Iff (out 0) (x 0 0)) (Not (out 1))
multiplier x u v out n = psimplify (And (Iff (out 0) (x 0 0)) (
         And (rippleshift
               (\i -> if i == n - 1 then FF else x 0 (i + 1))
               (x 1) (v 2) (out 1) (u 2) n)
             (if n == 2 then (And (Iff (out 2) (u 2 0)) (Iff (out 3) (u 2 1))) else
             (conjoin (\k -> rippleshift (u k) (x k) (v (k + 1)) (out k) (if k == n - 1 then \i -> out (n + i) else u (k + 1)) n) [2..n-1]))))

rippleshift u v c z w n =
  ripplecarry0 u v (\i -> if i == n then w (n - 1) else c (i + 1))
                   (\i -> if i == 0 then z else w (i - 1)) n

mk_index x i = Atom (P (x++"_"++(show i)))
mk_index2 x i j = Atom (P (x++"_"++(show i)++"_"++(show j)))

mk_adder_test n k = Imp (And (And (carryselect x y c0 c1 s0 s1 c s n k) (Not (c 0))) (ripplecarry0 x y c2 s2 n)) (And (Iff (c n) (c2 n)) (conjoin (\i -> Iff (s i) (s2 i)) [0 .. (n - 1)]))
 where
  [x,y,c,s,c0,s0,c1,s1,c2,s2] = map mk_index ["x","y","c","s","c0","s0","c1","s1","c2","s2"]

carryselect x y c0 c1 s0 s1 c s n k
  | k' < k = fm
  | otherwise = And fm (carryselect (offset k x) (offset k y) (offset k c0) (offset k c1) (offset k s0) (offset k s1) (offset k c) (offset k s) (n - k) k)
 where
  k' = min n k
  fm = And (And (ripplecarry0 x y c0 s0 k') (ripplecarry1 x y c1 s1 k')) (And (Iff (c k') (mux (c 0) (c0 k') (c1 k'))) (conjoin (\i -> Iff (s i) (mux (c 0) (s0 i) (s1 i))) [0 .. (k' - 1)]))

conjoin f l = list_conj (map f l)

offset n x i = x (n + i)

mux sel in0 in1 = Or (And (Not sel) in0) (And sel in1)

ripplecarry1 x y c out n = psimplify (ripplecarry x y (\i -> if i == 0 then TT else c i) out n)
ripplecarry0 x y c out n = psimplify (ripplecarry x y (\i -> if i == 0 then FF else c i) out n)
ripplecarry x y c out n = conjoin (\i -> fa (x i) (y i) (c i) (out i) (c (i+1))) [0..n-1]

carry x y z = Or (And x y) (And (Or x y) z)
sum x y z = halfsum (halfsum x y) z
fa x y z s c = And (Iff s (sum x y z)) (Iff c (carry x y z))

ha x y s c = And (Iff s (halfsum x y)) (Iff c (halfcarry x y))
halfsum x y = Iff x (Not y)
halfcarry = And

-- k element sublists of given list
choose :: [b] -> Int -> [[b]]
_      `choose` 0 = [[]]
[]     `choose` _ =  []
(x:xs) `choose` k =  (x:) `fmap` (xs `choose` (k-1)) ++ xs `choose` k

ramsey :: (Enum a, Num a, Show a) => Int -> Int -> a -> Formula Prop
ramsey s t n = Or (list_disj (map (list_conj . (map e)) yesgrps)) (list_disj (map (list_conj . (map (Not . e))) nogrps))
 where
  vertices = [1..n]
  yesgrps = map (flip choose 2) (vertices `choose` s)
  nogrps = map (flip choose 2) (vertices `choose` t)
  e [m,n] = Atom (P ("p_"++(show m)++"_"++(show n)))

-- Section 2.6

cnf :: Ord a => Formula a -> Formula a
cnf fm = list_conj (S.map list_disj (simpcnf fm))

simpcnf :: Ord t => Formula t -> S.Set (S.Set (Formula t))
simpcnf FF = S.singleton S.empty
simpcnf TT = S.empty
simpcnf fm = let cjs = S.filter (not . trivial) (purecnf fm) in
             S.filter (\c -> S.null (S.filter (flip S.isProperSubsetOf c) cjs)) cjs

purecnf :: Ord t => Formula t -> S.Set (S.Set (Formula t))
purecnf fm = S.map (S.map negate) (purednf (nnf (Not fm)))

dnf :: Ord a => Formula a -> Formula a
dnf fm = list_disj (S.map list_conj (simpdnf fm))

simpdnf :: Ord t => Formula t -> S.Set (S.Set (Formula t))
simpdnf FF = S.empty
simpdnf TT = S.singleton S.empty
simpdnf fm = let djs = S.filter (not . trivial) (purednf (nnf fm)) in
             S.filter (\d -> S.null (S.filter (flip S.isProperSubsetOf d) djs)) djs

trivial :: Ord t => S.Set (Formula t) -> Bool
trivial lits = not (S.null (S.intersection pos (S.map negate neg)))
 where
  (pos,neg) = S.partition positive lits

purednf :: Ord t => Formula t -> S.Set (S.Set (Formula t))
purednf (And p q) = distrib (purednf p) (purednf q)
purednf (Or p q) = S.union (purednf p) (purednf q)
purednf fm = S.singleton (S.singleton fm)

allpairs_union s1 s2 = S.fromList [S.union x y | x <- S.toList s1, y <- S.toList s2]

distrib :: Ord a => S.Set (S.Set a) -> S.Set (S.Set a) -> S.Set (S.Set a)
distrib s1 s2 = allpairs_union s1 s2

rawdnf :: Formula a -> Formula a
rawdnf (And p q) = distrib1 (And (rawdnf p) (rawdnf q))
rawdnf (Or p q) = Or (rawdnf p) (rawdnf q)
rawdnf fm = fm

distrib1 :: Formula a -> Formula a
distrib1 (And p (Or q r)) = Or (distrib1 (And p q)) (distrib1 (And p r))
distrib1 (And (Or p q) r) = Or (distrib1 (And p r)) (distrib1 (And q r))
distrib1 fm = fm

dnf1 :: Ord a => Formula a -> Formula a
dnf1 fm = list_disj (map (mk_lits (map Atom pvs)) satvals)
 where
  pvs = S.toList (atoms fm)
  satvals = allsatvaluations (eval fm) (const False) pvs

allsatvaluations :: Eq t => ((t -> Bool) -> Bool) -> (t -> Bool) -> [t] -> [t -> Bool]
allsatvaluations subfn v [] | subfn v = [v]
                            | otherwise = []
allsatvaluations subfn v (p:ps) = let v' t q = if q == p then t else v q in
                                  allsatvaluations subfn (v' False) ps ++
                                  allsatvaluations subfn (v' True) ps

mk_lits :: [Formula a] -> (a -> Bool) -> Formula a
mk_lits pvs v = list_conj (map (\p -> if eval p v then p else Not p) pvs)

list_conj :: Foldable t => t (Formula a) -> Formula a
list_conj fs
  | null fs = TT
  | otherwise = foldr1 And fs

list_disj :: Foldable t => t (Formula a) -> Formula a
list_disj fs
  | null fs = FF
  | otherwise = foldr1 Or fs

-- Section 2.5

nenf :: Formula a -> Formula a
nenf fm = nenf' (psimplify fm)
 where
  nenf' (Not (Not p)) = nenf p
  nenf' (Not (And p q)) = Or (nenf (Not p)) (nenf (Not q))
  nenf' (Not (Or p q)) = And (nenf (Not p)) (nenf (Not q))
  nenf' (Not (Imp p q)) = And (nenf p) (nenf (Not q))
  nenf' (Not (Iff p q)) = Iff (nenf p) (nenf (Not q))
  nenf' (And p q) = And (nenf p) (nenf q)
  nenf' (Or p q) = Or (nenf p) (nenf q)
  nenf' (Imp p q) = Or (nenf (Not p)) (nenf q)
  nenf' (Iff p q) = Iff (nenf p) (nenf q)
  nenf' fm = fm

-- Convert to negation normal form
nnf :: Formula a -> Formula a
nnf fm = nnf' (psimplify fm)
 where
  nnf' (And p q) = And (nnf p) (nnf q)
  nnf' (Or p q) = Or (nnf p) (nnf q)
  nnf' (Imp p q) = Or (nnf (Not p)) (nnf q)
  nnf' (Iff p q) = Or (And (nnf p) (nnf q)) (And (nnf (Not p)) (nnf (Not q)))
  nnf' (Not (Not p)) = nnf p
  nnf' (Not (And p q)) = Or (nnf (Not p)) (nnf (Not q))
  nnf' (Not (Or p q)) = And (nnf (Not p)) (nnf (Not q))
  nnf' (Not (Imp p q)) = And (nnf p) (nnf (Not q))
  nnf' (Not (Iff p q)) = Or (And (nnf p) (nnf (Not q))) (And (nnf (Not p)) (nnf q))
  nnf' fm = fm

negate :: Formula t -> Formula t
negate (Not p) = p
negate p = Not p

negative :: Formula t -> Bool
negative (Not p) = True
negative _ = False

positive :: Formula t -> Bool
positive = not . negative

psimplify :: Formula a -> Formula a
psimplify (Not p) = psimplify1 (Not (psimplify p))
psimplify (And p q) = psimplify1 (And (psimplify p) (psimplify q))
psimplify (Or p q) = psimplify1 (Or (psimplify p) (psimplify q))
psimplify (Imp p q) = psimplify1 (Imp (psimplify p) (psimplify q))
psimplify (Iff p q) = psimplify1 (Iff (psimplify p) (psimplify q))
psimplify fm = fm

psimplify1 :: Formula a -> Formula a
psimplify1 (Not FF) = TT
psimplify1 (Not TT) = FF
psimplify1 (Not (Not p)) = p
psimplify1 (And p FF) = FF
psimplify1 (And FF p) = FF
psimplify1 (And p TT) = p
psimplify1 (And TT p) = p
psimplify1 (Or p FF) = p
psimplify1 (Or FF p) = p
psimplify1 (Or p TT) = TT
psimplify1 (Or TT p) = TT
psimplify1 (Imp FF p) = TT
psimplify1 (Imp p TT) = TT
psimplify1 (Imp TT p) = p
psimplify1 (Imp p FF) = Not p
psimplify1 (Iff p TT) = p
psimplify1 (Iff TT p) = p
psimplify1 (Iff p FF) = Not p
psimplify1 (Iff FF p) = Not p
psimplify1 fm = fm

-- Section 2.4

dual :: Formula a -> Formula a
dual FF = TT
dual TT = FF
dual (Not p) = Not (dual p)
dual (And p q) = And (dual p) (dual q)
dual (Or p q) = Or (dual p) (dual q)
dual fm@(Atom _) = fm
dual _ = error "Formula involves connectives ==> or <==>"

psubst subfn = onatoms (\p -> maybe (Atom p) id (subfn p))

-- Section 2.3

unsatisfiable fm = tautology (Not fm)
satifsiable fm = not (unsatisfiable fm)
tautology fm = and (onallvaluations (eval fm) (const False) (S.toList (atoms fm)))

-- Section 2.2

print_truthtable :: Formula Prop -> IO ()
print_truthtable fm = putStrLn (unlines (header : separator : onallvaluations mk_row (const False) (S.toList ats)) ++ separator)
 where
  ats = atoms fm
  width = (S.fold max 5 (S.map (length . pname) ats)) + 1
  fixw s = s ++ (replicate (width - (length s)) ' ')
  truthstring p = fixw (if p then "true" else "false")
  mk_row v = let lis = map (truthstring . v) (S.toList ats) in
             let ans = truthstring (eval fm v) in
             ((intercalate " " lis) ++ " | " ++ ans)
  separator = replicate (width * (S.size ats) + 9) '-'
  header = (intercalate " " (S.toList (S.map (fixw . pname) ats))) ++ " | formula"

onallvaluations :: Eq t => ((t -> Bool) -> t1) -> (t -> Bool) -> [t] -> [t1]
onallvaluations subfn v [] = [subfn v]
onallvaluations subfn v (p:ps) = let v' t q = if q == p then t else v q in
                                 onallvaluations subfn (v' False) ps ++
                                 onallvaluations subfn (v' True) ps

eval :: Formula t -> (t -> Bool) -> Bool
eval FF _ = False
eval TT _ = True
eval (Atom x) v = v x
eval (Not p) v = not (eval p v)
eval (And p q) v = (eval p v) && (eval q v)
eval (Or p q) v = (eval p v) || (eval q v)
eval (Imp p q) v = (not (eval p v)) || (eval q v)
eval (Iff p q) v = (eval p v) == (eval q v)

-- Section 2.1

antecedent (Imp p q) = return p
antecedent _ = failure "antecedent"

consequent fm = dest_imp fm >>= return . snd

dest_or (Or p q) = return (p,q)
dest_or _ = failure "dest_or"

dest_and (And p q) = return (p,q)
dest_and _ = failure "dest_and"

dest_iff (Iff p q) = return (p,q)
dest_iff _ = failure "dest_iff"

dest_imp :: Formula t -> Failing (Formula t, Formula t)
dest_imp (Imp p q) = return (p,q)
dest_imp fm = failure "dest_imp"

atoms :: Ord b => Formula b -> S.Set b
atoms fm = atom_union id fm

atom_union :: (Ord a, Ord b) => (a -> b) -> Formula a -> S.Set b
atom_union f fm = S.map f (S.fromList (overatoms (:) fm []))

onatoms :: (t -> Formula a) -> Formula t -> Formula a
onatoms _ TT = TT
onatoms _ FF = FF
onatoms f (Atom a) = f a
onatoms f (Not p) = Not (onatoms f p)
onatoms f (And p q) = And (onatoms f p) (onatoms f q)
onatoms f (Or p q) = Or (onatoms f p) (onatoms f q)
onatoms f (Imp p q) = Imp (onatoms f p) (onatoms f q)
onatoms f (Iff p q) = Iff (onatoms f p) (onatoms f q)
onatoms f (Forall x p) = Forall x (onatoms f p)
onatoms f (Exists x p) = Exists x (onatoms f p)

overatoms :: (t -> a -> a) -> Formula t -> a -> a
overatoms _ TT b = b
overatoms _ FF b = b
overatoms f (Atom a) b = f a b
overatoms f (Not p) b = overatoms f p b
overatoms f (And p q) b = overatoms f p (overatoms f q b)
overatoms f (Or p q) b = overatoms f p (overatoms f q b)
overatoms f (Imp p q) b = overatoms f p (overatoms f q b)
overatoms f (Iff p q) b = overatoms f p (overatoms f q b)
overatoms f (Forall x p) b = overatoms f p b
overatoms f (Exists x p) b = overatoms f p b
