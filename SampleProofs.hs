{-# LANGUAGE ImplicitParams #-}
module SampleProofs where

import Data.List (groupBy)
import Debug.Trace
import qualified Data.Map as M
import qualified Data.Set as S

import PropositionalLogic hiding (nnf)
import FirstOrderLogic hiding (replace)
import Types
import Failing
import Parser
import Equality (mk_eq, lhs, rhs, dest_eq, termsize, replacet, equalitize)
import Proofsystem
import LCFProp
import LCFFOL
import Tactics

import GHC.Stack

data ThmOrHyp = Thm (Failing Thm) | Hyp String

isThm (Thm _) = True
isThm _ = False

bothThmHyp x y = (isThm x) == (isThm y)

toThm (Thm thm) = thm
toHyp (Hyp hyp) = hyp

-- a handy way of combining external theorems with named hypotheses
by_using :: (?loc :: CallStack) => [ThmOrHyp] -> t -> Goals -> Failing [Thm]
by_using fths_hyps p g = sequence
  [if (isThm (head xs)) then
      using (map toThm xs) p g else
         by (map toHyp xs) p g | xs <- groupBy bothThmHyp fths_hyps] >>= return . concat

-- adds a hypothesis that is useful for equational reasoning
{-
   x = y
   x = z
 --------
   y = z
-}
note_eq_step :: String -> Tactic
note_eq_step name = note (name, fol "forall x y z. x = y & x = z ==> y = z")
  using [eq_trans x y z, eq_sym x y]
 where [x,y,z] = map Var ["x","y","z"]

--------------------
-- Some group theory
--------------------

-- Takes a theorem that is a consequence of the group axioms
-- produces a tactic that results in adding a note for the
-- consequent of the implication. I.e. the formula without
-- the group axioms. This assumes the group axioms are
-- already in the proof environment.
-- Useful for bringing in other theorems.
note_group_thm :: (?loc :: CallStack) => String -> Failing Thm -> Tactic
note_group_thm name thm goals = do
  fm <- thm >>= return . concl >>= consequent
  note (name, fm)
   proof
   [have (groupize fm) using [thm],
    so conclude fm by ["left_identity","left_inverses","associativity"],
    qed
    ] goals

-- adds a hypothesis for the associativity axiom with the brackets going the other way.
note_sym_associativity :: String -> Tactic
note_sym_associativity name = note (name, fol "forall a b c. a * (b * c) = (a * b) * c")
    by_using [Hyp "associativity", Thm (eq_sym (Var "a") (Var "b"))]

-- group axioms
associativity = fol "forall a b c. (a * b) * c = a * (b * c)"
left_identity = fol "forall a. 1 * a = a"
left_inverses = fol "forall a. i(a) * a = 1"

-- group axioms in a list
group_axioms = [left_identity,left_inverses,associativity]

-- adds the group axioms as a hypothesis to
-- a given formula
groupize fm = Imp (list_conj group_axioms) fm

-- some true statements about groups
identity_unique = fol "forall e. (forall a. e * a = a) ==> e = 1"
inverses_unique = fol "forall a b. (a * b = 1 ==> a = i(b))"
right_identity = fol "forall a. a * 1 = a"
right_inverse = fol "forall a. a * i(a) = 1"

left_cancellation = fol "forall a b c. a * b = a * c ==> b = c"
right_cancellation = fol "forall a b c. b * a = c * a ==> b = c"

-- Debugging tactic. Prints the current proof state to console.
trace_tac :: Tactic
trace_tac gl = traceShow gl (return gl)

{-
square_trans:
  x  =  y
  =     =
  u  =  v
-}
square_trans :: Failing Thm
square_trans = prove (fol "forall x y u v. x = y & x = u & y = v ==> u = v")
 [
  note ("A", fol "forall a b c. a = b & a = c ==> b = c") using [eq_trans (Var "a") (Var "b") (Var "c"), eq_sym (Var "a") (Var "b")],
  fix "x", fix "y", fix "u", fix "v",
  assume [("xy", fol "x = y"),
          ("xu", fol "x = u"),
          ("yv", fol "y = v")],
  so have (fol "y = u") by ["xy","xu","A"],
  so conclude (fol "u = v") by ["yv","A"],
  qed
 ]

drop_identity_left :: Failing Thm
drop_identity_left = prove (groupize (fol "forall b c. 1 * b = 1 * c ==> b = c"))
 [
  note ("square_trans", fol "forall x y u v. x = y & x = u & y = v ==> u = v")
    using [square_trans],
  assume [("left_identity", left_identity),
          ("left_inverses", left_inverses),
          ("associativity", associativity)],
  fix "b", fix "c",
  assume [("ant", fol "1 * b = 1 * c")],
  so conclude (fol "b = c") by ["left_identity","square_trans"],
  qed
 ]

left_cancellation_thm :: Failing Thm
left_cancellation_thm = prove (groupize left_cancellation)
 [
  assume [("left_identity", left_identity),
          ("left_inverses", left_inverses),
          ("associativity", associativity)],
  
  note_group_thm "drop_identity_left" drop_identity_left,
  note_sym_associativity "sym_associativity",
  
  fix "a", fix "b", fix "c",
  assume [("ant", fol "a * b = a * c")],
  so have (fol "i(a) * (a * b) = i(a) * (a * c)") using [icongruence x y (folTerm "c * x") (folTerm "c * y")],
  so have (fol "(i(a) * a) * b = (i(a) * a) * c") by_using [Hyp "sym_associativity", Thm square_trans],
  so have (fol "1 * b = 1 * c") by_using [Hyp "left_inverses",
    Thm (icongruence (folTerm "(ia * a)") (folTerm "1") (folTerm "(ia * a) * c") (folTerm "1 * c")), Thm square_trans],
  so conclude (fol "b = c") by ["drop_identity_left"],
  qed]
 where [x,y] = map Var ["x","y"]

{-
1 = 1                        (eq_refl)
1 * 1 = 1                    (left identity)
(i(a) * a) * 1 = i(a) * a    (inverses)
i(a) * (a * 1) = i(a) * a    (associativity)
a * 1 = a                    (left cancellation)
-}
right_identity_thm :: Failing Thm
right_identity_thm = prove (groupize right_identity)
 [
  assume [("left_identity", left_identity),
          ("left_inverses", left_inverses),
          ("associativity", associativity)],
  
  fix "a",
  
  note_group_thm "left_cancellation" left_cancellation_thm,
  
  have (fol "1 = 1") using [return (axiom_eqrefl x)],
  so have (fol "1 * 1 = 1") by_using [Hyp "left_identity", Thm (eq_trans x y z)],
  so have (fol "(i(a) * a) * 1 = i(a) * a") by_using [Hyp "left_inverses", Thm (eq_sym x y),
     Thm (icongruence x y (folTerm "x * c") (folTerm "y * c")), Thm square_trans],
  so have (fol "i(a) * (a * 1) = i(a) * a") by_using [Hyp "associativity",
     Thm (eq_sym x y),  Thm (eq_trans x y z)],
  so conclude (fol "a * 1 = a") by ["left_cancellation"],
  
  qed]
 where [x,y,z] = map Var ["x","y","z"]

{-
right_inverse
i(a) = i(a)                      (eq_refl)
1 * i(a) = i(a) * 1              (left_identity and right_identity)
(i(a) * a) * i(a) = i(a) * 1     (left_inverse on lhs)
i(a) * (a * i(a)) = i(a) * 1     (associativity on lhs)
a * i(a) = 1                     (left_cancellation)
-}
right_inverse_thm :: (?loc :: CallStack) => Failing Thm
right_inverse_thm = prove (groupize (fol "forall a. a * i(a) = 1"))
 [
  note_eq_step "eq_step",
  
  assume [("left_identity", left_identity),
          ("left_inverses", left_inverses),
          ("associativity", associativity)],
  
  fix "a",
  
  note_group_thm "left_cancellation" left_cancellation_thm,
  note_group_thm "right_identity" right_identity_thm,
  
  have (fol "i(a) = i(a)") using [return (axiom_eqrefl x)],
  so have (fol "1 * i(a) = i(a)") by_using [Hyp "left_identity",Thm (eq_sym x y),Hyp "eq_step"],
  so have (fol "1 * i(a) = i(a) * 1") by_using [Hyp "right_identity",Thm (eq_sym x y),Hyp "eq_step"],
  so have (fol "(i(a) * a) * i(a) = i(a) * 1") by_using [
    Hyp "left_inverses",
    {- |- ((i[a] * a) = <1> ⟹  ((i[a] * a) * i[a]) = (<1> * i[a])) -}
    Thm (icongruence (folTerm "i(a) * a") (folTerm "1") (folTerm "(i(a) * a) * i(a)") (folTerm "1 * i(a)")),
    Thm (eq_trans x y z)
   ],
  so have (fol "i(a) * (a * i(a)) = i(a) * 1") by_using [Thm (eq_sym x y), Hyp "associativity", Thm (eq_trans x y z)],
  so conclude (fol "a * i(a) = 1") by ["left_cancellation"],
  
  qed]
 where [x,y,z] = map Var ["x","y","z"]

{-
right cancellation
b * a = c * a                         (hyp)
(b * a) * i(a) = (c * a) * i(a)       (mult right by i(a))
b * (a * i(a)) = c * (a * i(a))       (associativity)
b * 1 = c * 1                         (right inverse)
b = c                                 (right identity)
-}
right_cancellation_thm :: Failing Thm
right_cancellation_thm = prove (groupize right_cancellation)
 [
  assume [("left_identity", left_identity),
          ("left_inverses", left_inverses),
          ("associativity", associativity)],
  
  fix "a", fix "b", fix "c",
  
  note_group_thm "right_identity" right_identity_thm,
  note_group_thm "right_inverse" right_inverse_thm,
  
  assume [("ant", fol  "b * a = c * a")],
  so have (fol         "(b * a) * i(a) = (c * a) * i(a)") using [icongruence (folTerm "b * a") (folTerm "c * a") (folTerm "(b * a) * i(a)") (folTerm "(c * a) * i(a)")],
  so have (fol         "b * (a * i(a)) = c * (a * i(a))") by_using [Hyp "associativity", Thm square_trans],
  so have (fol         "b * 1 = c * 1")                   by_using [Hyp "right_inverse", Thm (icongruence (folTerm "a * i(a)") (folTerm "1") (folTerm "b * (a * i(a))") (folTerm "b * 1") >>= return . gen "b" >>= return . gen "a"), Thm square_trans],
  so conclude (fol     "b = c")                           by_using [Hyp "right_identity", Thm square_trans],
  
  qed]
 where [x,y,z] = map Var ["x","y","z"]

{-
identity_unique
∀a:(e * a) = a    (ant)
e * 1 = 1         (specification)
e = 1             (right identity)
-}
identity_unique_thm = prove (groupize identity_unique)
 [
  note_eq_step "eq_step",
  
  assume [("left_identity", left_identity),
          ("left_inverses", left_inverses),
          ("associativity", associativity)],
  
  fix "e",
  
  note_group_thm "right_identity" right_identity_thm,
  
  assume [("ant", fol "forall a. e * a = a")],
  so have (fol "e * 1 = 1") using [ispec (folTerm "1") (fol "forall a. e * a = a")],
  so conclude (fol "e = 1") by ["right_identity", "eq_step"],
  
  qed]
 where [x,y,z] = map Var ["x","y","z"]

{-
inverses_unique
a * b = 1                   (ant)
(a * b) * i(b) = 1 * i(b)   (mult right by i(b))
a * (b * i(b)) = 1 * i(b)   (associativity)
a * 1 = 1 * i(b)            (right inverse)
a * 1 = i(b)                (left identity)
a = i(b)                    (right identity)
-}
inverses_unique_thm :: Failing Thm
inverses_unique_thm = prove (groupize inverses_unique)
 [
  note_eq_step "eq_step",
  
  assume [("left_identity", left_identity),
          ("left_inverses", left_inverses),
          ("associativity", associativity)],
  
  fix "a", fix "b",
  
  note_group_thm "right_identity" right_identity_thm,
  note_group_thm "right_inverse" right_inverse_thm,
  
  assume [("ant", fol "a * b = 1")],
  so have (fol "(a * b) * i(b) = 1 * i(b)") using [icongruence (folTerm "a * b") (folTerm "1") (folTerm "(a * b) * i(b)") (folTerm "1 * i(b)")],
  so have (fol "a * (b * i(b)) = 1 * i(b)") by ["associativity","eq_step"],
  so have (fol "a * 1 = 1 * i(b)") by_using [Hyp "right_inverse",
    Thm (icongruence (folTerm "b * i(b)") (folTerm "1") (folTerm "a * (b * i(b))") (folTerm "a * 1")),
    Hyp "eq_step"],
  so conclude (fol "a = i(b)") by_using [Hyp "right_identity", Hyp "left_identity", Thm square_trans],
  
  qed]
 where [x,y,z] = map Var ["x","y","z"]

-----------
-- Peano arithmetic
-- Using axiomatization based on ordered semirings
-- See https://en.wikipedia.org/wiki/Peano_axioms#Equivalent_axiomatizations

addition_associative = fol "forall x y z. (x + y) + z = x + (y + z)"
addition_commutative = fol "forall x y. x + y = y + x"
multiplication_associative = fol "forall x y z. (x * y) * z = x * (y * z)"
multiplication_commutative = fol "forall x y. x * y = y * x"
distributive_law = fol "forall x y z. x * (y + z) = (x * y) + (x * z)"
additive_identity = fol "forall x. x + 0 = x & x * 0 = 0"
multiplicative_identity = fol "forall x. x * 1 = x"
order_transitive = fol "forall x y z. x < y & y < z ==> x < z"
order_irreflexive = fol "forall x. ~(x < x)"
order_equality = fol "forall x y. x < y | x = y | y < x"
order_add_right = fol "forall x y z. x < y ==> x + z < y + z"
order_mult_right = fol "forall x y z. 0 < z & x < y ==> x * z < y * z"
order_addition = fol "forall x y. x < y ==> exists z. x + z = y"
order_one = fol "0 < 1 & forall x. 0 < x ==> (1 < x | 1 = x)"
order_zero = fol "forall x. (0 < x | 0 = x)"

axioms_PA_minus = [
   ("addition_associative",addition_associative),
   ("addition_commutative",addition_commutative),
   ("multiplication_associative",multiplication_associative),
   ("multiplication_commutative",multiplication_commutative),
   ("distributive_law",distributive_law),
   ("additive_identity",additive_identity),
   ("multiplicative_identity",multiplicative_identity),
   ("order_transitive",order_transitive),
   ("order_irreflexive",order_irreflexive),
   ("order_equality",order_equality),
   ("order_add_right",order_add_right),
   ("order_mult_right",order_mult_right),
   ("order_addition",order_addition),
   ("order_one",order_one),
   ("order_zero",order_zero)]

pa_minus :: Formula FOL
pa_minus = list_conj (map snd axioms_PA_minus)

-- PA induction schema
-- First argument is name of variable to perform induction on
-- Second argument is predicate containing the variable you are
-- performing induction on.
induction :: String -> Formula FOL -> Formula FOL
induction x phi
  | S.member x (fv phi) = generalize (onatoms (subs M.!) (fol "(phi0 & forall x. phix ==> phiSx) ==> forall x. phix"))
  | otherwise = error "induction: predicate does not contain induction variable"
 where
  phi0 = subst (M.singleton x (folTerm "0")) phi
  phix = subst (M.singleton x (folTerm "x")) phi
  phiSx = subst (M.singleton x (folTerm "x + 1")) phi
  subs = M.fromList [(R "phi0" [],phi0),(R "phix" [],phix),(R "phiSx" [],phiSx)]

pa :: [(String, (String, Formula FOL))] -> Formula FOL
pa preds = list_conj ((map snd axioms_PA_minus) ++ (map ((uncurry induction) . snd) preds))

-- (∃n:A(n) ⟹  ∃m:∀k:(k < m ⟹  ¬A(k)))
well_ordering x phi
  | S.member x (fv phi) = generalize (onatoms subs (fol "(exists n. phin) ==> exists m. forall k. k < m ==> ~(phik)"))
  | otherwise = error "induction: predicate does not contain induction variable"
 where
  phin = subst (M.singleton x (folTerm "n")) phi
  phik = subst (M.singleton x (folTerm "k")) phi
  subs r = maybe (Atom r) id (M.lookup r (M.fromList [(R "phin" [],phin),(R "phik" [],phik)]))

zero_is_not_one :: Failing Thm
zero_is_not_one = prove (Imp pa_minus (fol "~(0 = 1)"))
 [
  assume axioms_PA_minus,
  note ("A", fol "~(1 < 1)") by ["order_irreflexive"],
  have (fol "0 = 1 ==> 1 < 1") by_using [Hyp "order_one", Thm (isubst (folTerm "0") (folTerm "1") (fol "0 < 1") (fol "1 < 1"))],
  so conclude (fol "~(0 = 1)") by ["A"],
  qed]

rational x = subst (M.singleton "x" x) (fol "exists p q. 0 < q & p = x * q")
irrational x = negatef (rational x)

root2_is_irrational = Imp (fol "<root2> * <root2> = 2") (irrational (folTerm "<root2>"))
half_is_rational = Imp (fol "<half> + <half> = 1") (rational (folTerm "<half>"))

no_fractions = fol "forall a b. ~(a < b & b < a + 1)"

blah = prove (Imp pa_minus (well_ordering "x" (fol "A(x)")))
 [
  assume axioms_PA_minus,
  exists_intro_tac (folTerm "n"),
  imp_intro_tac "A(n)",
  exists_intro_tac (folTerm "m"),
  fix "k",
  imp_intro_tac "k<m",
  trace_tac,
  qed]

half_is_rational_thm :: Failing Thm
half_is_rational_thm = prove (Imp pa_minus half_is_rational)
 [
  assume axioms_PA_minus,
  imp_intro_tac "ant",
  exists_intro_tac (folTerm "1"),
  exists_intro_tac (folTerm "1 + 1"),
  conj_intro_tac,
  
  note ("A", fol "1 < 1 + 1")
  proof
  [have (fol "0 < 1") by ["order_one"],
   so have (fol "0 + 1 < 1 + 1") by ["order_add_right"],
   so have (fol "1 + 0 < 1 + 1") by_using [Hyp "addition_commutative", Thm (isubst (folTerm "0 + 1") (folTerm "1 + 0") (fol "0 + 1 < 1 + 1") (fol "1 + 0 < 1 + 1"))],
   so conclude (fol "1 < 1 + 1") by_using [Hyp "additive_identity", Thm (isubst (folTerm "1 + 0") (folTerm "1") (fol "1 + 0 < 1 + 1") (fol "1 < 1 + 1"))],
   qed],
  
  -- subgoal "0 < 1 + 1"
  have (fol "0 < 1") by ["order_one"],
  so conclude (fol "0 < 1 + 1") by ["A","order_transitive"],
  qed,
  
  -- subgoal "1 = 'half' * (1 + 1)"
  note ("B", fol "<half> * (1 + 1) = <half> * 1 + <half> * 1") by ["distributive_law"],
  have (fol "<half> * 1 + <half> * 1 = <half> + <half>") by_using [Hyp "multiplicative_identity", Thm (icongruence (folTerm "<half> * 1") (folTerm "<half>") (folTerm "<half> * 1 + <half> * 1") (folTerm "<half> + <half>"))],
  so have (fol "<half> * (1 + 1) = <half> + <half>") by_using [Thm (eq_trans x y z), Hyp "B"],
  so have (fol "<half> * (1 + 1) = 1") by_using [Thm (eq_trans x y z), Hyp "ant"],
  so conclude (fol "1 = <half> * (1 + 1)") using [eq_sym x y],
  
  qed]
 where [x,y,z] = map Var ["x","y","z"]

no_fractions_thm = prove (Imp pa_minus no_fractions)
 [
  assume axioms_PA_minus,
  fix "a", fix "b",
  
  --def: ('root2' * 'root2') = '2'
  --ant: ∃p:∃q:('0' < q ⋀ p = ('root2' * q))
  
  
  
  trace_tac,
  
  qed]

-- From Section 7.7 of Harrison

-- Godel's First Incompleteness Theorem
-- Assuming the fixed point property of G
-- and some other convenient properties.
godel_1 = lcffol $ fol
   "(True(G) <==> ~(|--(G))) & Pi(G) &                 \
 \  (forall p. Sigma(p) ==> (|--(p) <==> True(p))) &   \
 \  (forall p. True(Not(p)) <==> ~True(p)) &           \
 \  (forall p. Pi(p) ==> Sigma(Not(p)))                \
 \  ==> (|--(Not(G)) <==> |--(G))"

-- Godel's Second Incompleteness Theorem
godel_2 = prove
 (fol ("(forall p. |--(p) ==> |--(Pr(p))) &                                \
 \  (forall p q. |--(imp(Pr(imp(p,q)),imp(Pr(p),Pr(q))))) &                \
 \  (forall p. |--(imp(Pr(p),Pr(Pr(p)))))                                  \
 \  ==> (forall p q. |--(imp(p,q)) & |--(p) ==> |--(q)) &                  \
 \      (forall p q. |--(imp(q,imp(p,q)))) &                               \
 \      (forall p q r. |--(imp(imp(p,imp(q,r)),imp(imp(p,q),imp(p,r)))))   \
 \      ==> |--(imp(G,imp(Pr(G),F))) & |--(imp(imp(Pr(G),F),G))            \
 \          ==> |--(imp(Pr(F),F)) ==> |--(F)"))
 [assume[("lob1", fol "forall p. |--(p) ==> |--(Pr(p))"),
         ("lob2", fol "forall p q. |--(imp(Pr(imp(p,q)),imp(Pr(p),Pr(q))))"),
         ("lob3", fol "forall p. |--(imp(Pr(p),Pr(Pr(p))))")],
  assume[("logic",fol "(forall p q. |--(imp(p,q)) & |--(p) ==> |--(q)) &    \
                 \  (forall p q. |--(imp(q,imp(p,q)))) &                    \
                 \  (forall p q r. |--(imp(imp(p,imp(q,r)),                 \
                 \                     imp(imp(p,q),imp(p,r)))))")],
  assume [("fix1", fol "|--(imp(G,imp(Pr(G),F)))"),
          ("fix2", fol "|--(imp(imp(Pr(G),F),G))")],
  assume[("consistency", fol "|--(imp(Pr(F),F))")],
  have (fol "|--(Pr(imp(G,imp(Pr(G),F))))") by ["lob1", "fix1"],
  so have (fol "|--(imp(Pr(G),Pr(imp(Pr(G),F))))") by ["lob2", "logic"],
  so have (fol "|--(imp(Pr(G),imp(Pr(Pr(G)),Pr(F))))") by ["lob2", "logic"],
  so have (fol "|--(imp(Pr(G),Pr(F)))") by ["lob3", "logic"],
  so note("L", (fol "|--(imp(Pr(G),F))")) by ["consistency", "logic"],
  so have (fol "|--(G)") by ["fix2", "logic"],
  so have (fol "|--(Pr(G))") by ["lob1", "logic"],
  so conclude (fol "|--(F)") by ["L", "logic"],
  qed]
