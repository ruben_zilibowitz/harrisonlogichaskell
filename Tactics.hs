{-# LANGUAGE ImplicitParams #-}
module Tactics where

import Prelude hiding (negate,sum,pred,take)
import qualified Data.Set as S
import Data.List (intercalate,minimumBy,maximumBy,find,partition,delete,findIndex,sortBy,groupBy)
import Data.Maybe
import qualified Data.Map as M

import PropositionalLogic hiding (nnf)
import FirstOrderLogic hiding (replace)
import Types
import Failing
import Parser
import Equality (mk_eq, lhs, rhs, dest_eq, termsize, replacet, equalitize)
import Proofsystem
import LCFProp
import LCFFOL

import GHC.Stack

data Goals = Goals [([(String,Formula FOL)],Formula FOL)] ([Thm] -> Failing Thm)

type Tactic = Goals -> Failing Goals

print_goal (Goals gls jfn) =
  let print_hyp (l,fm) = l ++ ": " ++ (show fm) in
    case gls of
      (asl,w):ogls ->
         (if null ogls then "1 subgoal:" else
          ((show (length gls)) ++ " subgoals starting with")) :
         (map print_hyp (reverse asl))
         ++ ["---> " ++ show w]
      [] -> ["No subgoals"]

instance Show Goals where
   show goals = unlines (print_goal goals)

{- ------------------------------------------------------------------------- -}
{- Setting up goals and terminating them in a theorem.                       -}
{- ------------------------------------------------------------------------- -}

set_goal :: Formula FOL -> Goals
set_goal p =
  let chk th = if concl th == p then return th else failure "wrong theorem" in
  Goals [([],p)] (\[th] -> truth >>= modusponens th >>= chk)

extract_thm :: Goals -> Failing Thm
extract_thm gls =
  case gls of
    Goals [] jfn -> jfn []
    _ -> failure "extract_thm: unsolved goals"

tac_proof :: Goals -> [Tactic] -> Failing Thm
tac_proof g prf = run prf g >>= extract_thm

prove :: Formula FOL -> [Tactic] -> Failing Thm
prove p prf = tac_proof (set_goal p) prf

{- ------------------------------------------------------------------------- -}
{- Conjunction introduction tactic.                                          -}
{- ------------------------------------------------------------------------- -}

conj_intro_tac :: Tactic
conj_intro_tac (Goals ((asl,And p q) : gls) jfn) = return $
  let jfn' (thp:thq:ths) = and_pair p q >>= imp_trans_chain [thp, thq] >>= \x -> jfn (x : ths) in
  Goals ((asl,p) : (asl,q) : gls) jfn'

{- ------------------------------------------------------------------------- -}
{- Handy idiom for tactic that does not split subgoals.                      -}
{- ------------------------------------------------------------------------- -}

jmodify :: ([a] -> Failing b) -> (a -> Failing a) -> [a] -> Failing b
jmodify jfn tfn (th:oths) = tfn th >>= \x -> jfn (x : oths)

{- ------------------------------------------------------------------------- -}
{- Version of gen_right with a bound variable change.                        -}
{- ------------------------------------------------------------------------- -}

gen_right_alpha :: String -> String -> Thm -> Failing Thm
gen_right_alpha y x th = do
  th1 <- gen_right y th
  consequent (concl th1) >>= alpha x >>= imp_trans th1

{- ------------------------------------------------------------------------- -}
{- Universal introduction.                                                   -}
{- ------------------------------------------------------------------------- -}

forall_intro_tac :: (?loc :: CallStack) => String -> Tactic
forall_intro_tac y (Goals ((asl,fm@(Forall x p)) : gls) jfn) =
  if S.member y (fv fm) || any (S.member y . fv . snd) asl
  then failure "fix: variable already free in goal" else
  return (Goals ((asl,subst (M.singleton x (Var y)) p) : gls) (jmodify jfn (gen_right_alpha y x)))
forall_intro_tac _ _ = failure "forall_intro_tac"

{- ------------------------------------------------------------------------- -}
{- Another inference rule: |- P[t] ==> exists x. P[x]                        -}
{- ------------------------------------------------------------------------- -}

right_exists :: String -> Term -> Formula FOL -> Failing Thm
right_exists x t p = do
  th <- ispec t (Forall x (Not p)) >>= contrapos
  Not (Not p') <- antecedent (concl th)
  (sequence [imp_contr p' FF, iff_imp1 (axiom_not p') >>= imp_add_concl FF,
             iff_imp2 (axiom_not (Not p')), return th, iff_imp2 (axiom_exists x p)]) >>= end_itlistM imp_trans

{- ------------------------------------------------------------------------- -}
{- Existential introduction.                                                 -}
{- ------------------------------------------------------------------------- -}

exists_intro_tac :: Term -> Tactic
exists_intro_tac t (Goals ((asl,Exists x p) : gls) jfn) = return $
  Goals ((asl,subst (M.singleton x t) p) : gls)
         (jmodify jfn (\th -> right_exists x t p >>= imp_trans th))

{- ------------------------------------------------------------------------- -}
{- Implication introduction tactic.                                          -}
{- ------------------------------------------------------------------------- -}

imp_intro_tac :: String -> Tactic
imp_intro_tac s (Goals ((asl,Imp p q) : gls) jfn) = return $
  let jmod = if null asl then add_assum TT else \x -> shunt x >>= imp_swap in
  Goals (((s,p):asl,q):gls) (jmodify jfn jmod)
imp_intro_tac _ _ = failure "imp_intro_tac"

{- ------------------------------------------------------------------------- -}
{- Append contextual hypothesis to unconditional theorem.                    -}
{- ------------------------------------------------------------------------- -}

assumptate :: Goals -> Thm -> Failing Thm
assumptate (Goals ((asl,w):gls) jfn) th =
  add_assum (list_conj (map snd asl)) th

{- ------------------------------------------------------------------------- -}
{- Get the first assumption (quicker than head of assumps result).           -}
{- ------------------------------------------------------------------------- -}

firstassum :: [(a, Formula FOL)] -> Failing Thm
firstassum asl =
  let p = snd (head asl)
      q = list_conj (map snd (tail asl)) in
  if null (tail asl) then imp_refl p else and_left p q

{- ------------------------------------------------------------------------- -}
{- Import "external" theorem.                                                -}
{- ------------------------------------------------------------------------- -}

using :: [Failing Thm] -> t -> Goals -> Failing [Thm]
using fths p g = do
  ths <- sequence fths
  let ths' = map (\th -> itlist gen (fv (concl th)) th) ths
  mapM (assumptate g) ths'

{- ------------------------------------------------------------------------- -}
{- Turn assumptions p1,...,pn into theorems |- p1 /\ ... /\ pn ==> pi        -}
{- ------------------------------------------------------------------------- -}

assumps_helper :: [Formula FOL] -> Failing [Thm]
assumps_helper asl =
  case asl of
    [] -> return []
    [p] -> imp_refl p >>= return . (: [])
    (p : ps) -> do
        ths <- assumps_helper ps
        q <- antecedent (concl (head ths))
        rth <- and_right p q
        sequence (and_left p q : map (\th -> imp_trans rth th) ths)


assumps :: [(a, Formula FOL)] -> Failing [(a, Thm)]
assumps asl = let (ls,ps) = unzip asl in assumps_helper ps >>= return . zip ls

{- ------------------------------------------------------------------------- -}
{- Produce canonical theorem from list of theorems or assumption labels.     -}
{- ------------------------------------------------------------------------- -}

by :: (?loc :: CallStack) => [String] -> t -> Goals -> Failing [Thm]
by hyps p (Goals ((asl,w):gls) jfn) = do
  ths <- assumps asl
  maybe (failure "by") return (mapM (\s -> lookup s ths) hyps)

{- ------------------------------------------------------------------------- -}
{- Main automatic justification step.                                        -}
{- ------------------------------------------------------------------------- -}

justify
  :: (t -> Formula FOL -> Goals -> Failing [Thm])
     -> t -> Formula FOL -> Goals -> Failing Thm
justify byfn hyps p g = sequenceNestedFailing $ do
  result <- byfn hyps p g
  return (case result of
    [th] | consequent (concl th) == return p -> return th
    ths -> do
      th <- itlistM (\x q -> consequent (concl x) >>= return . flip Imp q) ths p >>= lcffol
      if null ths then assumptate g th else imp_trans_chain ths th)

{- ------------------------------------------------------------------------- -}
{- Nested subproof.                                                          -}
{- ------------------------------------------------------------------------- -}

proof :: [Tactic] -> Formula FOL -> Goals -> Failing [Thm]
proof tacs p (Goals ((asl,w):gls) jfn) = sequence [tac_proof (Goals [(asl,p)] (return . head)) tacs]

{- ------------------------------------------------------------------------- -}
{- Trivial justification, producing no hypotheses.                           -}
{- ------------------------------------------------------------------------- -}

at once p gl = return []
once = []

{- ------------------------------------------------------------------------- -}
{- Hence an automated terminal tactic.                                       -}
{- ------------------------------------------------------------------------- -}

auto_tac :: (t -> Formula FOL -> Goals -> Failing [Thm]) -> t -> Tactic
auto_tac byfn hyps g@(Goals ((asl,w):gls) jfn) = do
  th <- justify byfn hyps w g
  return (Goals gls (\ths -> jfn (th:ths)))

g0 = set_goal $ parseFOL "(forall x. x <= x) & (forall x y z. x <= y & y <= z ==> x <= z) & (forall x y. f(x) <= y <==> x <= g(y)) ==> (forall x y. x <= y ==> f(x) <= f(y)) & (forall x y. x <= y ==> g(x) <= g(y))"

g1 = imp_intro_tac "ant" g0

g2 = g1 >>= conj_intro_tac

g3 = g2 >>= funpowM 2 (auto_tac by ["ant"])

sample_proof1 = prove (parseFOL "(forall x. x <= x) & (forall x y z. x <= y & y <= z ==> x <= z) & (forall x y. f(x) <= y <==> x <= g(y)) ==> (forall x y. x <= y ==> f(x) <= f(y)) & (forall x y. x <= y ==> g(x) <= g(y))")
      [imp_intro_tac "ant",
       conj_intro_tac,
       auto_tac by ["ant"],
       auto_tac by ["ant"]]

{- ------------------------------------------------------------------------- -}
{- A "lemma" tactic.                                                         -}
{- ------------------------------------------------------------------------- -}

infixr 9 -.-

(-.-) :: Monad m => (b -> m c) -> (a -> m b) -> a -> m c
(-.-) f g x = g x >>= f


lemma_tac
  :: String
     -> Formula FOL
     -> (t -> Formula FOL -> Goals -> Failing [Thm])
     -> t
     -> Tactic
lemma_tac s p byfn hyps g@(Goals ((asl,w):gls) jfn) = return $ do
  let tr th = justify byfn hyps p g >>= flip imp_trans th
  let mfn = if null asl then tr else imp_unduplicate -.- tr -.- shunt
  Goals (((s,p):asl,w):gls) (jmodify jfn mfn)

{- ------------------------------------------------------------------------- -}
{- Elimination tactic for existential quantification.                        -}
{- ------------------------------------------------------------------------- -}

exists_elim_tac
  :: String
     -> Formula FOL
     -> (t -> Formula FOL -> Goals -> Failing [Thm])
     -> t
     -> Tactic
exists_elim_tac l fm byfn hyps g@(Goals ((asl,w) : gls) jfn) =
  let Exists x p = fm in
  if any (S.member x . fv) (w : map snd asl)
  then failure "exists_elim_tac: variable free in assumptions" else do
  th <- justify byfn hyps (Exists x p) g
  let jfn' pth = shunt pth >>= exists_left x >>= imp_trans th >>= imp_unduplicate
  return (Goals (((l,p):asl,w) : gls) (jmodify jfn jfn'))

{- ------------------------------------------------------------------------- -}
{- If |- p ==> r and |- q ==> r then |- p \/ q ==> r                         -}
{- ------------------------------------------------------------------------- -}

ante_disj :: Thm -> Thm -> Failing Thm
ante_disj th1 th2 = do
  (p,r) <- dest_imp (concl th1)
  (q,s) <- dest_imp (concl th2)
  ths <- mapM contrapos [th1, th2]
  th3 <- and_pair (Not p) (Not q) >>= imp_trans_chain ths
  th4 <- iff_imp2 (axiom_not r) >>= flip imp_trans th3 >>= contrapos
  th5 <- iff_imp1 (axiom_or p q) >>= flip imp_trans th4
  iff_imp1 (axiom_not (Imp r FF)) >>= imp_trans th5 >>= right_doubleneg

{- ------------------------------------------------------------------------- -}
{- Elimination tactic for disjunction.                                       -}
{- ------------------------------------------------------------------------- -}

disj_elim_tac
  :: String
     -> Formula FOL
     -> (t -> Formula FOL -> Goals -> Failing [Thm])
     -> t
     -> Tactic
disj_elim_tac l fm byfn hyps g@(Goals ((asl,w):gls) jfn) = do
  th <- justify byfn hyps fm g
  (p,q) <- dest_or fm
  let jfn' (pth:qth:ths) = do
      th1 <- sequence [shunt pth,shunt qth] >>= \[x,y] -> ante_disj x y >>= imp_trans th
      imp_unduplicate th1 >>= \x -> jfn (x:ths)
  return (Goals (((l,p):asl,w):((l,q):asl,w):gls) jfn')

{- ------------------------------------------------------------------------- -}
{- Declarative proof.                                                        -}
{- ------------------------------------------------------------------------- -}

multishunt :: (Eq a, Num a) => a -> Thm -> Failing Thm
multishunt i th = do
  th1 <- funpowM i (imp_swap -.- shunt) th >>= imp_swap
  funpowM (i - 1) (unshunt -.- imp_front 2) th1 >>= imp_swap

assume :: [(String, Formula FOL)] -> Tactic
assume lps (Goals ((asl,Imp p q):gls) jfn) =
  if end_itlist And (map snd lps) /= p then failure "assume" else
  let jfn' th = if null asl then add_assum TT th
                else multishunt (length lps) th in
  return (Goals ((lps ++ asl,q):gls) (jmodify jfn jfn'))

note
  :: (String, Formula FOL)
     -> (t -> Formula FOL -> Goals -> Failing [Thm]) -> t -> Tactic
note (l,p) = lemma_tac l p

have
  :: Formula FOL
     -> (t -> Formula FOL -> Goals -> Failing [Thm]) -> t -> Tactic
have p = note ("",p)

so
  :: (t1 -> (t2 -> t3 -> Goals -> Failing [Thm]) -> t)
     -> t1 -> (t2 -> t3 -> Goals -> Failing [Thm]) -> t
so tac arg byfn =
  tac arg (\hyps p gl@(Goals((asl,w):_) _) -> sequence $
                     firstassum asl : sequence (byfn hyps p gl))

fix :: (?loc :: CallStack) => String -> Tactic
fix = forall_intro_tac

consider
  :: (String, Formula FOL)
     -> (t -> Formula FOL -> Goals -> Failing [Thm]) -> t -> Tactic
consider (x,p) = exists_elim_tac "" (Exists x p)

take :: Term -> Tactic
take = exists_intro_tac

cases
  :: Formula FOL
     -> (t -> Formula FOL -> Goals -> Failing [Thm]) -> t -> Tactic
cases = disj_elim_tac ""

{- ------------------------------------------------------------------------- -}
{- Thesis modification.                                                      -}
{- ------------------------------------------------------------------------- -}

conclude
  :: Formula FOL
     -> (t -> Formula FOL -> Goals -> Failing [Thm])
     -> t
     -> Tactic
conclude p byfn hyps gl@(Goals ((asl,w):gls) jfn) = do
  th <- justify byfn hyps p gl
  if p == w then return (Goals ((asl,TT):gls) (jmodify jfn (const (return th)))) else do
   th <- justify byfn hyps p gl
   (p',q) <- dest_and w
   if p' /= p then failure "conclude: bad conclusion" else
    let mfn th' = and_pair p q >>= imp_trans_chain [th, th'] in
    return (Goals ((asl,q):gls) (jmodify jfn mfn))

{- ------------------------------------------------------------------------- -}
{- A useful shorthand for solving the whole goal.                            -}
{- ------------------------------------------------------------------------- -}

our
  :: t
     -> (t1 -> Formula FOL -> Goals -> Failing [Thm])
     -> t1
     -> Tactic
our thesis byfn hyps gl@(Goals ((asl,w):gls) jfn) =
  conclude w byfn hyps gl

thesis :: String
thesis = ""

{- ------------------------------------------------------------------------- -}
{- Termination.                                                              -}
{- ------------------------------------------------------------------------- -}

qed :: (?loc :: CallStack) => Tactic
qed gl@(Goals ((asl,TT):gls) jfn) = return (Goals gls (\ths -> truth >>= assumptate gl >>= \x -> jfn (x : ths)))
qed _ = failure "qed: non-trivial goal"

{- ------------------------------------------------------------------------- -}
{- A simple example.                                                         -}
{- ------------------------------------------------------------------------- -}

fol = parseFOL
folTerm = parseFOLTerm

ewd954 :: Failing Thm
ewd954 = prove
 (fol "(forall x y. x <= y <==> x * y = x) & (forall x y. f(x * y) = f(x) * f(y)) ==> forall x y. x <= y ==> f(x) <= f(y)")
 [note ("eq_sym", fol "forall x y. x = y ==> y = x")
    using [eq_sym x y],
  note ("eq_trans", fol "forall x y z. x = y & y = z ==> x = z")
    using [eq_trans x y z],
  note ("eq_cong", fol "forall x y. x = y ==> f(x) = f(y)")
    using [icongruence x y (folTerm "f(x)") (folTerm "f(y)")],
  assume [("le", fol "forall x y. x <= y <==> x * y = x"),
          ("hom", fol "forall x y. f(x * y) = f(x) * f(y)")],
  fix "x", fix "y",
  assume [("xy", fol "x <= y")],
  so have (fol "x * y = x") by ["le"],
  so have (fol "f(x * y) = f(x)") by ["eq_cong"],
  so have (fol "f(x) = f(x * y)") by ["eq_sym"],
  so have (fol "f(x) = f(x) * f(y)") by ["eq_trans", "hom"],
  so have (fol "f(x) * f(y) = f(x)") by ["eq_sym"],
  so conclude (fol "f(x) <= f(y)") by ["le"],
  qed]
 where
  x = Var "x"
  y = Var "y"
  z = Var "z"

sample_proof2 = prove
 (fol "(exists x. p(x)) ==> (forall x. p(x) ==> p(f(x))) ==> exists y. p(f(f(f(f(y)))))")
  [assume [("A",fol "exists x. p(x)")],
   assume [("B",fol "forall x. p(x) ==> p(f(x))")],
   note ("C",fol "forall x. p(x) ==> p(f(f(f(f(x)))))")
   proof
    [have (fol "forall x. p(x) ==> p(f(f(x)))") by ["B"],
     so conclude (fol "forall x. p(x) ==> p(f(f(f(f(x)))))") at once,
     qed],
   consider ("a",fol "p(a)") by ["A"],
   take (Var "a"),
   so conclude (fol "p(f(f(f(f(a)))))") by ["C"],
   qed]

sample_proof3 = prove (fol "p(a) ==> (forall x. p(x) ==> p(f(x))) ==> exists y. p(y) & p(f(y))")
      [our thesis at once,
       qed]

{- ------------------------------------------------------------------------- -}
{- Alternative formulation with lemma construct.                             -}
{- ------------------------------------------------------------------------- -}

lemma :: (String, Formula FOL) -> Tactic
lemma (s,p) gl@(Goals ((asl,w):gls) jfn) = return $
  Goals ((asl,p):((s,p):asl,w):gls)
        (\(thp:thw:oths) -> shunt thw >>= imp_trans thp >>= imp_unduplicate >>= \x -> jfn (x : oths))

sample_proof4 = prove
 (fol "(exists x. p(x)) ==> (forall x. p(x) ==> p(f(x))) ==> exists y. p(f(f(f(f(y)))))")
  [assume [("A",fol "exists x. p(x)")],
   assume [("B",fol "forall x. p(x) ==> p(f(x))")],
   lemma ("C",fol "forall x. p(x) ==> p(f(f(f(f(x)))))"),
     have (fol "forall x. p(x) ==> p(f(f(x)))") by ["B"],
     so conclude (fol "forall x. p(x) ==> p(f(f(f(f(x)))))") at once,
     qed,
   consider ("a",fol "p(a)") by ["A"],
   take (Var "a"),
   so conclude (fol "p(f(f(f(f(a)))))") by ["C"],
   qed]

{- ------------------------------------------------------------------------- -}
{- Running a series of proof steps one by one on goals.                      -}
{- ------------------------------------------------------------------------- -}

run :: Monad m => [a -> m a] -> a -> m a
run [] g = return g
run (tac:prf) g = do
  h <- tac g
  run prf h
