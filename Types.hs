module Types where

import Text.PrettyPrint
import Data.List
import Data.Maybe
import Data.Char
import qualified Data.Set as S
import qualified Data.Map as M
import Failing
import Control.Monad (foldM)

newtype Prop = P {pname :: String}  deriving (Eq,Ord)

instance Show Prop where
  show (P s) = s

data PrologRule = Prolog [Formula FOL] (Formula FOL)   deriving (Eq,Ord)

data Formula a = FF
               | TT
               | Atom a
               | Not (Formula a)
               | And (Formula a) (Formula a)
               | Or (Formula a) (Formula a)
               | Imp (Formula a) (Formula a)
               | Iff (Formula a) (Formula a)
               | Forall String (Formula a)
               | Exists String (Formula a)
               deriving (Eq, Ord)

data Term = Var String | Fn String [Term]  deriving (Eq,Ord)

data FOL = R String [Term]  deriving (Eq,Ord)

-- library functions

repeatTo :: (c -> Failing c) -> c -> c
repeatTo f x = try (do
   y <- f x
   return (repeatTo f y)) x

decreasing :: Ord a => (t -> a) -> t -> t -> Ordering
decreasing f x y = compare (f y) (f x)

{-
	noerr :: Failing c -> c
	noerr (Right x) = x
	noerr (Left s) = error s
-}

tryfind :: (t -> Failing b) -> [t] -> Failing b
tryfind _ [] = failure "tryfind"
tryfind f (h:t) = tryM (f h) (tryfind f t) --either (const (tryfind f t)) return (f h)

funpow :: (Eq a, Num a) => a -> (t -> t) -> t -> t
funpow 0 _ x = x
funpow n f x = f (funpow (n - 1) f x)

funpowM :: (Eq a, Monad m, Num a) => a -> (b -> m b) -> b -> m b
funpowM 0 _ x = return x
funpowM n f x = funpowM (n - 1) f x >>= f

valmod a y f x = if x == a then y else f x

mapfilter f l = catMaybes (map f l)

flatten :: S.Set (S.Set a) -> [[a]]
flatten = (map S.toList) . S.toList

setify xs = map head (group (sort xs))

allpairs f (h1:t1) l2 = itlist (\x a -> f h1 x : a) l2 (allpairs f t1 l2)
allpairs _ [] _ = []

allnonemptysubsets = tail . allsubsets
allsubsets = subsequences
subset xs ys = isSubsequenceOf xs ys
psubset xs ys = (subset xs ys) && (xs /= ys)

conjuncts :: Formula t -> [Formula t]
conjuncts (And p q) = (conjuncts p) ++ (conjuncts q)
conjuncts fm = [fm]

disjuncts :: Formula t -> [Formula t]
disjuncts (Or p q) = (disjuncts p) ++ (disjuncts q)
disjuncts fm = [fm]

iffs :: Formula t -> [Formula t]
iffs (Iff p q) = (iffs p) ++ (iffs q)
iffs fm = [fm]

unions :: Ord a => S.Set (S.Set a) -> S.Set a
unions sets = S.fold S.union S.empty sets

itlist2 :: (t -> u -> v -> v) -> [t] -> [u] -> v -> v
itlist2 f [] [] b = b
itlist2 f (h1:t1) (h2:t2) b = f h1 h2 (itlist2 f t1 t2 b)
itlist2 _ _ _ _ = error "itlist2"

itlistM :: Monad m => (a -> b -> m b) -> [a] -> b -> m b
itlistM f l b = foldM (flip f) b (reverse l)

itlist :: Foldable t => (a -> b -> b) -> t a -> b -> b
itlist f l b = foldr f b l

end_itlist :: Foldable t => (a -> a -> a) -> t a -> a
end_itlist f l = foldr1 f l

end_itlistM :: Monad m => (b -> b -> m b) -> [b] -> m b
end_itlistM _ [] = error "end_itlistM"
end_itlistM f l = itlistM f (init l) (last l)

tryapplyl m k
 | M.member k m = m M.! k
 | otherwise = mempty

-- Whether the first of two items comes earlier in the list.
earlier :: Eq a => [a] -> a -> a -> Bool
earlier (h:t) x y = (h /= y) && (h == x || earlier t x y)
earlier [] x y = False

{- ------------------------------------------------------------------------- *)
(* Union-find algorithm.                                                     *)
(* ------------------------------------------------------------------------- -}

data PNode a = Nonterminal a | Terminal a Int
newtype Partition a = Partition (M.Map a (PNode a))

terminus ptn@(Partition f) a
  | M.member a f = case (f M.! a) of
    (Nonterminal b) -> terminus ptn b
    (Terminal p q) -> return (p,q)
  | otherwise = failure "terminus"

tryterminus ptn a = try (terminus ptn a) (a,1) --either (const (a,1)) id (terminus ptn a)

canonize ptn a = fst (tryterminus ptn a)

equivalent eqv a b = canonize eqv a == canonize eqv b

equate (a,b) ptn@(Partition f) = Partition
   (if a' == b' then f else
    if na <= nb then
       itlist id [M.insert a' (Nonterminal b'), M.insert b' (Terminal b' (na+nb))] f
    else
       itlist id [M.insert b' (Nonterminal a'), M.insert a' (Terminal a' (na+nb))] f)
 where
  (a',na) = tryterminus ptn a
  (b',nb) = tryterminus ptn b

unequal = Partition M.empty

equated (Partition f) = M.keys f

-- pretty printing Prop

instance (Show a) => Show (Formula a) where
 show fm = render (showFormula fm)

showFormula FF = char '⟘'
showFormula TT = char '⟙'
showFormula (Atom a) = text (show a)
showFormula (Not a@(Atom _)) = (char '¬') <> (showFormula a)
showFormula (Not p) = parens $ (char '¬') <> (showFormula p)
showFormula p@(And _ _) = parens $ hcat (punctuate (text " ⋀ ") (map showFormula (conjuncts p)))
showFormula p@(Or _ _) = parens $ hcat (punctuate (text " ⋁ ") (map showFormula (disjuncts p)))
showFormula (Imp p q) = parens $ (showFormula p) <+> (text "⟹ ") <+> (showFormula q)
showFormula p@(Iff _ _) = parens $ hcat (punctuate (text " ⟺  ") (map showFormula (iffs p)))
showFormula (Forall x p) = (char '∀') <> (text x) <> (char ':') <> (showFormula p)
showFormula (Exists x p) = (char '∃') <> (text x) <> (char ':') <> (showFormula p)

-- pretty printing FOL

isOperator c = (isSymbol c) || (isPunctuation c)

showTerm (Var s) = text s
showTerm (Fn s []) = text "'" <> text s <> text "'"
showTerm (Fn s@(h:_) [x,y]) | isOperator h = parens (showTerm x <+> (text s) <+> showTerm y)
showTerm (Fn s args) = text s <> (brackets (hcat (punctuate (char ',') (map showTerm args))))

showFOL (R s []) = text s
showFOL (R s@(h:_) [x,y]) | isOperator h = showTerm x <+> (text s) <+> showTerm y
showFOL (R s args) = text s <> (parens (hcat (punctuate (char ',') (map showTerm args))))

showProlog (Prolog xs y) = (showFormula y) <+> (text ":-") <+> (hcat (punctuate (text ", ") (map showFormula xs)))

instance Show Term where
   show t = render (showTerm t)

instance Show FOL where
   show t = render (showFOL t)

instance Show PrologRule where
   show prolog = render (showProlog prolog)
